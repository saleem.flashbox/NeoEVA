/**
 * Created by Suhas on 12/5/2015.
 */

(function() {
    'use strict';
    angular.module('NEC')
        .controller('loginCtrl', loginCtrl);
    function loginCtrl($scope, $window, $rootScope, settingsService, $state, loginService, $cookies, webSocketService, appConfig) {
        $scope.user = {};
        $scope.checkBox = {};
        console.log(localStorage.getItem('autoLogin'))
        $scope.checkBox = {value1 : (localStorage.getItem('autoLogin') === null || localStorage.getItem('autoLogin') === 'false') ? false : true }
        // $scope.autoLogin = false;
        init();
        function init(){
            checkBaseUrl();
            // getApplicationVersion();
        }
        function checkBaseUrl() {
            if(localStorage.hasOwnProperty('baseUrl')){
                settingsService.updateAppConfig();
            }
            else if(appConfig.baseUrl === ''){
                $state.go('menu.app.settings');
            }
        }
        /*$scope.applicationVersion = '-';
        function getApplicationVersion(){
            loginService.getApplicationVersion()
                .then( function (response){
                    $scope.applicationVersion = response.data[0].version;
            });
        };*/
        $scope.checkValidation = function(user) {
            $scope.error = '';
            $scope.loginLoader = true;
            loginService.getTokenForAuthorizedUser(user)
                .then(function(response) {
                    if(response.status===200) {
                       // $window.sessionStorage.token = response.data.Server_response;
                        localStorage.setItem('token', response.headers('authorizationToken'));
                        localStorage.setItem('username', user.username);
                        localStorage.setItem('password', user.password);
                        localStorage.setItem('autoLogin', $scope.checkBox.value1);
                        $scope.loginLoader = false;
                        // $cookies.put('currentApp', 'overview');
                        $rootScope.$emit(loginService.LOGIN.MSG, loginService.LOGIN.STATUS.CONNECTED);

                        $state.go('menu.home');
                       webSocketService.registerWebsocket();
                    } else if(response.status===404 || response.status===400 || response.status === 401) {
                        $scope.loginLoader = false;
                        $scope.error = response.statusText;
                    }
                });
        };

        $scope.logout = function() {
            loginService.logout(localStorage.getItem('token'));
            localStorage.removeItem('token');
            localStorage.removeItem('username');
            localStorage.removeItem('password');
            localStorage.removeItem('autoLogin');
            $rootScope.$emit(loginService.LOGIN.MSG, loginService.LOGIN.STATUS.DISCONNECTED);
            $cookies.put('currentApp', undefined);
            $state.go('login');
        };

        $scope.enterToLogin = function(event) {
            if (event.keyCode == 13) {
                $scope.checkValidation($scope.user);
            }
        };
      $rootScope.$on('autoLogin', function () {
        $scope.user.username = localStorage.getItem('username');
        $scope.user.password = localStorage.getItem('password');
        if($scope.user.username.length >0){
          $scope.checkValidation($scope.user);
        }

      })
    }
})();


