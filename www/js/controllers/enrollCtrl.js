/**
 * Created by Suhas on 12/5/2015.
 */

NEC.controller('enrollCtrl', function ($scope, watchListService, $timeout, $state, $http, appConfig, camera, $ionicHistory, $ionicPlatform) {
    $scope.person = {};
    $scope.personImg = '';
    $scope.person.pictures = [];
    $scope.person.firstName = '';
    $scope.person.type = "high";
    $scope.watchListSelectedForPeople = [];
    $scope.newPersonImgUpload = function (event, imgId) {
        $ionicPlatform.ready(function () {
            camera.uploadPhoto(event).then(function (base64data) {
                if (base64data) {
                    if (imgId == 'personImg') {
                        $scope.personImg = 'data:image/jpeg;base64,' + base64data;
                        $scope.person.pictures[0] = base64data;
                    }
                }
                $(".selectImgOpt").fadeOut(300);
            }, function (err) {
                $state.go('menu.home');
            });
        })
    };
    $scope.retakePersonImg = function (event, imgId) {
        $ionicPlatform.ready(function () {
            camera.uploadPhoto(event).then(function (base64data) {
                if (base64data) {
                    if (imgId == 'personImg') {
                        $scope.personImg = 'data:image/jpeg;base64,' + base64data;
                        $scope.person.pictures[0] = base64data;
                    }
                }
            });
        })
    };

    $scope.getAllWatchListDetails = function () {
        // $scope.watchlists.showCircularLoader = true;
        let allWatchlistDetails = watchListService.getAllWatchLists();
        allWatchlistDetails.then(function (response) {
            $scope.watchlists = response;
        }, function (error) {
            throw ('Error getting response from Server: \nError Code : ' + error.status + '\nMessage : ' + error.statusText);
        });
    };
    $scope.selectWatchlist = function toggleSelection(watchlistId) {
        let index = _.findIndex($scope.watchListSelectedForPeople, function (val) {
            return val === watchlistId;
        });
        if (index != -1) {
            _.remove($scope.watchListSelectedForPeople, function (obj) {
                return watchlistId === obj;
            });
        } else {
            $scope.watchListSelectedForPeople.push(watchlistId);
        }

    };

    $scope.savePersonLoader = false;
    $scope.savePerson = function (mode) {
        if ($scope.person.firstName != '') {
            $scope.savePersonLoader = true;
            $scope.personData = [];
            $scope.person.documentExpiryDate = moment($scope.person.documentExpiryDate).format('YYYY-MM-DD');
            $scope.personData.push($scope.person);
            watchListService.savePerson($scope.personData)
                .then(function (response) {
                    let serverResponse = JSON.parse(response.data.Server_response);
                    $scope.associatePersonToWatchlist($scope.watchListSelectedForPeople, serverResponse[0].personId);
                })
                .catch(function (error) {
                    throw ('Error While creating Entity: \nError  : ' + error);
                });
        }
        else {
            $scope.errMsg = "Person name required";
        }
    };
    $scope.associatePersonToWatchlist = function (watchlistArray, personId) {
        let count = 0;
        if (watchlistArray.length > 0) {
            _.forEach(watchlistArray, function (values) {
                let payLoad = [
                    {personId: personId}
                ]
                $http.post(appConfig.applicationBaseUrl + '/biometrics/watchlists/' + values + '/members', payLoad)
                    .then(function (response) {
                        // console.log("Watchlist associated to person");
                        count++;
                        if (count == watchlistArray.length) {
                            $scope.savePersonLoader = false;
                            $scope.showMessagePopup('personSavedMessage');
                            $state.go('menu.app.people');
                        }
                    });
            })
        } else {
            $scope.savePersonLoader = false;
            $scope.showMessagePopup('personSavedMessage');
            $state.go('menu.app.people');
        }
    };


    $scope.getAllWatchListDetails();

    $scope.callCam = function () {
        $scope.newPersonImgUpload('useCamera', 'personImg');
    };
    $scope.callGallery = function () {
        $scope.newPersonImgUpload('useGallery', 'personImg');
    };

    $scope.onCamSelection = function () {
        $(".selectImgOpt").fadeIn(300);
    };
    $scope.cancelCamSelection = function () {
        if ($scope.personImg) {
            $(".selectImgOpt").fadeOut(300);
        }
        else {
            window.history.back();
        }
    };

});


