/**
 * Created by MohammedSaleem on 29/12/16.
 */

NEC.controller('watchlistController', function (appConfig, $q, $rootScope, $scope, $location, $state,
                                                $timeout, $stateParams, watchListService, paginationService, loginService, settingsService, $http, camera, appConstantService, appService) {
    // ...... Slider initialization......
    $scope.slickConfig = {
        dots: true,
        slide: 'li',
        infinite: false,
        swipe: true,
        arrows: false,
        fade: true,
        enabled: false,
        dotsClass: 'personUploadNav',
        customPaging: function (slider, i) {
            return '';
        },
    };
    $scope.watchlistObj = {};
    $scope.personPageOffset = 0;
    $scope.personPageLimit = 20;
    $scope.personTotalPage = 0;
    $scope.paginationPageSize = parseInt(settingsService.getSettingDetails().pagination_pageSize);
    $scope.watchListSelectedForPeople = [];
    $scope.defaultImage = watchListService.getDefaultImage();
    $scope.thresholdMatchScore = parseFloat(settingsService.getSettingDetails().thresholdScore);
    $scope.enrolledImgNotAvailable = false;
    $scope.watchlistMembersLoader = false;
    $scope.personImg = false;
    $scope.personImages = [];
    $scope.redirectTo = 'menu.app.watchlist';
    $scope.redirectAfterRelationUpdate = 'menu.app.watchlist';
    $scope.watchlists = {
        watchlistDetails: [],
        watchlistNew: {
            name: '',
            watchlistId: 0,
        },
        selectedWatchlistId: '',
        watchlistEdit: {},
        showCircularLoader: false,
        formAction: 'save',
    };
    $scope.watchlistPersons = {
        personDetails: [],
        watchlistName: '',
        edit: true,
        editBtn: false,
        formAction: 'save',
        showCircularLoader: false,
    };
    $scope.watchlistNameandID = {
        name: '',
        watchlistId: 0,
    };
    $scope.person = {
        notes: {},
    };
    $scope.newPersonImgUpload = function (event, imgId){
        camera.uploadPhoto(event).then(function (base64data) {
            if (imgId == 'personImg') {
                $scope.personImg = 'data:image/jpeg;base64,' + base64data;
                $scope.personImages[0] = base64data;
            }
        });
    };
    $scope.resetPersonForm = function () {
        $scope.person = {};
        $scope.personImg = '';
    }

    $scope.resetImg = function (imgId) {
        if (imgId === 'personImg') {
            $scope.personImg = '';
        }
    }

    $scope.newWatchlist = function () {
        $scope.watchlist = {};
        $scope.watchlist.icon = '';
    }
    $scope.saveCroppedImage = function (image, imgId) {
        let base64data = image;
        if (imgId == 'personImg') {
            $scope.personImg = image;
            $scope.personImages[0] = base64data.substring(base64data.lastIndexOf('base64') + 7, base64data.length);
        }
    };
    $scope.uploadWatchlistIcon = function () {
        camera.uploadPhoto('useGallery').then(function (base64data) {
            $scope.watchlist.icon = base64data;
        });
    };
    $scope.closePersonEdit = function () {
        $state.go($scope.redirectTo, {}, {reload: true});
    };
    $scope.savePersonLoader = false;
    $scope.personImageUpdateMessage = [];

    $scope.watchlist = {};
    $scope.removePersonWatchlistRelationship = function () {
        let watchlistId = $stateParams.watchlistId;
        watchListService.removePersonWatchlistAssociation(watchlistId, $scope.person.personId);
        $scope.showMessagePopup('personWatchlistmappingUpdated');
        $timeout(function () {
            $state.go($scope.redirectAfterRelationUpdate, {'watchlistId': watchlistId}, {reload: true});
            $scope.loadPersonData();
        }, 1000);
    };

    $scope.loadPersonDetailsforVieworEdit = function (userSelected, mode) {
        $scope.person = userSelected;
        $scope.person.personId = userSelected.personId;
        $scope.submitClicked = false;
        $scope.loadPersonImage(userSelected.personId);
    };

    $scope.getAllWatchListDetails = function () {
        $scope.watchlists.showCircularLoader = true;
        let allWatchlistDetails = watchListService.getAllWatchLists();
        allWatchlistDetails.then(function (response) {
            $scope.serverErrorinWatchlistAll = false;
            $scope.watchlists.showCircularLoader = false;
            $scope.watchlists.watchlistDetails = response;
            _.forEach(response, function (values) {
                $scope.watchlistObj[values.watchlistId] = values;
            })

        }, function (error) {
            $scope.serverErrorinWatchlistAll = true;
            throw ('Error getting response from Server: \nError Code : ' + error.status + '\nMessage : ' + error.statusText);
        });
    };
    /*$scope.loadMoreWatchlist = function(offset, limit) {
     $scope.getAllWatchListDetails(offset, limit);
     }*/


    $scope.getAllWatchListDetailsChild = function () {
        watchListService.getAllWatchLists().then(function (response) {
            if (!angular.isUndefined(response.status) && response.status != 200) {
                throw ('Error getting response from Server: \nError Code : ' + response.data.status + '\nMessage : ' + response.statusText);
            }
            $scope.watchlists.WatchlistPersonData = response;
        });
    };

    $scope.resetPersonImageData = function () {
        $scope.personImg = false;
        $scope.personImg2 = false;
        $scope.personImg3 = false;
        $scope.personImages = [];
        $scope.person1Edit = false;
        $scope.person2Edit = false;
        $scope.person3Edit = false;

        $('.uploadBtn .eyePos').remove();
        $('.uploadBtn .personImg').removeClass('added');
    };
    $scope.showDetailRecord = function (data) {
        $scope.watchlist = data;
        $scope.watchlistMembers = {};
        $scope.watchlistMembersLoader = true;
        /*let loadWatchlistData = watchListService.getWatchlistforEdit(data.watchlistId);
         loadWatchlistData.then(function (resp) {
         $scope.watchlist = resp;
         $scope.serverErrorinPersonlist = false;
         }, function (error) {
         $scope.serverErrorinPersonlist = true;
         throw ('Error getting response from Server: \nError Code : ' + error.status + '\nMessage : ' + error.statusText);
         });*/
        let loadPeopleData = watchListService.getPeopleList(data.watchlistId);
        // watchListService.getAllWatchLists();
        loadPeopleData.then(function (successResp) {
            $scope.watchlistMembersLoader = false;
            $scope.watchlistMembers = successResp;
            //  $scope.watchlistPersons.personDetails = personlistMapper;
        }, function (error) {
            $scope.serverErrorinPersonlist = true;
            throw ('Error getting response from Server: \nError Code : ' + error.status + '\nMessage : ' + error.statusText);
        });
    };
// logic to show person details that was clicked from dashboard
    $scope.loadViewPersonDetails = function () {
        $scope.viewPersonDetails = watchListService.getDashboardPersonData();
    };
    $scope.loadPersonData = function (offset, limit) {
        $scope.watchlistPersons.showCircularLoader = true;
        let loadPeopleData = watchListService.getPeopleList(offset, limit);
        loadPeopleData.then(function (successResp) {
            if (successResp.length > 0) {
                // $scope.watchlistPersons.personDetails = successResp;
                _.forEach(successResp, function (values) {
                    $scope.watchlistPersons.personDetails.push(values);
                })
                $scope.watchlistPersons.showCircularLoader = false;
                $scope.serverErrorinPersonlist = false;
            }
            else {
                $scope.stopInfiniteScroll = true;
            }


            $scope.$broadcast('scroll.infiniteScrollComplete');
            $scope.personPageOffset = $scope.personPageOffset + limit;
        }, function (error) {
            $scope.serverErrorinPersonlist = true;
            throw ('Error getting response from Server: \nError Code : ' + error.status + '\nMessage : ' + error.statusText);
        });
    };
    $scope.loadPersonImage = function (personID) {
        watchListService.getPersonImageforPersonID(personID)
            .then(function (response) {
                $scope.personImages = response.data;
                $scope.personImg = (!angular.isUndefined(response.data[0])) ? 'data:image/jpeg;base64,' + response.data[0].image : '';
                $scope.personImg2 = (!angular.isUndefined(response.data[1])) ? 'data:image/jpeg;base64,' + response.data[1].image : '';
                $scope.personImg3 = (!angular.isUndefined(response.data[2])) ? 'data:image/jpeg;base64,' + response.data[2].image : '';
            });
    };
    $scope.personParentWatchlist = '';
    $scope.setParentScreenNavigation = function (screen) {
        watchListService.setParentScreenNavigationService(screen);
        $scope.personParentWatchlist = screen;
    };
    $scope.getParentScreenNavigation = function () {
        return '#/app/watchlistMain/watchlistCommon/watchlistFullView/' + watchListService.getParentScreenNavigationService();
    };
// logic to show person details that was clicked from dashboard
    $scope.viewPersonDetails = {
        appSpecificAlert: {},
    };
    $scope.loadWatchlistAssociatedPersonDetails = function () {
        let personImage = watchListService.getPersonImageforPersonID($stateParams.personId);
        watchListService.getPersonDetailsforpersonid($stateParams.personId)
            .then(function (personResponse) {
                $scope.viewPersonDetails.appSpecificAlert.person = personResponse.data[0].name;
                personImage.then(function (personImg) {
                    // if(personImg.status ==200)
                    $scope.viewPersonDetails.appSpecificAlert.enrolledImage = (!angular.isUndefined(personImg.data[0])) ? 'data:image/jpeg;base64,' + personImg.data[0].image : 'images/default_user.png';
                    $scope.viewPersonDetails.appSpecificAlert.detectedImage = 'images/default_user.png';
                });
                $scope.frameImgNotAvailable = true;
                $scope.enrolledImgNotAvailable = true;
            });
    };
    $scope.selectedPersonDetail = function (person) {
        watchListService.getPersonDetailsforpersonid(person.personId)
            .then(function (personResponse) {
                $scope.person = personResponse.data[0];
                $scope.loadPersonImage(person.personId);
            });
    };
    $scope.loadUpdatedPersonDetail = function (personId) {
        var updatedPersonIndex = _.findIndex($scope.watchlistPersons.personDetails, ['personId', personId])
        let personForUpdate = $scope.watchlistPersons.personDetails[updatedPersonIndex];
        watchListService.getPersonDetailsforpersonid(personId)
            .then(function (personResponse) {
                $scope.watchlistIds = [];
                for (let watchInfo in personResponse.data[0].personWatchlistInfo) {
                    $scope.watchlistIds.push(personResponse.data[0].personWatchlistInfo[watchInfo].watchlistId);
                }
                delete personForUpdate['watchListIds'];
                personForUpdate['watchListIds'] = $scope.watchlistIds;
                $scope.watchlistPersons.personDetails[updatedPersonIndex] = personForUpdate;
                // $scope.$apply();
            })
    }
    $scope.loadTags = function (query) {
        return $http.get('data/tags.json');
    };


    // .......................Update watchlist association.......................

    $scope.loadPersonsWatchlistDetailsforEdit = function (personRecord) {
        $scope.selection = [];
        $scope.watchListSelectedForPeople = [];
        $scope.personIDToBeUpdated = personRecord.personId;
        $scope.selectedPerson = personRecord;
        $scope.selection = personRecord.watchListIds;
        $scope.originalAssociatedWatchlistIDs = personRecord.watchListIds != undefined ? personRecord.watchListIds : [];
        $scope.watchListSelectedForPeople = ($scope.selection) ? angular.copy($scope.selection) : [];
    };
    $scope.updatePersonWatchlistRelationship = function () {
        $scope.watchlists.showCircularLoader = true;
        // console.log($scope.originalAssociatedWatchlistIDs);
        // console.log($scope.originalAssociatedWatchlistIDs.sort());
        // console.log($scope.watchListSelectedForPeople);
        // console.log($scope.watchListSelectedForPeople.sort());
        // $scope.watchListSelectedForPeople.sort();
        // updation logic
        if (_.isEqual($scope.originalAssociatedWatchlistIDs.sort(), $scope.watchListSelectedForPeople.sort())) {
            // $scope.openPopup('personWatchlistmappingNotChangedPopup');
            return;
        }
        watchListService
            .updatePersonWatchlistRelationship($scope.originalAssociatedWatchlistIDs, $scope.watchListSelectedForPeople, $scope.personIDToBeUpdated)
            .then(function () {
                $scope.watchlists.showCircularLoader = false;
                $scope.closePopup('addToWatchlistPopup');
                $scope.showMessagePopup('personWatchlistmappingUpdated');
                $scope.getAllWatchListDetails();
                $timeout(function () {
                        $scope.loadUpdatedPersonDetail($scope.personIDToBeUpdated);
                    }, 5000);
            });
    };
    $scope.toggleSelection = function toggleSelection(watchlistId) {
        let index = _.findIndex($scope.watchListSelectedForPeople, function (val) {
            return val === watchlistId;
        });
        if (index != -1) {
            _.remove($scope.watchListSelectedForPeople, function (obj) {
                return watchlistId === obj;
            });
        } else {
            $scope.watchListSelectedForPeople.push(watchlistId);
        }

    };


    // .......................End Update watchlist association.......................

    $scope.personPageLoaded = 0;
    $scope.loadMorePersonData = function (offset, limit) {
        console.log($scope.personPageLoaded + "-" + $scope.personTotalPage)
        if ($scope.personPageLoaded < $scope.personTotalPage) {
            $scope.loadPersonData(offset, limit);
            $scope.personPageLoaded = $scope.personPageLoaded + 1;
        }
        // else{
        //
        // }

    }

    $scope.getPeopleCount = function () {
        let allPersonsForCount = watchListService.getAllPersonsForCount();
        allPersonsForCount.then(function (response) {
            $scope.personTotalPage = Math.ceil(response.data[0].count / $scope.personPageLimit);

            console.log($scope.personTotalPage)
        }, function (error) {
            $scope.serverErrorinWatchlistAll = true;
            throw ('Error getting response from Server: \nError Code : ' + error.status + '\nMessage : ' + error.statusText);
        });
    };
    function init() {
        if ($state.current.url === '/watchlist') {
            $scope.getAllWatchListDetails();
        }
        else if ($state.current.url === '/people') {
            $scope.getPeopleCount();
        }
        $scope.getAllWatchListDetails();
        $scope.loadPersonData($scope.personPageOffset, $scope.personPageLimit);
        $scope.getAllWatchListDetailsChild();

    }

    init();

    $scope.$on('$stateChangeSuccess', function () {
        console.log("state change")
    });

    let notificationListener = $rootScope.$on(appConstantService.TOPIC.FACE_MATCH, function (event, faceMatchmsg) {
        // loadLastDetectedImageOnUI(faceMatchmsg);
        console.log(faceMatchmsg)
    });
});

NEC.filter('searchForPersoninDashboard', function () {
    return function (arr, searchString) {
        if (!searchString) {
            return arr;
        }
        let result = [];
        searchString = searchString.toLowerCase();
        angular.forEach(arr, function (item) {
            if ((item.name).toLowerCase().indexOf(searchString) !== -1) {
                result.push(item);
            }
        });
        return result;
    };
});
