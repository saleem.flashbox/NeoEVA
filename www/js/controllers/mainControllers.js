NEC.controller('mainController', function ($scope, $rootScope, $location, $timeout, locationService, $cookies, $state, appService, dateUtilService, settingsService, webSocketService, loginService, appConstantService, mapService, watchListService) {
    // $scope.pageLoader=false;
    $scope.currentRout = function (path) {
        var loc = $location.path();
        return loc.includes(path);
    };
    // ........... map settings ..........
    $scope.mapActions = {
        locIndex: 0,
        floorIndex: null,
        allAreaMaps: [],
    };
    $scope.watchlist = {
        icon: {}
    };
    $scope.currentRout = function (path) {
        let loc = $location.path();
        return loc.includes(path);
    };
    $scope.openPopup = function (popup) {
        $(".popupBox").fadeOut(0).removeClass('open');
        $('.' + popup).fadeIn(0).addClass('open');
        $('body').scrollTop(0);
    };
    $scope.closePopup = function (popup, keepBackground) {
        $('.' + popup).fadeOut(0).removeClass('open');
    };
    $scope.showMessagePopup = function (popup) {
        $('.' + popup).fadeIn(300);
        $timeout(function () {
            $('.' + popup).fadeOut(300);
        }, 3000);
    };
    $scope.watchlistAlertDateFormat = function (date) {
        return moment(date).format(dateUtilService.DATE_FORMAT.ALERT_FACEMATCH_FORMAT);
    };
    $scope.showNotification = false;
    $scope.refresh = function () {
        $state.reload();
    };
    $scope.currentTime = moment().format('MM-DD-YYYY, hh:mm:ss a');
    var tick = function () {
        $scope.currentTime = moment().format('MM-DD-YYYY, hh:mm:ss a');
        $timeout.cancel($scope.mytimeout);
        $scope.mytimeout = $timeout(tick, 1000);
    };
    $scope.mytimeout = $timeout(tick, 1000);
    // ...... to hide and show loader ........
    // $scope.pageLoader=true;
    $rootScope.showLoader = function () {
        $scope.pageLoader = true;
    };
    $rootScope.hideLoader = function () {
        $scope.pageLoader = false;
    };


    $scope.show = function () {
        $scope.showMessagePopup('mapSettingsMsg');
    };
    $scope.showMenu = false;
    if (localStorage.hasOwnProperty('baseUrl')) {
        $scope.showMenu = true;
        settingsService.updateAppConfig(localStorage.getItem('baseUrl'));
    }

    $scope.loadSensorLocationInfo = function () {
        mapService.getSensorLocationDetail().then(function (sensorDetail) {
            $scope.sensorLocationDetail = sensorDetail;
            if (!$scope.$$phase)
                $scope.$apply();
        });
    };
    $scope.selectedWatchlist = '';
    $scope.watchlistDetailsObj = {};
    $scope.getAllWatchList = function () {
        watchListService.getAllWatchLists().then(function (response) {
            angular.forEach(response, function (value, key) {
                $scope.watchlistDetailsObj[value.watchlistId] = value;
                if (!$scope.$$phase)
                    $scope.$apply();
            });
        }, function (error) {
            $scope.serverErrorinWatchlistAll = true;
            throw ('Error getting response from Server: \nError Code : ' + error.status + '\nMessage : ' + error.statusText);
        });
    };
    $scope.matchPercentage = function (matchData) {
        return ((matchData) * 100).toFixed(0) + '%';
    };
    $scope.watchlistAlertDateFormat = function (date) {
        return moment(date).format(dateUtilService.DATE_FORMAT.ALERT_FACEMATCH_FORMAT);
    };
    $scope.loadNotificationRecords = function () {
        $scope.notificationRecords = appService.getNotificationRecords();
        $scope.notificationRecords_facematch = appService.getFaceMatchNotificationRecords();
        if (!$scope.$$phase)
            $scope.$apply();
    };
    $scope.selectAlert = function (alert) {
        $scope.selectedAlertData = alert;
    }
    $scope.showCameraName = function (cameraId) {
       // return  $scope.sensorLocationDetail ? $scope.sensorLocationDetail[cameraId].cameraName : '-';
    }

    function init() {
        $scope.getAllWatchList();
        $scope.loadSensorLocationInfo();
    }

    init();

    function createWebSocketConnection() {
        if (!webSocketService.isWebsocketClosed()) {
            webSocketService.registerWebsocket();
        }
    }

    $rootScope.$on(loginService.LOGIN.MSG, function (event, loginMsg) {
        if (loginService.LOGIN.STATUS.CONNECTED === loginMsg) {
            createWebSocketConnection();
        }
    });

    let notificationListener = $rootScope.$on(appConstantService.TOPIC.FACE_MATCH, function (event, faceMatchmsg) {
        appService.setNotificationRecords(faceMatchmsg, appConstantService.TOPIC.FACE_MATCH);
        // $scope.latestDetectedMsg = faceMatchmsg;
    });

// notification for other messages
    let liveInstanceFeedListener = $rootScope.$on(appConstantService.ALERT_TYPE.INSTANCE_FEED, function (arg, data) {
        appService.setNotificationRecords(data, appConstantService.ALERT_TYPE.INSTANCE_FEED);
    });

    $scope.$on('$destroy', function () {
        notificationListener();
        liveInstanceFeedListener();
    });
});


