/**
 * Created by MohammedSaleem on 13/03/17.
 */
(function () {
  'use strict';
  angular.module('NEC')
    .controller('generalSettingsCtrl', generalSettings);
  function generalSettings($scope, appConfig, $state, settingsService, $location, liveVideoFeedService) {

    console.log(localStorage.getItem('autoLogin'))
    $scope.config = {};
    $scope.config = {
      IP: (appConfig.baseUrl).split(':')[0],
      port: parseInt((appConfig.baseUrl).split(':')[1]),
      value1: (localStorage.getItem('autoLogin') === null || localStorage.getItem('autoLogin') === 'false') ? false : true
    }
    $scope.saveSettings = function () {
      // appConfig.protocol = $scope.config.protocol;
      appConfig.protocol = 'http://';
      appConfig.baseUrl = $scope.config.IP + ":" + $scope.config.port;
      appConfig.applicationBaseUrl = appConfig.protocol + appConfig.baseUrl + "/api"
      appConfig.loginApiBaseUrl = appConfig.protocol + appConfig.baseUrl + "/authorizationApi"
      settingsService.setBaseUrl(appConfig.baseUrl);
      settingsService.setProtocol(appConfig.protocol);
      localStorage.setItem('autoLogin', $scope.config.value1)

      localStorage.removeItem('username');
      localStorage.removeItem('password');
      loadSettingDetails();
      // $scope.showMessagePopup('settingsSavedMsg');
      $state.go('login');
    };

    function loadSettingDetails() {
      let protocol = $location.protocol();
      if (protocol === 'https') {
        settingsService.setIsEnabledHttp(true);
      }
      settingsService.getAppSettingDetails().then(function (resultDetails) {
        settingsService.setSettingDetails(resultDetails.data);
      }, function error(errResponse) {
        console.log('cannot get settings config');
      });

      liveVideoFeedService.setAllVideoFeedDetailsForAllApps();
    }

    $scope.cancel = function () {
      history.back();
    }
  }
})();
