/**
 * Created by suhas on 25/5/17.
 */
NEC.factory('analyticsPollerService', function($http, $interval, analyticsService, $rootScope, appConstantService) {
    let that = this;
    const CONSTANTS={
        ANALYTICS_GRANULARITY: {
            AGE_GENDER: '24h',
            CROWD: '1h',
            PEOPLE: '1h',
        },
        INTERVAL_TIMER: 15000,
        ANALYTICS_PERIOD_IN_MINUTES: 5,
    };

    let sensorId ='';

    that.startAllPoller = function() {
        _.each(analyticsPollerDetails, function(applicationDetails) {
            if(!applicationDetails.pollerId) {
                that.startPollerByAppName(applicationDetails.name);
            }
        });
    };

    that.stopAllPoller = function() {
        _.each(analyticsPollerDetails, function(applicationDetails) {
            if(applicationDetails.pollerId) {
                $interval.cancel(applicationDetails.pollerId);
                applicationDetails.pollerId=null;
            }
        });
    };
    that.stopPollerByAppName=function(appName) {
        _.find(analyticsPollerDetails, function(applicationDetails) {
            if(applicationDetails.name===appName) {
                if(applicationDetails.pollerId) {
                    $interval.cancel(applicationDetails.pollerId);
                    applicationDetails.pollerId = null;
                }
            }
        });
    };
    that.setSensorIdForAggregation=function(sensorIdSelected) {
        sensorId = sensorIdSelected;
    };
    that.getAgeAndGenderAnalytics = function(startDate, endDate, sensorId) {
        analyticsService.getAnalyticsGraphDataByAlertType(startDate, endDate,
            CONSTANTS.ANALYTICS_GRANULARITY.AGE_GENDER, sensorId,
            appConstantService.AGGREGATOR_ALERTTYPE.AGE_GENDER, true)
            .then(function(data) {
                let listenerName = appConstantService.TOPIC.ANALYTICS_DASHBOARD_AGE_GENDER;
                emitAnalyticsDashboardData(listenerName, data);
            });
    };

    that.getCrowdAnalytics = function(startDate, endDate, sensorId) {
        analyticsService.getAnalyticsGraphDataByAlertType(startDate, endDate, '', sensorId,
            appConstantService.AGGREGATOR_ALERTTYPE.CROWD, true)
            .then(function(data) {
                let listenerName = appConstantService.TOPIC.ANALYTICS_DASHBOARD_CROWD;
                emitAnalyticsDashboardData(listenerName, data);
            });
    };

    that.getPeopleInAndOutAnalytics = function(startDate, endDate, sensorId) {
        analyticsService.getAnalyticsGraphDataByAlertType(startDate, endDate, '', sensorId,
            appConstantService.AGGREGATOR_ALERTTYPE.PEOPLE_IN, true)
            .then(function(data) {
                let listenerName = appConstantService.TOPIC.ANALYTICS_DASHBOARD_PEOPLE;
                emitAnalyticsDashboardData(listenerName, data);
            });
    };


    let analyticsPollerDetails = [
        {method: that.getAgeAndGenderAnalytics, pollerId: null, name: appConstantService.app[1].NAME},
        {method: that.getCrowdAnalytics, pollerId: null, name: appConstantService.app[2].NAME},
        {method: that.getPeopleInAndOutAnalytics, pollerId: null, name: appConstantService.app[3].NAME},
    ];
    that.startPollerByAppName=function(appName) {
        let startDate = moment().startOf('day');
        let endDate =moment();
        _.each(analyticsPollerDetails, function(applicationDetails) {
            if(applicationDetails.name==appName) {
                if(!applicationDetails.pollerId) {
                    applicationDetails.pollerId = $interval(function() {
                        applicationDetails.method(startDate, endDate, sensorId);
                    }, CONSTANTS.INTERVAL_TIMER);
                }
            }
        });
    };

    function emitAnalyticsDashboardData(listenerName, data) {
        $rootScope.$broadcast(listenerName, data);
    }

    return that;
});
