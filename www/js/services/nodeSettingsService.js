/**
 * Created by dhanalakshmi on 3/2/17.
 */
NEC.factory('nodeSettingsService', function($http, appConfig, $q) {
    let that = this;
    let nodeDetailsArray = [];

    that.getAllNodes = function() {
        return new $q(function(resolve, reject) {
            if (nodeDetailsArray.length > 0) {
                resolve(nodeDetailsArray);
            } else {
                $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/nodes')
                    .then(function(data) {
                        nodeDetailsArray = [];
                        nodeDetailsArray = data;
                        resolve(data);
                    });
            }
        });
    };
    that.setNodes = function(nodeDetails) {
        nodeDetailsArray = nodeDetails;
    };
    that.getNodes = function() {
        return nodeDetailsArray;
    };
    return that;
});
