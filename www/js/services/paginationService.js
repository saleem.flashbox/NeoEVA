/**
 * Created by suhas on 14/2/17.
 */
NEC.factory('paginationService', function($q) {
    let that = this;
    let paginationDetails = {
        maxDocPerPage: 10,
        maxNoOfPages: 10,
        totalDocs: 0,
        pageSelected: 1,
        noOfPagesToBeDisplayed: 10,
        totalNoOfPages: 0,
        reportList: [],
        dataList: [],
    };
    that.setPaginationInitDetails = function(maxDocPerPage, maxNoOfPages, noOfPagesToBeDisplayed, reportList) {
        paginationDetails.maxDocPerPage = maxDocPerPage;
        paginationDetails.maxNoOfPages = maxNoOfPages;
        paginationDetails.noOfPagesToBeDisplayed = noOfPagesToBeDisplayed;
        paginationDetails.reportList = reportList;
        paginationDetails.dataList = reportList;
        paginationDetails.totalDocs = reportList.length;
    };
    that.getPaginationDetails = function() {
        return new $q(function(resolve, reject) {
            paginationDetails.totalNoOfPages = parseInt(paginationDetails.totalDocs) / paginationDetails.maxDocPerPage;
            let roundedTotalPageNo = Math.round(parseInt(paginationDetails.totalDocs) / paginationDetails.maxDocPerPage);
            if (paginationDetails.totalNoOfPages > roundedTotalPageNo) {
                paginationDetails.totalNoOfPages = roundedTotalPageNo + 1;
            } else if (paginationDetails.totalNoOfPages < 1) {
                paginationDetails.totalNoOfPages = 1;
            } else if (paginationDetails.totalNoOfPages < roundedTotalPageNo) {
                paginationDetails.totalNoOfPages = roundedTotalPageNo;
            }
            if (paginationDetails.totalNoOfPages <= paginationDetails.maxNoOfPages) {
                paginationDetails.noOfPagesToBeDisplayed = range(paginationDetails.totalNoOfPages);
            } else {
                paginationDetails.noOfPagesToBeDisplayed = range(paginationDetails.maxNoOfPages);
            }
            resolve(paginationDetails);
        });
    };
    that.getDataListForPage = function(pageNo, dataListForDisplay) {
        return new $q(function(resolve, reject) {
            if (dataListForDisplay.length > 0) {
                let start = ((pageNo - 1) * paginationDetails.maxDocPerPage);
                let end = start + paginationDetails.maxDocPerPage;
                if (end >= dataListForDisplay.length) {
                    end = dataListForDisplay.length;
                }
                if (start < dataListForDisplay.length) {
                    let data = dataListForDisplay.slice(start, end);
                    resolve(data);
                }
            } else {
                resolve(dataListForDisplay);
            }
        });
    };
    function range(n) {
        return new Array(n);
    }

    return that;
});
