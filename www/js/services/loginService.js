/**
 * Created by dhanalakshmi on 27/2/17.
 */
NEC.factory('loginService', function($http, appConfig) {
    const LOGIN = {
        STATUS: {
        CONNECTED: 'CONNECTED',
        DISCONNECTED: 'DISCONNECTED',
        },
        MSG: 'LOGIN_ALERT',
    };
    let that = this;
    this.LOGIN = LOGIN;
    that.getTokenForAuthorizedUser = function(userDetails) {
     //   return $http.get(appConfig.applicationBaseUrl + "/pulseExecutionFramework/token?username=" + userDetails.username + "&password=" + userDetails.password);
        let userCred = {
            username: userDetails.username,
            password: userDetails.password,
        };
        return $http.post(appConfig.loginApiBaseUrl+'/authentication/signIn', userCred);
    };

    that.logout = function(token) {
       $http.delete(appConfig.loginApiBaseUrl+'/authentication/signOut/'+token);
    };
    that.getApplicationVersion = function() {
       return $http.get('data/appDetails.json');
    };
    return that;
});
