NEC.factory('appService', function($rootScope, settingsService, appConstantService, sensorService, $q, dateUtilService) {
    let that = this;

    const APP_LOGO_PATH ='../images/LOGO.png';
    let dataNotificationSet = [];
    let dataNotificationSetforFaceMatch = [];
    let systemMsg = {
        alert_Type: '',
        msg: '',
        timestamp: '',
    };
    that.setNotificationRecords = function(dataRecord, msgType) {
        if (msgType == appConstantService.ALERT_TYPE.INSTANCE_FEED) {
            systemMsg.alert_Type = msgType;
            systemMsg.msg = dataRecord;
            if ((dataNotificationSet.length + 1) > settingsService.getSettingDetails().notificationDataSetSize)
                dataNotificationSet.pop();
            systemMsg.timestamp = moment.utc().format(dateUtilService.DATE_FORMAT.ALERT_MANAGER);
            dataNotificationSet.unshift(cloneMessage(systemMsg));
        } else {
            if ((dataRecord == undefined || dataRecord == null || dataRecord.appSpecificAlert == undefined ))
                return;
            if ((dataRecord.appSpecificAlert.score > parseFloat(settingsService.getSettingDetails().thresholdScore) )
                && (dataNotificationSet.length + 1) > settingsService.getSettingDetails().notificationDataSetSize) {
                dataNotificationSet.pop();
            }
            if (dataRecord.appSpecificAlert.score > parseFloat(settingsService.getSettingDetails().thresholdScore)) {
                dataNotificationSet.unshift(dataRecord);
            }
            if (msgType == appConstantService.TOPIC.FACE_MATCH) {
                if (dataRecord.appSpecificAlert.score > parseFloat(settingsService.getSettingDetails().thresholdScore)
                    && (dataNotificationSet.length + 1) > settingsService.getSettingDetails().notificationDataSetSize) {
                    dataNotificationSetforFaceMatch.pop();
                }
                if (dataRecord.appSpecificAlert.score > parseFloat(settingsService.getSettingDetails().thresholdScore)) {
                    dataNotificationSetforFaceMatch.unshift(dataRecord);
                }
            }
           }
    };
    function cloneMessage(servermessage) {
        let clone ={};
        for( let key in servermessage ) {
            if(servermessage.hasOwnProperty(key)) // not adding inherited props
                clone[key]=servermessage[key];
        }
        return clone;
    }
    that.getNotificationRecords = function() {
        return dataNotificationSet;
    };
    that.resetNotificationData = function() {
        dataNotificationSet.length = 0;
    };
    that.getFaceMatchNotificationRecords = function() {
        return dataNotificationSetforFaceMatch;
    };
    that.getEncodedLogoImg = function() {
        let applicationLogoPath = APP_LOGO_PATH;
        return new $q(function(resolve, reject) {
            toDataUrl(applicationLogoPath, function(myBase64) {
                 resolve(myBase64);// myBase64 is the base64 string
            });
        });
       };

    function toDataUrl(url, callback) {
        let xhr = new XMLHttpRequest();
        xhr.onload = function() {
            let reader = new FileReader();
            reader.onloadend = function() {
                callback(reader.result);
            };
            reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();
    }

    that.saveLogoImage = function(form) {
        return $q(function(resolve) {
            let data = new FormData(form);
            $.ajax({
                type: 'POST',
                enctype: 'multipart/form-data',
                url: '/settings/file/upload',
                data: data,
                processData: false, // prevent jQuery from automatically transforming the data into a query string
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function(data) {
                    console.log('SUCCESS : ', data);
                    resolve('SUCCESS');
                },
                error: function(e) {
                    console.log('ERROR : ', e);
                    resolve('FAILURE');
               },
            });
        });
    };


    return that;
});
