/**
 * Created by alok on 26/2/17.
 */

NEC.factory('webSocketService', function($rootScope, $location, appConstantService, appConfig) {
    // initialiseWebsocket(); // will initialise on refresh.
    let that = this;
    let protocol = $location.protocol();
    let wsProtocol = 'wss://';
    that.registerWebsocket = function() {
        if (protocol === 'http') {
            wsProtocol = 'ws://';
        }
        initialiseWebsocket();
    };
    that.deregisterWebsockets = function() {
        if (that.websocket) {
            that.websocket.close();
        }
        that.websocket = null;
    };

    that.isWebsocketClosed = function() {
        return (!that.websocket)? true:false;
    };
    that.reconnectWebSocket = function() {
        if (!that.websocket) {
           that.registerWebsocket();
        }
    };

    function initialiseWebsocket() {
        if(appConfig.platform === 'mobile') {
            // temp. code for wsProtocol
            wsProtocol = 'ws://';
            that.websocket = new WebSocket(wsProtocol + appConfig.baseUrl  +  appConstantService.WEB_SOCKET_END_POINT.ALERT);
        }else{
            that.websocket = new WebSocket(wsProtocol + '163.172.172.74' + ':' + '3002' + appConstantService.WEB_SOCKET_END_POINT.ALERT);

        }
        that.websocket.onopen = function(evt) {
            onOpen(evt);
        };
        that.websocket.onclose = function(evt) {
            onClose(evt);
        };
        that.websocket.onmessage = function(evt) {
            onMessage(evt);
        };
        that.websocket.onerror = function(evt) {
            onError(evt);
        };
    }


    function onOpen(evt) {
    }

    function onClose(evt) {
        that.deregisterWebsockets();
    }

    function onMessage(msg) {
        if (!angular.isUndefined(msg.data) && msg.data.length > 0) {
            messageRouter(msg);
        }
    }

    function onError(evt) {
        console.log('Error in websocket ' + evt);
    }


    function messageRouter(msg) {
        let msgData = JSON.parse(msg.data);
        if (msgData.alertType) {
            $rootScope.$emit(msgData.alertType, msgData);
        } else if (msgData._type === appConstantService.ALERT_TYPE.INSTANCE_FEED) {
            $rootScope.$emit(msgData._type, msgData.message);
        }
    }
    return that;
});
