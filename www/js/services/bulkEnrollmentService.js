/**
 * Created by alok on 21/8/17.
 */
NEC.factory('bulkEnrollmentService', function($http, appConfig, $q, $timeout) {
    'use strict';
    let that = this;

    const batch_Size = 10;
    that.batch_Size = batch_Size;
    that.updatingWatchlistMapping = false;
    that.personIDArray =[];
    that.bulkdata = {
        totalRecords: 0,
        recordsUploaded: 0,
        recordsFailed: 0,
    };
    that.WatchlistRequestCtr = 0;
    that.bulkDataReset = function() {
        that.bulkdata = {
            totalRecords: 0,
            recordsUploaded: 0,
            recordsFailed: 0,
        };
    };
    that.getWatchlistMappingUpdateStatus = function(){
        return that.updatingWatchlistMapping;
    }
    that.getDataUploadProgress = function() {
        return that.bulkdata.recordsUploaded;
    };
    that.getTotalRecords = function() {
        return that.bulkdata.totalRecords;
    };
    that.updateTotalRecordsToBeUploaded = function(totalRecords) {
        that.bulkdata.totalRecords = totalRecords;
    };
    that.resetTotalRecords = function() {
        that.bulkdata.recordsUploaded =0;
        that.bulkdata.totalRecords =0;
    };
    that.updateTotalRecordsUploadedSuccessfully = function(uploadedRecords) {
        that.bulkdata.recordsUploaded += uploadedRecords;
    };
    that.bulkPersonUpload =function(bulkPersonData, bulkAssociatedWatchlists) {
        that.updateTotalRecordsToBeUploaded(bulkPersonData.length);
        let arrPersonURIForBulkUpload =[];
        for(let i = 0; i < bulkPersonData.length; i+=that.batch_Size) {
            arrPersonURIForBulkUpload.length = 0;
            arrPersonURIForBulkUpload.push($http.post(appConfig.applicationBaseUrl + '/biometrics/persons', bulkPersonData.slice(i, i+that.batch_Size)));
        // build report for success-failure
        let iPersonCtr =0;
        let arrPersonUploadResponseAll = [];
        $q.all(arrPersonURIForBulkUpload).then(function(ret) {
            arrPersonUploadResponseAll.push(buildResponseForReport(ret[iPersonCtr].data.Server_response));
            if(bulkAssociatedWatchlists.length > 0) {
                that.WatchlistRequestCtr ++;
                associatedWatchlistWithCreatedPerson(that.personIDArray, bulkAssociatedWatchlists);
            }
            that.personIDArray.length = 0;
            that.updateTotalRecordsUploadedSuccessfully(arrPersonUploadResponseAll[0].length);
        });
    }
    };
    function buildResponseForReport(responseStruct) {
        let respReport = [];
        let respInJSON = JSON.parse(responseStruct);
        _.each(respInJSON, function(personResponseNode) {
            respReport.push(' Record created successfully for '+personResponseNode.firstName+' '+personResponseNode.lastName+' with personID '+personResponseNode.personId);
            that.personIDArray.push(personResponseNode.personId);
        });
        return respReport;
    }
    function associatedWatchlistWithCreatedPerson(personIDList, watchlistData) {
        let arrWatchlistMemeber = [];
         _.each(watchlistData, function(watchlistNode) {
            arrWatchlistMemeber.push($http.post(appConfig.applicationBaseUrl + '/biometrics/watchlists/' + watchlistNode + '/members', formatPersonArrayAsNode(personIDList)));
            that.updatingWatchlistMapping = true;
        });
        $q.all(arrWatchlistMemeber).then(function(ret) {
            that.WatchlistRequestCtr --;
            that.updatingWatchlistMapping = (that.WatchlistRequestCtr >= 1);
      });
    }
    function formatPersonArrayAsNode(personIDArray) {
        let personArrayasMembers =[];
        _.each(personIDArray, function(personIDNode) {
            personArrayasMembers.push({'personId': personIDNode});
        });
        return personArrayasMembers;
    }
    return that;
});
