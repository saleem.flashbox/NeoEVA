NEC.factory('appConstantService', function() {
    // 5m, 10m, 1h, 6h, 24h, 2d, 7d
    const SECOND = 1000;
    const MINUTE = 60 * SECOND;
    const HOUR = 60 * MINUTE;
    const DAY = 24 * HOUR;
    const VALID_MAX_DATE_RANGE = 364 * DAY;
    const ALERT_TYPE = {
        AGE_GENDER: 'gender-age',
        CROWD: 'crowd-count',
        PEOPLE_OUT: 'people-counting-out',
        PEOPLE_IN: 'people-counting-in',
        FACE_MATCH: 'facematch',
        INSTANCE_FEED: 'system',
    };
    const AGGREGATOR_ALERTTYPE = {
            AGE_GENDER: 'gender-age',
            CROWD: 'crowd-count',
            PEOPLE: {
                PEOPLE_OUT: 'people-count-out',
                PEOPLE_IN: 'people-count-in',
            },
            FACE_MATCH: 'watchlist-matches',
            DEFAULT_GRANULARITY: '1h',
            DEFAULT: {
                GRANULARITY: {
                    DASHBOARD: {
                        AGE_GENDER: '24h',
                        CROWD: '1h',
                        PEOPLE: '1h',
                    },
                    NO_OF_X_AXISCOORDINATES: 10,
                    APP: {
                        AGE_GENDER: '30d',
                    },

                },
            },
            DASHBOARD: {
                APPS: {
                    'People': ['people-count-in', 'people-count-out'],
                    'Watchlist': ['watchlist-matches'],
                    'Crowd': ['crowd-count'],
                    'Age-Gender': ['gender-age-male', 'gender-age-female'],
                },
                SNAPSHOT: {
                    GRANUALRITY: '5m',
                    START_END_TIME_DIFF: 6 * MINUTE,
                },
                threshold: {
                    Crowd: [
                        {min: 0, max: 2, colour: 'green'},
                        {min: 2, max: 5, colour: 'yellowishGreen'},
                        {min: 5, max: 10, colour: 'yellow'},
                        {min: 10, max: 15, colour: 'orange'},
                        {min: 15, max: 15, colour: 'red'},
                    ],
                    People: [
                        {min: 0, max: 5, colour: 'green'},
                        {min: 5, max: 10, colour: 'yellowishGreen'},
                        {min: 10, max: 15, colour: 'yellow'},
                        {min: 15, max: 30, colour: 'orange'},
                        {min: 30, max: 30, colour: 'red'},
                    ],
                },
            },
        };

     const DATE_VALIDATION = {
            OK: 'validdate',
            ENDDATE_LESS_THAN_STARTDATE: 'End Date is less than Start Date',
            DATE_RANGE_EXCEEDS_ALLOWED_RANGE: 'Date range should not exceed Year',
        };

    const LIVE_FEED = {
        VIDEO_RESOLUTION: {
            HIGH: 'width=1280&height=960',
            MED: 'width=640&height=360',
            LOW: 'width=470&height=264',
        },
    };
    const APP = {
        PEOPLE: {
            NAME: 'people',
            ALERT_TYPE: {
                OUT: 'people-counting-out',
                IN: 'people-counting-in',
            },
        },
        AGE_GENDER: {
            NAME: 'Age-Gender',
            ALERT_TYPE: {
                MALE: 'gender-age-male',
                FEMALE: 'gender-age-female',
            },
            CROWD: {
                NAME: 'Crowd',
                ALERT_TYPE: {
                    CROWD: 'crowd-count',
                },
            },
            WATCHLIST: {
                NAME: 'Watchlist',
                ALERT_TYPE: {
                    CROWD: 'watchlist-matches',
                },
            },
        },
    };
    return {
        TOPIC: {
            AGE_GENDER: ALERT_TYPE.AGE_GENDER,
            CROWD: ALERT_TYPE.CROWD,
            PEOPLE_OUT: ALERT_TYPE.PEOPLE_OUT,
            PEOPLE_IN: ALERT_TYPE.PEOPLE_IN,
            FACE_MATCH: ALERT_TYPE.FACE_MATCH,
            CROWD_SNAP_SHOT: 'crowd-snapshot',
            PEOPLE_SNAP_SHOT: 'people-counting',
            APP_SENSOR_THRESHOLD: 'app_sensor_threshold_',
            ANALYTICS_DASHBOARD_PEOPLE: 'analytics_dashboard_people',
            ANALYTICS_DASHBOARD_CROWD: 'analytics_dashboard_crowd',
            ANALYTICS_DASHBOARD_AGE_GENDER: 'analytics_dashboard_age_gender',
        },
        WEB_SOCKET_END_POINT: {
            ALERT: '/ws/alert',
            CROWD_SNAP_SHOT: '/ws/crowdSnapShot',
            PEOPLE_SNAP_SHOT: '/ws/peopleSnapShot',
        },

        GRANULARITY: 30,
        app: [{
            id: 1,
            alertType: ALERT_TYPE.FACE_MATCH,
            NAME: 'Watchlist',
            aggregatorAlertType: AGGREGATOR_ALERTTYPE.FACE_MATCH,
            graph: {
                analytics: {
                    legend: ['Alert Count'],
                },

            },
        },
            {
                id: 2,
                alertType: ALERT_TYPE.AGE_GENDER,
                NAME: 'Age-Gender',
                aggregatorAlertType: AGGREGATOR_ALERTTYPE.AGE_GENDER,
                graph: {
                    analytics: {
                        legend: ['Age-Gender Count'],
                    },
                },
            },
            {id: 3,
                alertType: ALERT_TYPE.CROWD,
                NAME: 'Crowd',
                aggregatorAlertType: AGGREGATOR_ALERTTYPE.CROWD,
                graph: {
                    analytics: {
                        legend: ['Crowd Count'],
                    },
                },
            },
            {
                id: 4,
                alertType: ALERT_TYPE.PEOPLE_OUT,
                NAME: 'People',
                aggregatorAlertType: AGGREGATOR_ALERTTYPE.PEOPLE_OUT,
                graph: {
                    analytics: {
                        legend: ['People-In'],
                    },
                },
            },
            {
                id: 5,
                alertType: ALERT_TYPE.PEOPLE_IN,
                NAME: 'People',
                aggregatorAlertType: AGGREGATOR_ALERTTYPE.PEOPLE_IN,
                graph: {
                    analytics: {
                        legend: ['People-Out'],
                    },
                },
            },
        ],
        sensorTypes: {
            camera: 'camera',
        },
        ANALYTICS_GRAPH: {
            SERIES_TYPE: {
                MULTIPLE: 'multiple',
                SINGLE: 'single',
            },
        },
        ALERT_TYPE: ALERT_TYPE,
        LIVE_FEED: LIVE_FEED,
        AGGREGATOR_ALERTTYPE: AGGREGATOR_ALERTTYPE,
        // GRANUALRITY_SLOTS: GRANUALRITY_SLOTS,
        VALID_MAX_DATE_RANGE: VALID_MAX_DATE_RANGE,
        Genders: {FEMALE: 'FEMALE', MALE: 'MALE'},
        intervalTimer: {overview: 60000 / 4, THRESHOLD: 15000},
        ANALYTICS_GRAPH: {
            SERIES_TYPE: {
                MULTIPLE: 'multiple',
                SINGLE: 'single',
            },
        },
      };
});
