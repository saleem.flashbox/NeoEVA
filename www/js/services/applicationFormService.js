/**
 * Created by dhanalakshmi on 25/4/17.
 */
NEC.factory('appFormService', function($http, appConfig) {
    let that = this;
    const APPLICATION_MSG = {
        ERROR: {
            APPLICATION_CONFIG_MISSING_ERROR: 'Application_Configuration_Missing',
        },
    };
    that.APPLICATION_MSG= APPLICATION_MSG;
    let appConfiguration ='';
    that.getAppDetails = function(appId) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/apps/' + appId);
    };
    that.saveInstance = function(instanceObj) {
        return $http.post(appConfig.applicationBaseUrl + '/pulseExecutionFramework/instances/', instanceObj);
    };
    that.setAppConfigurationType = function(configurationType) {
        appConfiguration = configurationType;
    };
    that.getAppConfigurationType = function() {
        return appConfiguration;
    };
    that.parseApplicationConfigurationDataTypeAndSettingType = function(configurationForm, isBasicConfiguration) {
        let advancedFormWithData = configurationForm.output;
        let formWithNodeElementDetails = configurationForm.fullOutput;
        _.each(advancedFormWithData, function(advancedFormDetails, key) {
            _.each(formWithNodeElementDetails, function(elementDetails) {
                if (elementDetails.appInstance === key) {
                    filterAdvancedElements(elementDetails.config, advancedFormDetails, isBasicConfiguration);
                }
            });
        });
    };
    function stringArrayToIntArray(arrStr) {
        if(typeof(arrStr) === 'object') {
            return arrStr;
        }
        if((arrStr.indexOf('[') > -1) || (arrStr.indexOf(']') >-1))
        arrStr = arrStr.substring(arrStr.indexOf('[')+1, arrStr.indexOf(']'));
        let dataNoBrace = arrStr.split(',');
        return _.map(dataNoBrace, _.ary(parseInt, 1));
    }
    function filterAdvancedElements(nodeMetaDetails, applicationInstanceForm, isBasicConfiguration) {
        _.each(applicationInstanceForm, function(applicationFormNodeItem, applicationFormNodeKey) {
                 let node = _.find(nodeMetaDetails, {name: applicationFormNodeKey});
                if(node) {
                if (node.type === 'boolean') {
                    if (typeof(applicationInstanceForm[applicationFormNodeKey]) === 'string') {
                        applicationInstanceForm[applicationFormNodeKey] =
                            (applicationInstanceForm[applicationFormNodeKey] === 'true');
                    } else if(typeof(applicationInstanceForm[applicationFormNodeKey]) === 'object') {
                        applicationInstanceForm[applicationFormNodeKey] = (Boolean)(applicationInstanceForm[applicationFormNodeKey]);
                    }
                }
                if (node.type === 'array[int32]') {
                    applicationInstanceForm[applicationFormNodeKey] =
                        stringArrayToIntArray(applicationInstanceForm[applicationFormNodeKey]);
                }
                if (node.type === 'object' || node.name === 'opt') {
                    applicationInstanceForm[applicationFormNodeKey] =
                        ((typeof applicationInstanceForm[applicationFormNodeKey]) === 'string') ?
                            JSON.parse(applicationInstanceForm[applicationFormNodeKey]) :
                            applicationInstanceForm[applicationFormNodeKey];
                }
                if (node.settingLevel === 'advanced' && isBasicConfiguration)
                    delete applicationInstanceForm[applicationFormNodeKey]; // delete node if it is basic configuration
            }
        });
     }
    return that;
});
