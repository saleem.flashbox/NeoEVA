NEC.factory('alertService', function($http, appConfig, appConstantService, $q) {
    let that = this;

    that.getCountforAlertType = function(alertType, startTime, endTime, sensorId, instanceId, granularity, filterVal, limit) {
        let alertURI = appConfig.applicationBaseUrl + '/alertStatAggregator/timeseries/' + alertType;
        // if(alertType) alertURI += alertURI.indexOf("?") == -1 ? "?alertType="+alertType+"&":"alertType="+alertType+"&";
        if (startTime) {
            alertURI += alertURI.indexOf('?') === -1 ? '?start=' + startTime + '&' : 'start=' + startTime + '&';
        }
        if (endTime) {
            alertURI += alertURI.indexOf('?') === -1 ? '?end=' + endTime + '&' : 'end=' + endTime + '&';
        }
        if (instanceId) {
            alertURI += alertURI.indexOf('?') === -1 ? '?instanceId=' + instanceId + '&' : 'instanceId=' + instanceId + '&';
        }
        if (sensorId) {
            alertURI += alertURI.indexOf('?') === -1 ? '?sensors=' + sensorId + '&' : 'sensors=' + sensorId + '&';
        }
        if (limit) {
            alertURI += alertURI.indexOf('?') === -1 ? '?limit=' + limit + '&' : 'limit=' + limit + '&';
        }
        // defaulting granularity
        granularity = (granularity != undefined && granularity != '' & granularity != null) ? granularity : appConstantService.AGGREGATOR_ALERTTYPE.DEFAULT_GRANULARITY;
        if (granularity) {
            alertURI += alertURI.indexOf('?') == -1 ? '?granularity=' + granularity + '&' : 'granularity=' + granularity + '&';
        }
        if (filterVal) {
            alertURI += alertURI.indexOf('?') == -1 ? '?filter=' + filterVal + '&' : 'filter=' + filterVal + '&';
        }
        alertURI = alertURI.indexOf('&') == -1 ? alertURI : alertURI.substring(0, alertURI.length - 1);
        alertURI = encodeURI(alertURI);
        return $http.get(alertURI);
    };
        that.getDataForAlertType = function(alertType, startTime, endTime, sensorId, instanceId, offset, limit, filterVal) {
            let alertURI = appConfig.applicationBaseUrl + '/alert/alerts';
            if (alertType) {
                alertURI += alertURI.indexOf('?') === -1 ? '?alertType=' + alertType + '&' : 'alertType=' + alertType + '&';
            }
            if (startTime) {
                alertURI += alertURI.indexOf('?') === -1 ? '?startTime=' + startTime + '&' : 'startTime=' + startTime + '&';
            }
            if (endTime) {
                alertURI += alertURI.indexOf('?') === -1 ? '?endTime=' + endTime + '&' : 'endTime=' + endTime + '&';
            }
            if (instanceId) {
                alertURI += alertURI.indexOf('?') === -1 ? '?instanceId=' + instanceId + '&' : 'instanceId=' + instanceId + '&';
            }
            if (sensorId) {
                alertURI += alertURI.indexOf('?') === -1 ? '?sensorId=' + sensorId + '&' : 'sensorId=' + sensorId + '&';
            }
            if (offset) {
                alertURI += alertURI.indexOf('?') === -1 ? '?offset=' + offset + '&' : 'offset=' + offset + '&';
            }
            if (limit) {
                alertURI += alertURI.indexOf('?') === -1 ? '?limit=' + limit + '&' : 'limit=' + limit + '&';
            }
            if (filterVal) {
                alertURI += alertURI.indexOf('?') === -1 ? '?filter=' + filterVal + '&' : 'filter=' + filterVal + '&';
            }
            alertURI = alertURI.indexOf('&') === -1 ? alertURI : alertURI.substring(0, alertURI.length - 1);
            alertURI = encodeURI(alertURI);
            return $http.get(alertURI);
        }; /* offset="+offset+"&limit="+limit+"&startTime="+startDate+"&alertType="+alertType+"&endTime="+endDate*/
    that.getDataForAlertTypeCountTypeSummary = function(alertType, startTime, endTime, sensorId, instanceId, offset, limit, filterVal) {
        let alertURI = appConfig.applicationBaseUrl + '/alert/alerts/summary';
        if (alertType) alertURI += alertURI.indexOf('?') === -1 ? '?alertType=' + alertType + '&' : 'alertType=' + alertType + '&';
        if (startTime) alertURI += alertURI.indexOf('?') === -1 ? '?startTime=' + startTime + '&' : 'startTime=' + startTime + '&';
        if (endTime) alertURI += alertURI.indexOf('?') === -1 ? '?endTime=' + endTime + '&' : 'endTime=' + endTime + '&';
        if (instanceId) alertURI += alertURI.indexOf('?') === -1 ? '?instanceId=' + instanceId + '&' : 'instanceId=' + instanceId + '&';
        if (sensorId) alertURI += alertURI.indexOf('?') === -1 ? '?sensorId=' + sensorId + '&' : 'sensorId=' + sensorId + '&';
        if (offset) alertURI += alertURI.indexOf('?') === -1 ? '?offset=' + offset + '&' : 'offset=' + offset + '&';
        if (limit) alertURI += alertURI.indexOf('?') === -1 ? '?limit=' + limit + '&' : 'limit=' + limit + '&';
        if (filterVal) alertURI += alertURI.indexOf('?') === -1 ? '?filter=' + filterVal + '&' : 'filter=' + filterVal + '&';
        alertURI = alertURI.indexOf('&') === -1 ? alertURI : alertURI.substring(0, alertURI.length - 1);
        alertURI = encodeURI(alertURI);
        return $http.get(alertURI);
    };
    that.getGranularityForSingleRecord = function(startDate, endDate) {
        return that.getSingleGranularityInParsedFormatforDateRange(startDate, endDate);
    };
    that.getAlertCountForWatchlistPagination = function(alertType, startDate, endDate, sensorId) {
       return new $q(function(resolve, reject) {
           that.getCountforAlertType(alertType, startDate, endDate, sensorId, null, that.getGranularityForSingleRecord(startDate, endDate))
               .then(function(resp) {
                   console.log('received response' + resp.data[0].y[0]);
                   resolve(resp.data[0].y[0]);
               });
       });
    };
    that.getSingleGranularityInParsedFormatforDateRange = function(startDate, endDate) {
        return that.getSingleGranularityBetweenStartDateAndEndDate(startDate, endDate);
    };
    that.getSingleGranularityBetweenStartDateAndEndDate = function(startDate, endDate) {
        let parseConvertedDate = that.getTimeDifferenceBetweenStartDateAndEndDate(startDate, endDate).split(' ');
        return ((parseInt(parseConvertedDate[0])+2)+parseConvertedDate[1]);
    };
    that.getTimeDifferenceBetweenStartDateAndEndDate= function(startDate, endDate) {
        let timeDifferenceInMilli = (moment(endDate).utc() - moment(startDate).utc());
        return dateConversion(timeDifferenceInMilli);
    };
    function dateConversion(millseconds) {
        let oneSecond = 1000;
        let oneMinute = oneSecond * 60;
        let oneHour = oneMinute * 60;
        let oneDay = oneHour * 24;

        let seconds = Math.floor((millseconds % oneMinute) / oneSecond);
        let minutes = Math.floor((millseconds % oneHour) / oneMinute);
        let hours = Math.floor((millseconds % oneDay) / oneHour);
        let days = Math.floor(millseconds / oneDay);

        let timeString = '';
        if (days !== 0) {
            timeString += (days !== 1) ? (days + ' d ') : (days + ' d ');
        }
        if (hours !== 0) {
            timeString += (hours !== 1) ? (hours + ' h ') : (hours + ' h ');
        }
        if (minutes !== 0) {
            timeString += (minutes !== 1) ? (minutes + ' m ') : (minutes + ' m ');
        }
        if (seconds !== 0 || millseconds < 1000) {
            timeString += (seconds !== 1) ? (seconds + ' s ') : (seconds + ' s ');
        }
        return timeString;
    }

    return that;
});
