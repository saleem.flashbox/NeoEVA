NEC.factory('dashboardService', function($q, $rootScope, alertService, settingsService, dateUtilService, $http, appConfig, appConstantService, mapService) {
    let that = this;
    let pushDashboardData = [];
    let pushMessageData = {};
    let lastReceivedAlert ={};
    let areaSelected = 'All Areas';
    let mapSelected = 'All Cameras';
    let sensorIDSelected = 0;

    that.fetchLastReceivedData = function(sensorID) {
      return new $q(function(resolve, reject) {
           alertService.getDataForAlertType(appConstantService.TOPIC.FACE_MATCH, null, null, sensorID, null, null, 10, null)
                .then(function(response) {
                    if(response.status === 200) {
                        let faceMatchDataSet = response.data;
                        let iMsgCounter = 0;
                         if (that.getSensorIDSelected() === sensorID) {
                            that.setLastReceivedAlertForSensorID();
                        }

                        _.each(faceMatchDataSet ? faceMatchDataSet.reverse() : '', function(rowDetails) {
                            rowDetails.timestamp = moment.utc(rowDetails.timestamp).format(dateUtilService.DATE_FORMAT.ALERT_MANAGER);
                            that.setDashboardData(rowDetails);
                            if (++iMsgCounter === response.data.length) {
                                pushDashboardData = pushDashboardData.sort(function(left, right) {
                                    return moment.utc(right.timestamp).diff(moment.utc(left.timestamp));
                            });
                                (cleanArray(pushDashboardData))
                                    .then(function(resp) {
                                        resolve(that.getDashboardData());
                                    });
                            }
                        });
                    }else{
                        if (that.getSensorIDSelected() === sensorID && pushDashboardData.length === 0) {
                            that.setLastReceivedAlertForSensorID();
                        }
                        resolve(that.getDashboardData());
                    }
                });
         });
    };
    function cleanArray(actual) {
        return new $q(function(resolve, reject) {
            let newArray = new Array();
            for (let i = 0; i < actual.length; i++) {
                if (actual[i]) {
                    newArray.push(actual[i]);
                }
            }
            resolve(newArray);
        });
    }
    that.setSensorIDSelected = function(sensorID) {
        sensorIDSelected = sensorID;
    };
    that.getSensorIDSelected = function() {
        return sensorIDSelected;
    };
    that.getSensorSelected = function() {
        return mapSelected;
    };
    that.resetWatchlistDataSet = function() {
        pushDashboardData.length =0;
    };
    that.getAreaSelectd = function() {
        return areaSelected;
    };
    that.setSensorSelected = function(map) {
        mapSelected = map;
    };
    that.setAreaSelectd = function(area) {
        areaSelected = area;
    };
    that.setLastFetchedWatchlistAlert = function(lastAlert) {
        lastReceivedAlert = lastAlert;
    };
    that.setLastReceivedAlertForSensorID = function() {
        that.setDashboardData(lastReceivedAlert);
    };
    that.setDashboardData = function(faceMsgDataAlert) {
       // faceMsgDataAlert = buildWatchlistData(faceMsgDataAlert);
        if ((pushDashboardData.length + 1) > settingsService.getSettingDetails().dashboardDataSetSize) {
            pushDashboardData.pop(); // remove the last element
            pushDashboardData.shift([]); // insert an empty object
        }
        pushDashboardData.unshift(faceMsgDataAlert);
    };
    that.getDashboardData = function() {
        return pushDashboardData;
    };
    that.getMessageData = function() {
        return pushMessageData;
    };
    that.isAreaorMapSelected = function() {
        if (that.getSensorIDSelected() === 0) {
            return false;
        }
        return true;
    };
    that.getSensorSnapshotforSensorID = function(sensorId) {
            return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors/' + sensorId + '/snapshot');
        };

    return that;
});
