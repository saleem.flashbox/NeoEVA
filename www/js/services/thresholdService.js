/**
 * Created by suhas on 24/5/17.
 */

NEC.factory('thresholdService', function($http, $q, appService, $interval, appConstantService, $rootScope, sensorService, alertService, dateUtilService) {
    let that = this;
    that.pollerId = null;
    let sensorDetailsWithApplication = {};
    that.startPoller = function(interval, appName) {
        if (!that.pollerId) {
            let intervalPeriod = (interval != null) ? interval : appConstantService.intervalTimer.THRESHOLD;
            let pollerMethodToBeCalled = (appName != null) ? that.getThresholdOfAllSensorsByAppName : that.getThresholdOfAllAppsSensor;
            pollerMethodToBeCalled(appName);
            that.pollerId = $interval(function() {
                pollerMethodToBeCalled(appName);
            }, intervalPeriod);
        } else {
            that.stopPoller();
            that.startPoller();
        }
    };
    that.stopPoller = function() {
        $interval.cancel(that.pollerId);
        that.pollerId = null;
    };
    that.getThresholdOfAllAppsSensor = function() {
        return new $q(function(resolve, reject) {
            sensorService.getSensorListwithAppIds()
                .then(function(response) {
                    let sensorData = response;
                    let appArrayGrouped = _.groupBy(response, 'appName');
                    let appArray = _.keys(appArrayGrouped);
                    let noOfSenor = 0;
                    _.each(appArray, function(obj) {
                        _.each(appArrayGrouped[obj][0].sensorIDs, function() {
                            noOfSenor = noOfSenor + 1;
                        });
                    });

                    let sensorCounter = 0;
                    let sensorReturnCounter = 0;
                    sensorDetailsWithApplication = {};
                    let alertNameAndTypeMapping = appConstantService.AGGREGATOR_ALERTTYPE.DASHBOARD.APPS;
                    angular.forEach(appArray, function(appName) {
                        angular.forEach(alertNameAndTypeMapping[appName], function(alertType) {
                            angular.forEach(appArrayGrouped[appName][0].sensorIDs, function(dataSet) {
                                sensorReturnCounter = sensorReturnCounter + 1;
                                let endTime = moment.utc();
                                let startTime = moment(endTime).subtract(5, 'minutes');
                                startTime = moment(startTime).utc();
                                endTime = moment(endTime).format(dateUtilService.DATE_FORMAT.ALERT_MANAGER);
                                startTime = moment(startTime).format(dateUtilService.DATE_FORMAT.ALERT_MANAGER);
                                getAggregationDetailsAlongWithSensorAndApp(alertType, startTime, endTime, dataSet,
                                    appConstantService.AGGREGATOR_ALERTTYPE.DASHBOARD.SNAPSHOT.GRANUALRITY, appName)
                                    .then(function(alertResponse) {
                                        sensorCounter = sensorCounter + 1;
                                        if (sensorReturnCounter == sensorCounter) {
                                            _.each(alertResponse, function(alertAllVal) {
                                                let appAvailable = alertAllVal.apps;
                                                let availableAppsGrouped = _.groupBy(appAvailable, 'appName');
                                                let groupByType = _.groupBy(appAvailable, 'type');
                                                if (availableAppsGrouped.People) {
                                                    if (groupByType['people-count-in'] && groupByType['people-count-out']) {
                                                        let inCount = groupByType['people-count-in'][0].count;
                                                        let outCount = groupByType['people-count-out'][0].count;
                                                        if (inCount && inCount) {
                                                            groupByType['people-count-in'][0].count = inCount - outCount;
                                                        }
                                                    } else if (availableAppsGrouped['Age-Gender']) {
                                                        let maleCount = groupByType['gender-age-male'][0].count;
                                                        let femaleCount = groupByType['gender-age-female'][0].count;
                                                        groupByType['gender-age-male'][0].count = maleCount + femaleCount;
                                                    }
                                                }
                                            });
                                            resolve(alertResponse);
                                            emitThresholdPollerData(alertResponse);
                                        }
                                    });
                            });
                        });
                    });
                });
        });
    };
    that.getThresholdOfAllSensorsByAppName = function(appNamePassed) {
        return new $q(function(resolve, reject) {
            sensorService.getSensorListwithAppIds()
                .then(function(response) {
                    let sensorData = response;
                    let appArrayGrouped = _.groupBy(response, 'appName');
                    let appArray = _.keys(appArrayGrouped);

                    let sensorCounter = 0;
                    let sensorReturnCounter = 0;
                    sensorDetailsWithApplication = {};
                    let alertNameAndTypeMapping = appConstantService.AGGREGATOR_ALERTTYPE.DASHBOARD.APPS;
                    angular.forEach(alertNameAndTypeMapping[appNamePassed], function(alertType) {
                        angular.forEach(appArrayGrouped[appNamePassed][0].sensorIDs, function(dataSet) {
                            sensorReturnCounter = sensorReturnCounter + 1;
                            let endTime = moment.utc();
                            let startTime = moment(endTime).subtract(5, 'minutes');
                            endTime = moment(endTime).format(dateUtilService.DATE_FORMAT.ALERT_MANAGER);
                            startTime = moment(startTime).format(dateUtilService.DATE_FORMAT.ALERT_MANAGER);
                            getAggregationDetailsAlongWithSensorAndApp(alertType, startTime, endTime, dataSet,
                                appConstantService.AGGREGATOR_ALERTTYPE.DASHBOARD.SNAPSHOT.GRANUALRITY, appNamePassed)
                                .then(function(alertResponse) {
                                    sensorCounter = sensorCounter + 1;
                                    if (sensorReturnCounter == sensorCounter) {
                                        _.each(alertResponse, function(alertAllVal) {
                                            let appAvailable = alertAllVal.apps;
                                            let availableAppsGrouped = _.groupBy(appAvailable, 'appName');
                                            if (availableAppsGrouped.People) {
                                                let groupByType = _.groupBy(appAvailable, 'type');
                                                if (groupByType['people-count-in'] && groupByType['people-count-out']) {
                                                    let inCount = groupByType['people-count-in'][0].count;
                                                    let outCount = groupByType['people-count-out'][0].count;
                                                    if (inCount && inCount) {
                                                        groupByType['people-count-in'][0].count = inCount - outCount;
                                                    }
                                                }
                                            }
                                        });
                                        resolve(alertResponse);
                                        emitThresholdPollerData(alertResponse);
                                    }
                                });
                        });
                    });
                });
        });
    };
    function getAggregationDetailsAlongWithSensorAndApp(alertType, startTime, endTime, sensorID, granularity, appName) {
        return new $q(function(resolve, reject) {
            if (appName == 'People') {
                startTime = moment(startTime).startOf('day');
                startTime = moment(startTime).format(dateUtilService.DATE_FORMAT.ALERT_MANAGER);
            }
            alertService
                .getCountforAlertType(alertType, startTime, endTime, sensorID,
                    null, granularity, null)
                .then(function(res) {
                    if (res.status == 200) {
                        if (res.data[0].y[0] || res.data[0].y[0] == 0) {
                            let alertCount = res.data[0].y[0];
                            let data = {
                                sensorDetails: {
                                    id: sensorID,
                                    name: null,
                                },
                                alertDetails: {
                                    type: alertType,
                                    count: alertCount,
                                    appName: appName,
                                },
                            };
                            let sensorDetailsObj = data.sensorDetails;
                            let alertDetails = data.alertDetails;
                            if (!sensorDetailsWithApplication[data.sensorDetails.id]) {
                                sensorDetailsWithApplication[data.sensorDetails.id] = {};
                                sensorDetailsWithApplication[data.sensorDetails.id] = sensorDetailsObj;
                            }
                            if (!sensorDetailsWithApplication[data.sensorDetails.id].apps) {
                                sensorDetailsWithApplication[data.sensorDetails.id].apps = [];
                            }
                            sensorDetailsWithApplication[data.sensorDetails.id].apps.push(data.alertDetails);
                            resolve(sensorDetailsWithApplication);
                        } else {
                            resolve(null);
                        }
                    } else {
                        resolve(null);
                    }
                });
        });
    }

    function emitThresholdPollerData(data) {
        $rootScope.$broadcast(appConstantService.TOPIC.APP_SENSOR_THRESHOLD, data);
    }

    that.getUpdateSensorInstanceThreshold = function(areaList, sensorList) {
        let colorCode = '';
        for (let sensor in sensorList) {
            let sensorId = sensorList[sensor].id;
            for (let area in areaList) {
                let floors = areaList[area].floors;
                for (let floor in floors) {
                    let locations = floors[floor].locations;
                    for (let marker in locations) {
                        if (locations[marker].id == sensorId) {
                            let allApps = sensorList[sensor].apps;
                            for (let app in allApps) {
                                if (allApps[app].appName == 'Crowd') {
                                    colorCode = getColorCode(allApps[app].appName, allApps[app].count);
                                    locations[marker].allInstances.Crowd = '<li class="' + colorCode + '"><img src="images/CrowdCountApp.png" alt=""></li>';
                                } else if (allApps[app].appName == 'People') {
                                    if (allApps[app].type == 'people-count-in') {
                                        colorCode = getColorCode(allApps[app].appName, allApps[app].count);
                                        locations[marker].allInstances.People = '<li class="' + colorCode + '"><img src="images/PeopleCountApp.png" alt=""></li>';
                                    }
                                } else if (allApps[app].appName == 'Age-Gender') {

                                } else if (allApps[app].appName == 'Watchlist') {

                                }
                            }
                            let updatedInstances = locations[marker].allInstances;
                            let instances = '';
                            let watchlistAlert = locations[marker].watchlistAlert;
                            let camName = locations[marker].name;
                            for (let inst in updatedInstances) {
                                instances += updatedInstances[inst];
                            }
                            locations[marker].instances = instances;
                            locations[marker].label.message =
                                '<div class="liveMarker">' + watchlistAlert +
                                '<ul class="instances">' + instances +
                                '<span class="clear"></span>' +
                                '</ul>' +
                                '<div class="cameraName">' + camName + '</div>' +
                                '</div>';
                        }
                    }
                }
            }
        }
        return areaList;
    };
    function getColorCode(appName, threshold) {
        let thresholdLimit = appConstantService.AGGREGATOR_ALERTTYPE.DASHBOARD.threshold[appName];
        let index = _.findIndex(thresholdLimit, function(thresholdObj) {
            let min = thresholdObj.min;
            let max = thresholdObj.max;
            if (threshold <= Math.max(min, max) && threshold >= Math.min(min, max)) {
                return true;
            }
        });
        if (index != -1) {
            let colour = thresholdLimit[index].colour;
            return colour;
        } else {
            return 'green';
        }
    }

    return that;
});
