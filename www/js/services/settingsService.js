/**
 * Created by dhanalakshmi on 14/3/17.
 */


NEC.factory('settingsService', function($http, appConfig) {
    let that = this;
    let settingObj = {};
    let isHttpsEnabled = false;
    let protocol = 'http://';
    that.getAppSettingDetails = function() {
        let appBaseURL = 'http://163.172.172.74:3002';
        if(appConfig.platform === 'mobile'){
            //protocol=appConfig.protocol;
            appBaseURL = protocol+appConfig.baseUrl;
        }
        return $http.get(appBaseURL+'/settingDetails');
    };
    that.setSettingDetails = function(settingDetails) {
        settingObj = settingDetails;
    };
    that.getSettingDetails = function() {
        return settingObj;
    };
    that.setIsEnabledHttp = function(state) {
        isHttpsEnabled = state;
    };
    that.getIsEnabledHttp = function() {
        return isHttpsEnabled;
    };
    that.setBaseUrl = function (baseUrl) {
        localStorage.setItem('baseUrl',baseUrl);
    }
    that.setProtocol = function (protocol) {
        localStorage.setItem('protocol',protocol);
    }
    that.getBaseUrl = function () {
        return localStorage.getItem('baseUrl');
    }
    that.updateAppConfig = function () {
        appConfig.baseUrl = localStorage.getItem('baseUrl');
        appConfig.protocol = localStorage.getItem('protocol');
        appConfig.applicationBaseUrl = appConfig.protocol+appConfig.baseUrl+"/api";
        appConfig.loginApiBaseUrl = appConfig.protocol+appConfig.baseUrl+"/authorizationApi";
    }

    return that;
});

