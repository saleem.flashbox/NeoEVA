/**
 * Created by suhas on 25/4/17.
 */

NEC.factory('instancePollerService', function($http, appConfig, $q, instanceService, appConstantService, $rootScope, $timeout, $interval, settingsService) {
    let that = this;
    let requiredStatus = {
        start: {required: 'true', intermediate: 'starting', afterCompleteStatus: 'stop'},
        stop: {required: 'false', intermediate: 'stopping', afterCompleteStatus: 'start'},
        configure: {required: 'false', intermediate: 'configuring', afterCompleteStatus: 'start'},
    };
    let timer = 7 * 1000;

    that.sensorPoller = {
        pollingTime: 60 * 1000,
        pollingFreq: 2 * 1000,
        pollerRunningTracker: 0,
        runningInstanceDataSet: [],
    };
    that.runningInstance = null;
    that.instancePollerData = {};

    that.startSensorPolling = function(appId, sensorID, instanceId, pollerAction, oldInstanceStatus) {
        initialisePollerConfiguration();
        that.runningInstance = $interval(function() {
            let instanceKeyIdentifier = sensorID + '$' + appId + '$' + instanceId;
            if (!(_.find(that.sensorPoller.runningInstanceDataSet, function(o) {
                    return o.instanceKey === (instanceKeyIdentifier);
                }))) {
                that.sensorPoller.runningInstanceDataSet.push({
                    instanceKey: instanceKeyIdentifier,
                    instanceIdentifier: that.runningInstance,
                });
                emitPollerInfo('sensorInstanceStateChange', instanceKeyIdentifier);
            }
            instanceService.getInstanceDetailsById(instanceId)
                .then(function(response) {
                    let newInstanceStatus = response.data[0].status.running;
                    if (oldInstanceStatus !== newInstanceStatus) {
                        that.stopSensorPoller(instanceKeyIdentifier);
                    }
                });
            (that.sensorPoller.pollerRunningTracker >= that.sensorPoller.pollingTime) ? that.stopSensorPoller(instanceKeyIdentifier) : that.sensorPoller.pollerRunningTracker += that.sensorPoller.pollingFreq;
        }, that.sensorPoller.pollingFreq);
    };

    that.checkPollerStatus = function(instanceKey) {
        return _.find(that.sensorPoller.runningInstanceDataSet, function(o) {
            return o.instanceKey === (instanceKey);
        }) ? true : false;
    };
    that.stopSensorPoller = function(instanceKeyRef) {
        _.forEach(that.sensorPoller.runningInstanceDataSet, function(key) {
            if (key != undefined && key.instanceKey === instanceKeyRef) {
                $interval.cancel(key.instanceIdentifier);
                _.pull(that.sensorPoller.runningInstanceDataSet, key);
                emitPollerInfo('sensorInstanceStateChange', instanceKeyRef);
            }
        });
        if (that.sensorPoller.runningInstanceDataSet.length == 0) {
            $interval.cancel(that.runningInstance);
        }
    };
    that.isPollerRunning = function(appName, sensorId) {
        return new $q(function(resolve, reject) {
            if (that.instancePollerData[appName]) {
                if (that.instancePollerData[appName][sensorId.toString()]) {
                    resolve(that.instancePollerData[appName][sensorId.toString()].isPollerRunning);
                } else {
                    resolve(false);
                }
            } else {
                resolve(false);
            }
        });
    };
    that.startPoller = function(appName, instanceId, sensorId, status) {
        if (!that.instancePollerData[appName]) {
            that.instancePollerData[appName] = {};
        }
        that.instancePollerData[appName][sensorId.toString()] = {
            sensorId: sensorId,
            status: status,
            instanceId: instanceId,
            appName: appName,
            startTime: moment(),
            isPollerRunning: true,
            timeOutId: null,
        };
        instanceService.setInstanceStatus(instanceId, status)
            .then(function(response) {
            });
        getInstanceDetailsById(appName, instanceId, sensorId, status);
    };

    function getInstanceDetailsById(appName, instanceId, sensorId, status) {
        if (that.instancePollerData[appName][sensorId.toString()].timeOutId) {
            $timeout.cancel(that.instancePollerData[appName][sensorId.toString()].timeOutId);
            that.instancePollerData[appName][sensorId.toString()].timeOutId = null;
        }
        let currentTime = moment();
        let diffMinutes = currentTime.diff(moment(that.instancePollerData[appName][sensorId.toString()].startTime), 'minutes');
        let intermediateStatus = requiredStatus[status].intermediate;
        if (diffMinutes <= 1) {
            instanceService.getInstanceDetailsById(instanceId)
                .then(function(instanceDetailsResponse) {
                    if (instanceDetailsResponse.status == 200) {
                        let instanceDetails = instanceDetailsResponse.data[0];
                        let instanceStatus = instanceDetails.status.running.toString();
                        let indexVal = requiredStatus[status].required === instanceStatus;
                        if (indexVal) {
                            that.instancePollerData[appName][sensorId.toString()].isPollerRunning = false;
                            let pollerRes1 = {
                                data: that.instancePollerData[appName][sensorId.toString()],
                                status: requiredStatus[status].afterCompleteStatus,
                                isInterMediate: false,
                            };
                            emitPollerInfo(appName + 'InstancePoller', pollerRes1);

                            if (that.instancePollerData[appName][sensorId.toString()].timeOutId) {
                                $timeout.cancel(that.instancePollerData[appName][sensorId.toString()].timeOutId);
                                that.instancePollerData[appName][sensorId.toString()].timeOutId = null;
                            }
                            delete that.instancePollerData[appName][sensorId.toString()];
                        } else {
                            let pollerRes2 = {
                                data: that.instancePollerData[appName][sensorId.toString()],
                                status: intermediateStatus,
                                isInterMediate: true,
                            };
                            emitPollerInfo(appName + 'InstancePoller', pollerRes2);
                            if (that.instancePollerData[appName][sensorId.toString()].timeOutId) {
                                $timeout.cancel(that.instancePollerData[appName][sensorId.toString()].timeOutId);
                                that.instancePollerData[appName][sensorId.toString()].timeOutId = null;
                            }

                            that.instancePollerData[appName][sensorId.toString()].timeOutId =
                                $timeout(getInstanceDetailsById, timer, true, appName, instanceId, sensorId, status);
                        }
                    } else {
                        let pollerRes3 = {
                            data: that.instancePollerData[appName][sensorId.toString()],
                            status: intermediateStatus,
                        };
                        emitPollerInfo(appName + 'InstancePoller', pollerRes3);
                        if (that.instancePollerData[appName][sensorId.toString()].timeOutId) {
                            $timeout.cancel(that.instancePollerData[appName][sensorId.toString()].timeOutId);
                            that.instancePollerData[appName][sensorId.toString()].timeOutId = null;
                        }
                        that.instancePollerData[appName][sensorId.toString()].timeOutId =
                            $timeout(getInstanceDetailsById, timer, true, appName, instanceId, sensorId, status);
                    }
                });
        } else {
            if (that.instancePollerData[appName][sensorId.toString()].timeOutId) {
                $timeout.cancel(that.instancePollerData[appName][sensorId.toString()].timeOutId);
            }
            that.instancePollerData[appName][sensorId.toString()].isPollerRunning = false;
            let pollerRes4 = {
                data: that.instancePollerData[appName][sensorId.toString()],
                status: null,
            };
            emitPollerInfo(appName + 'InstancePoller', pollerRes4);
            delete that.instancePollerData[appName][sensorId.toString()];
        }
    }

    function emitPollerInfo(channelName, data) {
        $rootScope.$emit(channelName, data);
    }

    function initialisePollerConfiguration() {
        let configPollingTime = settingsService.getSettingDetails().pollerParams.sensorStatusCheckMaximumAttempts_Seconds;
        let configPollingFreq = settingsService.getSettingDetails().pollerParams.sensorStatusCheckRequestFreq_Seconds;
        that.sensorPoller.pollingTime = (configPollingTime) ? parseFloat(configPollingTime) * 1000 : that.sensorPoller.pollingTime;
        that.sensorPoller.pollingFreq = (configPollingFreq) ? parseFloat(configPollingFreq) * 1000 : that.sensorPoller.pollingFreq;
        that.sensorPoller.pollerRunningTracker = 0;
    }

    return that;
});
