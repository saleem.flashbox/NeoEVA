/**
 * Created by suhas on 15/4/17.
 */
NEC.factory('dateUtilService', function(appConstantService) {
    let that = this;

    const DATE_FORMAT = {
        ALERT_MANAGER: 'YYYY-MM-DD[T]HH:mm:ss[Z]',
        ALERT_FACEMATCH_FORMAT: 'MMM Do, YYYY hh:mm A',
            WATCHLIST_DASHBOARD_FORMAT: 'MM-DD-YYYY, hh:mm:ss',
            OVERVIEW_CARD_DATE_FORMAT: 'MMM Do, YYYY',
            OVERVIEW_CARD_TIME_FORMAT: 'hh:mm A',
            OVERVIEW_FROM_DATE_FORMAT: 'MM-DD-YYYY, hh:mm',
            APP_ANALYTICS_FORMAT: 'MM-DD-YYYY, hh:mm',
            UI_ANALYTICS_DATE_SELECTION_FORMAT: 'MM-DD-YYYY, hh:mm',
            DASHBOARD_ANALYTICS_FORMAT: 'hh:mm A',
    };
    const DATE_VALIDATION = {
        OK: 'validdate',
        ENDDATE_LESS_THAN_STARTDATE: 'End Date is less than Start Date',
        DATE_RANGE_EXCEEDS_ALLOWED_RANGE: 'Date range should not exceed Year',
    };

    that.DATE_VALIDATION = DATE_VALIDATION;
    that.DATE_FORMAT = DATE_FORMAT;
    that.userTZOffset = null;
    that.userTimeZone = null;
    function calculateTZOffset() {
        let offset = new Date().getTimezoneOffset(),
            o = Math.abs(offset);
        let offsetInHoursMins = (offset < 0 ? '+' : '-') + ('00' + Math.floor(o / 60)).slice(-2) + ':' + ('00' + (o % 60)).slice(-2);
        let userTimeZone = moment().tz(moment.tz.guess()).zoneName();
        that.setUserTimeZone(userTimeZone);
        that.userTZOffset = 'UTC' + offsetInHoursMins;
        return that.userTZOffset;
    }
    that.getTimeZoneOffset= function() {
        return (that.userTZOffset)?that.userTZOffset:calculateTZOffset();
    };
    that.getUserTimeZone= function() {
        return that.userTimeZone;
    };
    that.setUserTimeZone = function(userTimeZone) {
        that.userTimeZone = userTimeZone;
    };
    that.modifyDataFormatAsRequired = function(start, end, format) {
        let startVal = angular.copy(start);
        let endVal = angular.copy(end);
        let dateChanged = {start: null, end: null};
        if (start) {
            if (typeof startVal === 'string') {
                dateChanged.start = moment(startVal).utc().format(format);
            } else {
                dateChanged.start = moment(startVal).utc().format(format);
            }
        }
        if (end) {
            dateChanged.end = moment(endVal).utc().format(format);
        }
        return dateChanged;
    };

    that.alertManagerToMMDDYYYYHHmm = function(date) {
        return moment(date).format(DATE_FORMAT.APP_ANALYTICS_FORMAT);
    };

    that.alertManagerToMMMDYYYYHHmmA = function(date) {
        return moment(date).format(DATE_FORMAT.ALERT_FACEMATCH_FORMAT);
    };

    that.alertManagerToHHmm = function(date) {
        return moment(date).format(DATE_FORMAT.DASHBOARD_ANALYTICS_FORMAT);
    };

    that.getStartOfDate = function(date) {
        if (date) {
            let dateToBeModified = angular.copy(date);
            dateToBeModified.setHours(0);
            dateToBeModified.setMinutes(0);
            dateToBeModified.setSeconds(0);
            return dateToBeModified;
        } else {
            return null;
        }
    };

    that.validateDateforAnalytics = function(startDt, endDate) {
        if (endDate < startDt) {
            return DATE_VALIDATION.ENDDATE_LESS_THAN_STARTDATE;
        }
        if ((endDate - startDt) > (appConstantService.VALID_MAX_DATE_RANGE)) {
            return DATE_VALIDATION.DATE_RANGE_EXCEEDS_ALLOWED_RANGE;
        }
        return DATE_VALIDATION.OK;
    };
    return that;
});
