NEC.factory('mapService', function($http, appConfig, $q, locationService, sensorService, settingsService) {
    let that = this;
    let appIconTemplate = {
        'Crowd': '<li class="green"><img src="images/CrowdCountApp.png" alt=""></li>',
        'Age-Gender': '<li class="green"><img src="images/AgeGenderApp.png" alt=""></li>',
        'People': '<li class="green"><img src="images/PeopleCountApp.png" alt=""></li>',
        'Watchlist': '<li class="green"><img src="images/WatchlistApp.png" alt=""></li>',
    };
    that.fetchFacilities = function() {
        return $q(function(resolve) {
            let allArea = {};
            locationService.getLocationByType('facility').then(function(response) {
                let facilitiesList = response.data;
                if (facilitiesList.length > 0) {
                    for (let facility = 0; facility < facilitiesList.length; facility++) {
                        let currentFacility = facilitiesList[facility];
                        allArea[currentFacility.id] = {
                            'id': currentFacility.id,
                            'name': currentFacility.name,
                            'lat': currentFacility.info.lat,
                            'lng': currentFacility.info.lng,
                            'type': settingsService.getSettingDetails().locationType.LOCATION_FACILITY,
                            'members': currentFacility.members,
                            'floors': [],
                        };
                        if (facility === facilitiesList.length - 1) {
                            resolve(allArea);
                        }
                    }
                } else {
                    resolve(allArea);
                }
            });
        });
    };
    that.fetchAllFloors = function() {
        return $q(function(resolve) {
            let floors = {};
            locationService.getLocationByType('floor').then(function(response) {
                let floorList = response.data;
                if (floorList.length > 0) {
                    for (let floor = 0; floor < floorList.length; floor++) {
                        let currentFloor = floorList[floor];
                        floors[currentFloor.id] = {
                            'id': currentFloor.id,
                            'name': currentFloor.name,
                            'planHeight': currentFloor.info.planHeight,
                            'planWidth': currentFloor.info.planWidth,
                            'planImg': currentFloor.info.planImg,
                            'members': currentFloor.members,
                            'locations': [],
                            'type': currentFloor.type,
                            'facilityId': currentFloor.info.areaId,
                        };
                        if (floor === floorList.length - 1) {
                            resolve(floors);
                        }
                    }
                } else {
                    resolve(floors);
                }
            });
        });
    };
    that.fetchAllLocations = function(appId, pageName) {
        return $q(function(resolve) {
            let locationsInfo = {};
            let locations = {};
            let counter = 0;
            locationService.getLocationByType('location').then(function(locResponse) {
                let locationList = locResponse.data;
                if (locationList.length > 0) {
                    for (let loc=0; loc<locationList.length; loc++) {
                        locationsInfo[locationList[loc].id] = locationList[loc];
                        counter++;
                        if (counter === locationList.length) {
                            generateLocationsMarker(locationsInfo, appId, pageName).then(function(result) {
                                resolve(result);
                            });
                        }
                    }
                }
                else{
                    resolve(locations);
                }
            });
        });
    };
    that.listAllSensors = function(appId, pageName) {
        let data = {};
        return $q(function(resolve) {
            locationService.getAllLocations().then(function(response) {
                if (response) {
                    let listFacility = that.fetchFacilities().then(function(facilities) {
                        data.facilityList = facilities;
                    });
                    let listFloor = that.fetchAllFloors().then(function(floors) {
                        data.floorList = floors;
                    });
                    let listLocation = that.fetchAllLocations(appId, pageName).then(function(locations) {
                        data.locationList = locations;
                    });
                    $q.all([listFacility, listFloor, listLocation]).then(function() {
                        if (pageName) {
                            resolve(data);
                        } else {
                            // console.log(data);
                            let areaCounter = 0;
                            let mapData = {
                                allAreaMaps: [],
                                sensorIndex: null,
                                floorIndex: null,
                                facilityIndex: null,
                            };
                            let facilityObj = [];
                            if(!_.isEmpty(data.locationList)){
                                angular.forEach(data.facilityList, function(value, facilityKey) {
                                    if (value.members.length > 0) {
                                        let facilityDetail = value;
                                        let floorObj = [];
                                        angular.forEach(facilityDetail.members, function(value, floorKey) {
                                            let floorDetail = data.floorList[value];
                                            if (floorDetail.members.length > 0) {
                                                angular.forEach(floorDetail.members, function(value, locationKey) {
                                                    if (data.locationList[value]) {
                                                        if (facilityObj.length === 0) {
                                                            facilityObj.push(facilityKey);
                                                        } else {
                                                            if (facilityObj.indexOf(facilityKey) === -1) {
                                                                facilityObj.push(facilityKey);
                                                            }
                                                        }
                                                        if (floorObj.length === 0) {
                                                            floorObj.push(floorKey);
                                                        } else {
                                                            if (floorObj.indexOf(floorKey) === -1) {
                                                                floorObj.push(floorKey);
                                                            }
                                                        }
                                                        let facilityIndex = facilityObj.length - 1;
                                                        let floorIndex = floorObj.length - 1;
                                                        mapData.allAreaMaps[facilityIndex] = facilityDetail;
                                                        mapData.allAreaMaps[facilityIndex].floors[floorIndex] = floorDetail;
                                                        mapData.allAreaMaps[facilityIndex].floors[floorIndex].locations.push(data.locationList[value]);
                                                        if (mapData.sensorIndex === null) {
                                                            mapData.sensorIndex = mapData.allAreaMaps[facilityIndex].floors[floorIndex].locations.length - 1;
                                                            mapData.floorIndex = floorIndex;
                                                            mapData.facilityIndex = facilityIndex;
                                                            mapData.sensorId = data.locationList[value].id;
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }

                                    if (areaCounter === Object.keys(data.facilityList).length - 1) {
                                        resolve(mapData);
                                    }
                                    areaCounter++;
                                });
                            }
                            else{
                                mapData.allAreaMaps = [];
                                resolve(mapData)
                            }
                        }
                    });
                }
            });
        });
    };
    that.mapScreen = function(lat, lng, theme) {
        if (theme === 'darkTheme') {
            return 'https://maps.googleapis.com/maps/api/staticmap?center=' + lat + ',' + lng +
                '&zoom=14&size=300x220&maptype=roadmap' +
                '&style=feature:road%7Celement:geometry%7Ccolor:0x353b50&' +
                'style=feature:landscape%7Celement:geometry.fill%7Ccolor:0x1d212f&' +
                'style=feature:administrative%7Celement:geometry.fill%7Ccolor:0x1a3541%7Cweight:0.6&' +
                'style=feature:poi%7Celement:geometry%7Ccolor:0x1e2230&' +
                'style=feature:poi.park%7Celement:geometry%7Ccolor:0x1e2230&' +
                'style=feature:transit%7Celement:geometry%7Ccolor:0x1a1e2a&' +
                'style=feature:water%7Celement:geometry%7Ccolor:0x616c8d&' +
                'style=element:labels.text.fill%7Ccolor:0x838f9d&' +
                'style=element:labels.text.stroke%7Cvisibility:simplified&' +
                'style=element:labels.icon%7Cvisibility:off&' +
                'style=feature:road.arterial%7Celement:labels%7Cinvert_lightness:false&' +
                'key=AIzaSyAAYbK-rfCSGgXhsFuSv7MebK-ZX6WjRZI';
        } else {
            return 'https://maps.googleapis.com/maps/api/staticmap?center=' + lat + ',' + lng +
                '&zoom=14&size=300x220&maptype=roadmap&key=AIzaSyAAYbK-rfCSGgXhsFuSv7MebK-ZX6WjRZI';
        }
    };

    that.getSensorLocationDetail = function() {
        let appId = '';
        let pageName = 'watchlist';
        let sensorLocationInfo = {};
        return $q(function(resolve) {
            that.listAllSensors(appId, pageName).then(function(data) {
                if(Object.keys(data).length > 0) {
                    angular.forEach(data.locationList, function(value) {
                        sensorLocationInfo[value.id] = {
                            'cameraName': value.name,
                            'floorName': (value.floorId)? (data.floorList[value.floorId]? data.floorList[value.floorId].name : '-' ) : '-',
                            'facilityName': (value.floorId)? ((data.floorList[value.floorId].facilityId)? ((data.facilityList[data.floorList[value.floorId].facilityId])? data.facilityList[data.floorList[value.floorId].facilityId].name: '-' ): '-') : '-',
                        };
                        resolve(sensorLocationInfo);
                    });
                } else{
                    resolve(sensorLocationInfo);
                }
            });
        });
    };

    function generateLocationsMarker(locationsInfo, appId, pageName) {
        return $q(function(resolve) {
            let locations = {};
            let startCounter = 0;
            let appCounter = 0;
            let resolveCounter = 0;
            sensorService.getSensorsWithInstance().then(function(sensorsList) {
                if(!_.isEmpty(sensorsList)){
                    angular.forEach(sensorsList, function(sensorData, locationId) {
                        let sensorId = sensorData.id;
                        let appList = sensorData.appList;
                        if (locationsInfo[locationId]) {
                            startCounter++;
                            if (!appId) {
                                fetchSensorsInstances(appId, appList).then(function(sensorWithInstances) {
                                    locations[locationId] = createMarker(sensorsList[locationId], locationsInfo[locationId], sensorWithInstances, pageName);
                                    resolveCounter++;
                                    if (startCounter === resolveCounter) {
                                        resolve(locations);
                                    }
                                });
                            } else {

                                angular.forEach(appList, function (value, key) {
                                    if(appId == value){
                                        fetchSensorsInstances(appId, appList).then(function (sensorWithInstances) {
                                            locations[locationId] = createMarker(sensorsList[locationId], locationsInfo[locationId], sensorWithInstances, pageName);
                                            resolve(locations);
                                        });
                                    }
                                    else{
                                        resolve(locations);
                                    }
                                })
                            }
                        }
                    });
                } else {
                    resolve(locations);
                }

            });
        });
    }
    function fetchSensorsInstances(appId, allInstances) {
        return $q(function(resolve) {
            let sensorInstanceData = {};
            sensorInstanceData.sensorInstances = '';
            sensorInstanceData.instanceObj = {};
            sensorInstanceData.appList = [];
            if (!appId) {
                let instanceCount = {
                    'Crowd': 0,
                    'Age-Gender': 0,
                    'People': 0,
                    'Watchlist': 0,
                };
                let counter =0;
                if (allInstances.length > 0) {
                    for (let instance in allInstances) {
                        if (Object.prototype.hasOwnProperty.call(allInstances, instance)) {
                            angular.forEach(settingsService.getSettingDetails().appIdAndApplicationMapping, function(value, key) {
                                if (allInstances[instance]===value) {
                                    if (instanceCount[key] === 0) {
                                        sensorInstanceData.sensorInstances += appIconTemplate[key];
                                        sensorInstanceData.instanceObj[key] = appIconTemplate[key];
                                        sensorInstanceData.appList.push(key);
                                        instanceCount[key]=instanceCount[key]+1;
                                    }
                                }
                            });
                            counter++;
                            if (counter === allInstances.length) {
                                resolve(sensorInstanceData);
                            }
                        }
                    }
                } else {
                    resolve(sensorInstanceData);
                }
            } else {
                angular.forEach(settingsService.getSettingDetails().appIdAndApplicationMapping, function(value, key) {
                    if(appId === value) {
                        sensorInstanceData.sensorInstances = appIconTemplate[key];
                        sensorInstanceData.instanceObj[key]= appIconTemplate[key];
                        sensorInstanceData.appList.push(key);
                    }
                });
                resolve(sensorInstanceData);
            }
        });
    }

    function createMarker(sensorData, locationData, instancesData, pageName) {
        let marker = drawMarker(sensorData, locationData);
        if (pageName) {
            marker['label'] = {
                message: '<div class="markerName ' + sensorData.id + '">' + sensorData.name + '</div>',
                options: {
                    noHide: true,
                    offset: [22, -15],
                },
            };
            return marker;
        } else {
            marker['instances'] = instancesData.sensorInstances;
            marker['allInstances'] = instancesData.instanceObj;
            marker['appList'] = instancesData.appList;
            marker['watchlistAlert'] = '';
            marker['label'] = {
                message: '<div class="liveMarker">' +
                '<ul class="instances">' + instancesData.sensorInstances +
                '<span class="clear"></span>' +
                '</ul>' +
                '<div class="cameraName">' + sensorData.name + '</div>' +
                '</div>',
                options: {
                    noHide: true,
                    offset: [22, -15],
                },
            };
            return marker;
        }
    }

    function drawMarker(sensorData, locationData) {
        return {
            id: sensorData.id,
            uri: sensorData.uri,
            status: 'saved',
            name: sensorData.name,
            floorId: locationData.info.areaId,
            lat: locationData.info.lat,
            lng: locationData.info.lng,
            type: settingsService.getSettingDetails().locationType.LOCATION_CAMERA,
            compileMessage: true,
            getMessageScope: function() {
                return this;
            },
            draggable: false,
            icon: {
                iconUrl: 'images/cam.png',
                iconSize: [40, 40],
            },
            popupOptions: {
                autoClose: false,
                closeButton: false,
                closeOnClick: false,
            },
        };
    }

    return that;
});
