/**
 * Created by dhanalakshmi on 6/3/17.
 */

NEC.factory('adminSettingService', function($http, appConfig) {
    let that = this;
    that.getAllUsers = function() {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/users');
    };
    that.saveUserDetails = function(userObj) {
        return $http.post(appConfig.applicationBaseUrl + '/pulseExecutionFramework/users', userObj);
    };
    that.updateUserDetails = function(username, userObj) {
        return $http.put(appConfig.applicationBaseUrl + '/pulseExecutionFramework/users/' + username, userObj);
    };
    that.deleteUserDetails = function(userName) {
        return $http.delete(appConfig.applicationBaseUrl + '/pulseExecutionFramework/users/' + userName);
    };

    return that;
});
