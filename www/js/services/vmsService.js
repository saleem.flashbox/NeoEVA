NEC.factory('vmsService', function($http, appConfig, $q) {
    let that = this;

    that.getVMSDetailsFromAPI = function(cameraDetails) {
        let vmsQueryParams ='?';
        if (cameraDetails.vmsIp)
            vmsQueryParams += 'vmsIp=' + cameraDetails.vmsIp + '&';
            if (cameraDetails.vmsPort)
                vmsQueryParams += 'vmsPort=' + cameraDetails.vmsPort + '&';
                if (cameraDetails.basePath)
                    vmsQueryParams += 'basePath=' + cameraDetails.basePath + '&';
                    if (cameraDetails.username)
                        vmsQueryParams += 'username=' + cameraDetails.username + '&';
                        if (cameraDetails.password)
                            vmsQueryParams += 'password=' + cameraDetails.password + '&';
                            if (cameraDetails.authKey)
                                vmsQueryParams += 'authKey=' + window.encodeURIComponent(cameraDetails.authKey) + '&';
                                if (cameraDetails.mediaPort)
                                    vmsQueryParams += 'mediaPort=' + cameraDetails.mediaPort;
                                    return $http.get(appConfig.applicationBaseUrl + '/vms/mapping' + vmsQueryParams);
                                };
    that.parseDataFromVMS = function(serverVMSResponse) {
        return new $q(function(resolve, reject) {
           let vmsFormattedData = [];
           let vmsResponse ={};
           if(!serverVMSResponse.data.Status && serverVMSResponse.data.status !== 500) {
       _.each(serverVMSResponse.data[0], function(cameraURL, ip) {
           vmsFormattedData.push({
               'vmsip': ip,
               'vmsurl': cameraURL,
           });
             });
               vmsResponse.data= vmsFormattedData;
               vmsResponse.status = serverVMSResponse.status;
        } else {
               vmsResponse.status = serverVMSResponse.data.Status;
               vmsResponse.data= serverVMSResponse.data.Server_response;
      }
      resolve(vmsResponse);
    });
    };
    that.setVMSDetails= function(data) {
        return $http.post(appConfig.applicationBaseUrl + '/retailConfig/vms', data);
    };
    that.getVMSDetails = function(cameraDetails) {
        return new $q(function(resolve, reject) {
           that.getVMSDetailsFromAPI(cameraDetails)
                .then(function(response) {
                    resolve(that.parseDataFromVMS(response));
                });
        });
    };
    return that;
});
