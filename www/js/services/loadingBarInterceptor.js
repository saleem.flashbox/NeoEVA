/**
 * Created by MohammedSaleem on 01/03/17.
 */

NEC.factory('loadingBarInterceptor', function($rootScope, $injector) {
    let loadingBar = {
        request: function(config) {
            if (!(config.url.includes('/snapshot') ||
                config.url.includes('/api/pulseExecutionFramework/sensors') ||
                config.url.includes('/api/pulseExecutionFramework/instances') ||
                config.url.includes('api/alertStatAggregator'))) {
                $rootScope.showLoader();
            }

            return config;
        },
        response: function(res) {
            let $http = $injector.get('$http');

            if ($http.pendingRequests.length === 0) {
                $rootScope.hideLoader();
            }

            return res;
        },
        requestError: function(rejection) {
            $rootScope.hideLoader();
            return rejection;
        },
        responseError: function(rejection) {
            $rootScope.hideLoader();
            return rejection;
        },
    };
    return loadingBar;
});

