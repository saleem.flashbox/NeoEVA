/**
/**
 * Created by dhanalakshmi on 3/2/17.
 * SensorService:
 */
NEC.factory('sensorService', function($http, appConfig, $q, settingsService, $rootScope, appConstantService, instanceService, instancePollerService, locationService) {
    let that = this;
    let selectedNode;
    let sensorInstanceAppIDMapper = [];
    let sensorDetailsGroupedByAlertType = null;
    this.allSensorList = [];
    let sensorAppIDMapper = [];
    let arrInstanceDetailsforSensor = [];
    let arrInstanceDatset = [];
    let sensorsListWithInstanceDetail = {};
    const SENSOR ={
        LENGTH: {
            URI: 25,
            APPNAME: 20,
        },
    };
    that.setSensorAppIDMapper = function(mapper) {
        that.sensorAppIDMapper = mapper;
    };
    that.resetSensorAppIDMapper = function() {
        that.sensorAppIDMapper = [];
    };
    that.getSensorAppIDMapper = function() {
        return that.sensorAppIDMapper;
    };
    that.SENSOR = SENSOR;
    that.getInstanceDetailsByAppIDandSensorID = function(appID, sensorID) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/instances?appId=' + appID + '&sensorId=' + sensorID);
    };
    that.getInstanceforSensorId = function(sensorId) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/instances?sensorId=' + sensorId);
    };
    that.getAllSensors = function() {
        let type = appConstantService.sensorTypes.camera;
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors?type=' + type);
    };
    that.setSelectedNode = function(nodeName) {
        selectedNode = nodeName;
    };
    that.getSelectedNode = function() {
        return selectedNode;
    };
    that.getAllSensorsByType = function() {
        let type = appConstantService.sensorTypes.camera;
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors?type=' + settingsService.getSettingDetails().sensorType);
    };
    that.updateAssociatedAppsForAllSensors = function() {

    };
    that.getAssociatedAppIDforSensorId = function(sensorID) {
        return new $q(function(resolve, reject) {
            let associatedAppIDs = [];
            _.each(that.getSensorAppIDMapper(), function(item, key) {
                if(_.includes(item.sensorIDs, sensorID)) {
                    associatedAppIDs.push(item.appID);
                }
            });
            resolve(associatedAppIDs);
        });
    };
    that.sensorListByAppType = function(type) {
        return new $q(function(resolve, reject) {
            if (!sensorDetailsGroupedByAlertType) {
                setSensorListByAppType()
                    .then(function(data) {
                        sensorDetailsGroupedByAlertType = data;
                        resolve(sensorDetailsGroupedByAlertType[type]);
                    });
            } else {
                resolve(sensorDetailsGroupedByAlertType[type]);
            }
        });
    };
    that.getSensorDetailsFromIdFromCache = function(id) {
        return new $q(function(resolve, reject) {
            let sensorById = _.filter(this.allSensorList, {'id': id});
            if (sensorById.length > 0) {
                sensorById = sensorById[0];
            } else {
                getSensorById(id).then(function(data) {
                    if (data.status == 200) {
                        resolve(data.data);
                    } else {
                        reject(data.status);
                    }
                });
            }
            resolve(sensorById);
        });
    };
    that.getSensorById = function(sensorId) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors/' + sensorId);
    };
    that.getSensorListByAppName = function(appName) {
        return new $q(function(resolve, reject) {
            settingsService.getAppSettingDetails()
                .then(function(settingsResponse) {
                    let settingDetails = settingsResponse.data;
                    let appIdAndNameMapping = settingDetails.appIdAndApplicationMapping;
                    let selectedAppId = appIdAndNameMapping[appName];
                    getSensorListByAppId(selectedAppId)
                        .then(function(res) {
                            if (res.status == 200) {
                                resolve(res.data);
                            } else if (res.status == 500) {
                                reject(res.status);
                            }
                        });
                });
        });
    };
    that.getSensorListByAppId = function(appId) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors?appId=' + appId);
    };
    that.getAllSensorWithInstanceDetailsByAppName = function(appName) {
        return new $q(function(resolve, reject) {
            that.getAllSensors()
                .then(function(allSensorResponse) {
                    if (allSensorResponse.status == 200) {
                        let allSensors = allSensorResponse.data;
                        settingsService.getAppSettingDetails()
                            .then(function(settingsResponse) {
                                let settingDetails = settingsResponse.data;
                                let appIdAndNameMapping = settingDetails.appIdAndApplicationMapping;
                                let selectedAppId = appIdAndNameMapping[appName];
                                getSensorListByAppId(selectedAppId)
                                    .then(function(appSensorList) {
                                        if (appSensorList.status == 200) {
                                            let appSpecificSensors = appSensorList.data;
                                            let counter = 0;
                                            let instanceList = [];
                                            for (let i = 0; i < appSpecificSensors.length; i++) {
                                                let sensorSelected = appSpecificSensors[i];
                                                instanceService.getInstanceBySensorIdAndAppId(sensorSelected.id, selectedAppId)
                                                    .then(function(instanceDetails) {
                                                        let obj = {
                                                            instance: instanceDetails.instanceList[0],
                                                            sensorId: instanceDetails.sensorId,
                                                        };
                                                        instanceList.push(obj);
                                                        counter++;
                                                        if (counter == appSpecificSensors.length) {
                                                            arrangeSensorList(allSensors, appSpecificSensors, instanceList, selectedAppId)
                                                                .then(function(data) {
                                                                    resolve(data);
                                                                });
                                                        }
                                                    });
                                            }
                                        }
                                    });
                            });
                    }
                });
        });
    };
    that.getSensorIDfromInstanceId = function(instanceId) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors?instanceId=' + instanceId);
    };

    that.getLocationByID = function(locID) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations/' + locID);
    };

    that.isAllInstancesRunningonSensorID = function(sensorID) {
        return new $q(function(resolve, reject) {
            let allInstanceRunning = '';
            let runningInstance = true;
            that.getInstanceforSensorId(sensorID)
                .then(function(sensorDataResp) {
                    if(sensorDataResp.status === 200) {
                    angular.forEach(sensorDataResp.data, function(key, value) {
                        if (!key.parameters.configuration.parentInstanceId && runningInstance) {
                            if (allInstanceRunning == '')
                                allInstanceRunning = key.status.running;
                            allInstanceRunning = (allInstanceRunning == false) ? false : key.status.running;
                            runningInstance = allInstanceRunning;
                        }
                    });
                  }
                    resolve((allInstanceRunning == '') ? false : allInstanceRunning);
                });
        });
    };
    that.getSensorSnapshotforSensorID = function(sensorId) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors/' + sensorId + '/snapshot');
    };
    that.getSensorByLocationId = function(location, appId) {
        if (appId) {
            return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors?locationId=' + location + '&appId=' + appId + '&type=camera');
        } else {
            return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors?locationId=' + location + '&type=camera');
        }
    };
    that.getFloorsByLocationId = function(location) {
        return locationService.getLocationId(location);
        // return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations/' + location );
    };
    that.startStopMonitoring = function(instanceID, action) {
        let payLoad = {
            status: action,
        };

        return $http.post(appConfig.applicationBaseUrl + '/pulseExecutionFramework/instances/' + instanceID + '/status', payLoad);
    };
    that.getInstanceDetailsforSensor = function(sensor) {
        //  return new $q(function (resolve, reject) {
        let deferred = $q.defer();
        let arrInstaneURLforAppandSensor = [];
        // var arrInstanceDatset = [];
        arrInstanceDatset.length = 0;
        arrInstanceDetailsforSensor.length = 0;
        _.each(sensor.associatedAppID, function(item, key) {
            arrInstaneURLforAppandSensor.push(that.getInstanceDetailsByAppIDandSensorID(item, sensor.id));
        });


        $q.all(arrInstaneURLforAppandSensor).then(function(ret) {
            let iCtr = -1;
            for (let instaneData in sensor.associatedAppID) {
                iCtr++;
                 if (ret[iCtr].data[0]) {
                    for(let k=0; k<ret[0].data.length; k++) {
                        if(ret[iCtr].data[k]) {
                            arrInstanceDatset.push({
                                'appId': ret[iCtr].data[k].parameters.appId,
                                'instanceName': ret[iCtr].data[k].parameters.instanceName,
                                'status_status': ret[iCtr].data[k].status.status,
                                'status_running': ret[iCtr].data[k].status.running,
                                'node': ret[iCtr].data[k].parameters.node,
                                'instanceId': ret[iCtr].data[k].id,
                                'isPollerRunningForInstanceStatusChange': false,
                                });
                           }
                      }
                }
            }

            deferred.resolve(arrInstanceDatset);
        });

        return deferred.promise;
        // });
    };
    that.getSensorListwithAppIds = function() {
        let tempSensorAppIDMapper = [];
        return new $q(function(resolve, reject) {
            let all_Apps = settingsService.getSettingDetails().appIdAndApplicationMapping;
            let arrSensorsforAppID = [];
            for (key in all_Apps) {
                arrSensorsforAppID.push(getSensorListByAppId(all_Apps[key]));
            }
            let iCtr = 0;
            $q.all(arrSensorsforAppID).then(function(ret) {
                for (app in all_Apps) {
                    var sensorIDs = [];

                    let associatedSensors = ret[iCtr++].data;
                    angular.forEach(associatedSensors, function(key) {
                        sensorIDs.push(key.id);
                    });
                        tempSensorAppIDMapper.push({
                            'appID': all_Apps[app],
                            'sensorIDs': sensorIDs,
                            'appName': app,
                        });
                }

                if (iCtr >= arrSensorsforAppID.length) {
                    that.setSensorAppIDMapper(tempSensorAppIDMapper);
                    resolve(tempSensorAppIDMapper);
                }
            });

        });
    };
    that.getSensorDataSet = function() {
        return new $q(function(resolve, reject) {
            let sensorList = that.getAllSensors();
            let sensorWithLocationDetails = locationService.getAllLocations();
            let sensorDetailDemoAppIDs = [];
            sensorList.then(function(sensorListResp) {
                let sensorDataSet = _.orderBy(sensorListResp.data, ['id'], ['desc']);
                sensorWithLocationDetails.then(function(locatonDtls) {
                    let arrLocationDetails = locatonDtls.data;
                    _.each(sensorDataSet, function(item, key) {
                        let sensorRecord = item;
                        angular.forEach(that.getAssociatedAppIDforSensorId(item.id), function(key, value) {
                            sensorRecord.associatedAppID = key.value;
                        });
                        // logic for locationName
                        let locationOfLocationId = _.find(arrLocationDetails, {id: item.locationId});
                        sensorRecord.locationName = locationOfLocationId?locationOfLocationId.name:'';
                        // sensorRecord.allInstanceRunningStatus = that.isAllInstancesRunningonSensorID(sensorRecord.id);
                        that.isAllInstancesRunningonSensorID(sensorRecord.id, sensorRecord.name)
                            .then(function(responseData) {
                                sensorRecord.allInstanceRunningStatus = responseData;
                                sensorDetailDemoAppIDs.push(sensorRecord);
                            });
                    });
                });
                if (sensorDetailDemoAppIDs)
                    sensorDetailDemoAppIDs = _.orderBy(sensorDetailDemoAppIDs, ['id'], ['desc']);
                resolve(sensorDetailDemoAppIDs);
            });
        });
    };
    that.deleteInstance = function(instanceID) {
        return $http.delete(appConfig.applicationBaseUrl + '/pulseExecutionFramework/instances/' + instanceID);
    };
    that.deleteSensor = function(sensorID) {
        return $http.delete(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors/' + sensorID);
    };
    that.deleteLocationByID= function(locationID) {
        return $http.delete(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations/' + locationID);
    };

    that.deleteFloor= function(facilityId, floorId) {
        return that.deleteLocationByID(floorId);
        // return $http.delete(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations/' + facilityId+'/members/'+floorId);
    };

    that.editLocation= function(locID, locDetails) {
        return $http.put(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations/' + locID, locDetails);
    };

    that.getSensorsWithInstance = function() {
        let deferred = $q.defer();
        let promise = deferred.promise;
        if (Object.keys(sensorsListWithInstanceDetail).length == 0) {
            $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors?&type=camera')
                .then(function(data) {
                    if (data.data.length > 0) {
                        let sensors = data.data;
                        let counter = 0;
                        for (let s = 0; s < sensors.length; s++) {
                            sensorsListWithInstanceDetail[sensors[s].locationId] = sensors[s];
                            sensorsListWithInstanceDetail[sensors[s].locationId].appList = [];
                            that.getInstanceforSensorId(sensors[s].id).then(function(data) {
                                let allInstances = data.data;
                                if (allInstances.length > 0) {
                                        for (let instance in allInstances) {
                                            if (allInstances[instance].parameters.appId) {
                                                sensorsListWithInstanceDetail[sensors[s].locationId].appList.push(allInstances[instance].parameters.appId);
                                            }
                                            if (instance == allInstances.length - 1) {
                                                counter++;
                                            }
                                        }
                                    } else {
                                    counter++;
                                }
                                if (counter == sensors.length) {
                                    deferred.resolve(sensorsListWithInstanceDetail);
                                }
                            });
                        }
                    } else{
                        deferred.resolve(sensorsListWithInstanceDetail);
                    }
                });
        } else {
            deferred.resolve(sensorsListWithInstanceDetail);
        }

        return promise;
    };
    that.updateSensorsWithInstanceCacheById = function(sensorId) {
        $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors/'+sensorId)
                .then(function(data) {
                    if (data) {
                        if (data.data.length > 0) {
                            let sensors = data.data[0];
                            let counter = 0;
                            sensorsListWithInstanceDetail[sensors.locationId] = sensors;
                            sensorsListWithInstanceDetail[sensors.locationId].appList = [];
                            that.listSensorInstance(sensors.id).then(function(appList) {
                                sensorsListWithInstanceDetail[sensors.locationId].appList = appList;
                            });
                        }
                    }
                });
    };
    that.deleteSensorsWithInstanceCacheById = function(locationId) {
        delete sensorsListWithInstanceDetail[locationId];
    };
    that.updateSensorsWithInstanceCache = function() {
        sensorsListWithInstanceDetail = {};
        that.getSensorsWithInstance();
    };
    that.listSensorInstance = function(sensorId) {
        let appList = [];
        return $q(function(resolve, reject) {
            that.getInstanceforSensorId(sensorId).then(function(data) {
                let allInstances = data.data;
                if (allInstances.length > 0) {
                    for (let instance in allInstances) {
                        if (allInstances[instance].parameters.appId) {
                            appList.push(allInstances[instance].parameters.appId);
                        }
                        if (instance == allInstances.length - 1) {
                            resolve(appList);
                        }
                    }
                } else {
                    resolve(appList);
                }
            });
        });
    };
    function arrangeSensorList(allSensors, appSpecificSensors, instanceList, appId) {
        return new $q(function(resolve, reject) {
            let sensorList = _.map(allSensors, function(sensorObj) {
                let exist = _.findIndex(appSpecificSensors, {'id': sensorObj.id});
                let obj = {
                    sensorDetails: sensorObj,
                    instanceDetails: {
                        present: false,
                        start: false,
                        stop: false,
                        status: '',
                        id: '',
                    },
                    appId: '',
                    order: 3,
                };
                if (exist != -1) {
                    let instanceDetails = _.filter(instanceList, {'sensorId': sensorObj.id});
                    if (instanceDetails.length > 0) {
                        instanceDetails = instanceDetails[0];
                        obj.instanceDetails = {
                            present: true,
                            id: instanceDetails.instance.id,
                            completeData: instanceDetails.instance,
                        };
                        obj.appId = appId;
                        if (instanceDetails.instance.status.running) {
                            obj.instanceDetails.statusButtonIndicator = 'stop';
                            obj.instanceDetails.start = true;
                            obj.instanceDetails.stop = false;
                            obj.order = 1;
                        } else {
                            obj.instanceDetails.statusButtonIndicator = 'start';
                            obj.instanceDetails.start = false;
                            obj.instanceDetails.stop = true;
                            obj.order = 2;
                        }
                    }
                }
                return obj;
            });
            resolve(sensorList);
        });
    }

    function getSensorListByAppId(appId) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors?appId=' + appId);
    }

    return that;
});
