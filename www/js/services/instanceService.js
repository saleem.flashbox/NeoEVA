/**
 * Created by suhas on 25/4/17.
 */

NEC.factory('instanceService', function($http, appConfig, $q) {
    let that = this;
    const INSTANCE= {
        STATUS: {
            START_STATE: ['RUNNING', 'created', 'running'],
                STOP_STATE: [':Stopped', 'STOPPED', 'Failed', 'FATAL', 'stopped'],
        },
        ACTION: {
            START: 'Start',
                STOP: 'Stop',
                EVENT_IS_RUNNING: 'Start',
                EVENT_ISNOT_RUNNING: 'Stop',
                STATUS_ISRUNNING: true,
        },
    };
    that.INSTANCE = INSTANCE;

    that.getInstanceBySensorIdAndAppId = function(sensorId, appId) {
        return new $q(function(resolve, reject) {
            $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/instances?sensorId=' + sensorId + '&appId=' + appId)
                .then(function(data) {
                    if (data.status === 200) {
                        let obj = {
                            instanceList: data.data,
                            sensorId: sensorId,
                        };
                        resolve(obj);
                    } else {
                        reject(data.status);
                    }
                });
        });
    };
    that.setInstanceStatus = function(id, action) {
        let payLoad = {
            status: action,
        };

        return $http.post(appConfig.applicationBaseUrl + '/pulseExecutionFramework/instances/' + id + '/status', payLoad);
    };

    that.getInstanceDetailsById = function(id) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/instances/' + id);
    };

    return that;
});
