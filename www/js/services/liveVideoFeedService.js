/**
 * Created by suhas on 23/5/17.
 */
NEC.factory('liveVideoFeedService', function(settingsService, $q, instanceService) {
    let that = this;
    that.liveFeedStrategy = null;
    let liveVideoData = {
        appDetails: '',
        discoveryMethod: {
            app: setVideoFeedByAppAndSensorId,
            instance: setVideoFeedByInstanceId,
        },
        baseUrl: '',
    };
    that.setAllVideoFeedDetailsForAllApps = function() {
        settingsService.getAppSettingDetails().then(function(data) {
            let settingsDetails = data.data;
            liveVideoData.appDetails = settingsDetails.videoFeedAppName;
            liveVideoData.baseUrl = settingsDetails.applicationLiveFeed;
        });
    };
    that.getLiveVideoUrlByAppNameAndSensorId = function(appName, sensorId) {
        return new $q(function(resolve, reject) {
            let appDiscoveryDetails = liveVideoData.appDetails[appName];
            let discoveryType = appDiscoveryDetails.discoveryType;
            let appId = appDiscoveryDetails.appId;
            liveVideoData.discoveryMethod[discoveryType](appId, sensorId)
                .then(function(url) {
                    resolve(url);
                });
        });
    };
    function setVideoFeedByInstanceId(appId, sensorId) {
        return new $q(function(resolve, reject) {
            let baseUrl = liveVideoData.baseUrl;
            instanceService.getInstanceBySensorIdAndAppId(sensorId, appId)
                .then(function(data) {
                    if (data.instanceList[0]) {
                        let vfAppId = data.instanceList[0].id;
                        let url = baseUrl + '/instances/' + vfAppId;
                        resolve(url);
                    }
                });
        });
    }

    function setVideoFeedByAppAndSensorId(appId, sensorId) {
        return new $q(function(resolve, reject) {
            let baseUrl = liveVideoData.baseUrl;
            let url = baseUrl + '/apps/' + appId + '/sensor/' + sensorId;
            resolve(url);
        });
    }

    return that;
});
