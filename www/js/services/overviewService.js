/**
 * Created by suhas on 14/4/17.
 */

NEC.factory('overviewService', function($q, alertService, appConstantService) {
    let that = this;
    that.getAggregatedCrowdDataForAllRegion = function(data, filter) {
        return new $q(function(resolve, reject) {
            try {
                let totalRegionCrowdCount = _.sum(_.map(data.appSpecificAlert.regions, function(val) {
                    return (filter == 'max') ? val.crowdInfo.max : val.crowdInfo.min;
                }));
                resolve(totalRegionCrowdCount);
            } catch (e) {
                reject(e.stack);
            }
        });
    };
    that.getAggregatedTotalPeopleInAndOutCount = function(startDateTime, endDateTime) {
        return new $q(function(resolve, reject) {
            let alertTypeIn = appConstantService.ALERT_TYPE.PEOPLE_IN;
            let alertTypeOut = appConstantService.ALERT_TYPE.PEOPLE_OUT;
            let data = {countIn: '-', countOut: '-'};
            try {
                that.getTotalPeopleInAndOutCount(startDateTime, endDateTime)
                    .then(function(res) {
                        alertService.getDataForAlertType(alertTypeIn, startDateTime, endDateTime, null, null, 0, res.totalCountIn)
                            .then(function(resIn) {
                                if (resIn.status == 200) {
                                    let countInArray = _.map(resIn.data, function(obj) {
                                        return obj.appSpecificAlert.summary;
                                    });
                                    data.countIn = _.sum(countInArray);
                                    alertService.getDataForAlertType(alertTypeOut, startDateTime, endDateTime, null, null, 0, res.totalCountOut)
                                        .then(function(resOut) {
                                            if (resOut.status == 200) {
                                                let countOutArray = _.map(resOut.data, function(obj) {
                                                    return obj.appSpecificAlert.summary;
                                                });
                                                data.countOut = _.sum(countOutArray);
                                                resolve(data);
                                            }
                                        });
                                }
                            });
                    });
            } catch (e) {
                reject(e);
            }
        });
    };
    that.getTotalPeopleInAndOutCount = function(startDateTime, endDateTime) {
        return new $q(function(resolve, reject) {
            try {
                let alertTypeIn = appConstantService.TOPIC.PEOPLE_IN;
                let alertTypeOut = appConstantService.TOPIC.PEOPLE_OUT;
                let data = {totalCountIn: 0, totalCountOut: 0};
                alertService.getCountforAlertType(alertTypeIn, startDateTime, endDateTime)
                    .then(function(respIn) {
                        data.totalCountIn = respIn.data[0].summary;
                        alertService.getCountforAlertType(alertTypeOut, startDateTime, endDateTime)
                            .then(function(respOut) {
                                data.totalCountOut = respOut.data[0].summary;
                                resolve(data);
                            });
                    });
            } catch (e) {
                reject(e);
            }
        });
    };


    return that;
});
