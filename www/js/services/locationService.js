NEC.factory('locationService', function($http, $q, appConfig, settingsService) {
    let that = this;
    let locationDetailsArray = null;
    let areaDetailsArray = [];
    let locationObj = {};
    let dashboardClickedPersonDetails;
    let areaId = '';
    let locationId = '';

    const LOCATION_IDENTIFIER ={
        TYPE: 'LocationType',
        ID: 'LocationID',
    };
    that.LOCATION_IDENTIFIER = LOCATION_IDENTIFIER;
    that.setDashboardPersonData = function(dashboardPerson) {
        dashboardClickedPersonDetails = dashboardPerson;
    };
    that.getDashboardPersonData = function() {
        return dashboardClickedPersonDetails;
    };
    that.editCamera = function(cameraDetails, locationDetails) {
        if (locationDetails.info.floorPlanName) {
            let Imagepayload = {
                'name': cameraDetails.cameraLocationDetails.locationCameraName,
                'type': appConfig.sensorType,
                'locationId': cameraDetails.cameraLocationDetails.locationID,
                'uri': 'https://' + cameraDetails.cameraLocationDetails.canvasCameraIPAddress,
                'info': {
                    'lat': cameraDetails.cameraLocationDetails.canvasCameraLatitude,
                    'lng': cameraDetails.cameraLocationDetails.canvasCameraLongitude,
                    'ipAddress': cameraDetails.cameraLocationDetails.canvasCameraIPAddress,
                    'floorPlanName': locationDetails.info.floorPlanName,
                    'floorImageWidth': locationDetails.info.floorImageWidth,
                    'floorImageHeight': locationDetails.info.floorImageHeight,
                    'connnectivityNetwork': cameraDetails.cameraLocationDetails.connnectivityNetwork,
                },

            };
            Imagepayload = JSON.stringify(Imagepayload);
            // Making Rest call
            // send Post data
            return $http.put(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors/' + cameraDetails.cameraLocationDetails.id, Imagepayload);
        } else {
            let payload = {
                'name': cameraDetails.cameraLocationDetails.locationCameraName,
                'type': appConfig.sensorType,
                // "type": cameraDetails.cameraLocationDetails.connnectivityNetwork,
                'locationId': cameraDetails.cameraLocationDetails.locationID,
                'uri': 'https://' + cameraDetails.cameraLocationDetails.canvasCameraIPAddress,
                'info': {
                    'lat': cameraDetails.cameraLocationDetails.canvasCameraLatitude,
                    'lng': cameraDetails.cameraLocationDetails.canvasCameraLongitude,
                    'ipAddress': cameraDetails.cameraLocationDetails.canvasCameraIPAddress,
                    'connnectivityNetwork': cameraDetails.cameraLocationDetails.connnectivityNetwork,
                },

            };

            payload = JSON.stringify(payload);
            // Making Rest call
            // send Post data
            return $http.put(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors/' + cameraDetails.cameraLocationDetails.id, payload);
        }
    };
    that.getAllLocations = function() {
        let deferred = $q.defer();
        let promise = deferred.promise;

        if (!locationDetailsArray) {
            $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations')
                .then(function(data) {
                    locationDetailsArray = data;
                    deferred.resolve(locationDetailsArray);
                });
        } else {
            deferred.resolve(locationDetailsArray);
        }
        return promise;
    };
    that.updateLocationCache =function(locationId){
        if(locationDetailsArray) {
            _.remove(locationDetailsArray.data, function (obj) {
                return obj.id === locationId;
            });
        }else{
            that.getAllLocations();
        }
    };
    that.getLocationIDfromAPI = function(locationID) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations/' + locationID);
    };
    that.updateLocationCacheForLatitudeAndLongitude =  function(locationId,latitude,longitude){
        if (locationDetailsArray){
            let updatedLocation = _.filter(locationDetailsArray.data, {id: locationId});
            updatedLocation[0].info.lat = latitude;
            updatedLocation[0].info.lng = longitude;
        }
        };
    that.getLocationDataFromCache = function(key, value) {
        let deferred = $q.defer();
        let promise = deferred.promise;
        let locationData = {};
        if (locationDetailsArray) {
            if(key === LOCATION_IDENTIFIER.ID) {
                locationData.data = _.filter(locationDetailsArray.data, {id: value});
            }else if (key === LOCATION_IDENTIFIER.TYPE) {
                locationData.data = _.filter(locationDetailsArray.data, {type: value});
            }
        }else{
            that.getAllLocations(); // if locationDetailsArray is empty, build it
        }
        if (locationData.data) {
            deferred.resolve(locationData);
        } else {
            locationDetailsArray = null; // nullify the existing/old dataSet to build updated Array.
            that.getAllLocations(); // if locationDetailsArray is empty, build it
            if (key === LOCATION_IDENTIFIER.ID) {
            that.getLocationIDfromAPI(value)
                .then(function(data) {
                    deferred.resolve(data);
                });
            }else if (key === LOCATION_IDENTIFIER.TYPE) {
                that.getLocationTypeFromAPI(value)
                    .then(function(data) {
                        deferred.resolve(data);
                    });
            }
        }
        return promise;
    };

    that.getLocationByType = function(locationType) {
        return that.getLocationDataFromCache(LOCATION_IDENTIFIER.TYPE, locationType);
    };
    that.getLocationById = function(locationId) {
       return that.getLocationDataFromCache(LOCATION_IDENTIFIER.ID, locationId);
    };
    that.getSensorLocation = function(locationId) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations/' + locationId);
    };
    that.getLocationTypeFromAPI = function(locationType) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations?type=' + locationType);
    };
    that.saveLocation = function(locationObj) {
        locationDetailsArray = null;
        return $http.post(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations', locationObj);
    };
    that.setLocationsDetails = function(locationDetails) {
        locationDetailsArray = locationDetails;
    };
    that.getLocationDetails = function() {
        return locationDetailsArray;
    };
    that.setLocationsLatLng = function(latLngObj) {
        locationObj = latLngObj;
    };
    that.getLocationsLatLng = function() {
        return locationObj;
    };
    that.saveSensorDetails = function(cameraDetails) {
        return $http.post(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors', cameraDetails);
    };
    that.saveAreaToDb = function(areaObj) {
        return $http.post('/area', areaObj);
    };
    that.getAllArea = function() {
        return $http.get('/area');
    };
    that.getAllAreaByName = function(areaName) {
        return $http.get('/area' + areaName);
    };
    that.setAreaDetails = function(areaDetails) {
        areaDetailsArray = areaDetails;
    };
    that.getAreaDetails = function() {
        return areaDetailsArray;
    };
    that.getAreaDetailsBasedOnAddress = function(address) {
        let isHttpsEnabled = settingsService.getIsEnabledHttp();
        if (isHttpsEnabled) {
            return $http.get('https://maps.google.com/maps/api/geocode/json?address=' + address);
        } else {
            return $http.get('http://maps.google.com/maps/api/geocode/json?address=' + address);
        }
    };
    that.getFloorPlanByName = function(planNameImageName) {
        return $http.get('/planImage/' + planNameImageName);
    };
    that.getFileUploadName = function(imageName) {
        return $http.get('/floorName/' + imageName);
    };
    that.addLocationMembers = function(areaId, memberLocId) {
        let members = [];
        members.push(memberLocId);
        return $http.post(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations/' + areaId + '/members', members);
    };
    that.getSensorDetailsById = function(sensorId) {
        return $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors/' + sensorId);
    };
    that.getAllLocationsByAreaType = function(areaName) {
        return that.getLocationDataFromCache(LOCATION_IDENTIFIER.TYPE, areaName);
       // return locationDetailsArray = $http.get(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations?type=' + areaName);
    };
    that.setAreaId = function(currentAddedAreaId) {
        areaId = currentAddedAreaId;
    };

    that.getAreaId = function() {
        return areaId;
    };

    that.setLocationId = function(currentAddedLocationId) {
        locationId = currentAddedLocationId;
    };

    that.getLocationId = function() {
        return locationId;
    };

    that.updateSensorAndLocation = function(sensorObject) {

        that.getSensorDetailsById(sensorObject.id)
            .then(function(response) {
                 let oldSensorObj = response.data[0];
                that.updateLocationCoordinates(oldSensorObj.locationId, sensorObject.lat, sensorObject.lng, sensorObject.name);
                if(sensorObject.newLocName!==oldSensorObj.name||sensorObject.newUri!==oldSensorObj.newUri) {
                let sensorPayload = {
                    'name': sensorObject.name,
                    'type': oldSensorObj.type,
                    'vms': oldSensorObj.vms,
                    'locationId': oldSensorObj.locationId,
                    'uri': sensorObject.newUri,
                    'info': oldSensorObj.info,
                };
                sensorPayload = JSON.stringify(sensorPayload);
                return $http.put(appConfig.applicationBaseUrl + '/pulseExecutionFramework/sensors/' + sensorObject.id, sensorPayload);
            }
        });
    };

    that.updateLocationCoordinates = function(locationId, latitude, longitude, locationName) {
        that.getLocationIDfromAPI(locationId)
            .then(function(response) {
               let oldLocationObject = response.data[0];
               if(oldLocationObject.info.lat===latitude && oldLocationObject.info.lng===longitude)
                   return;
               let locationPayload = {
                   'name': locationName,
                   'type': oldLocationObject.type,
                   'info': {
                       'lat': latitude,
                       'lng': longitude,
                       'areaId':oldLocationObject.info.areaId,
                       'ipAddress': oldLocationObject.info.ipAddress,
                       // if(oldLocationObject.info.floorPlanName !== undefined){
                        'floorPlanName': oldLocationObject.info.floorPlanName,
                        'floorImageWidth': oldLocationObject.info.floorImageWidth,
                         'floorImageHeight': oldLocationObject.info.floorImageHeight,
                         // }
                        'connnectivityNetwork': oldLocationObject.info.connnectivityNetwork,
                     },

                 };
               locationPayload = JSON.stringify(locationPayload);
               that.updateLocationCacheForLatitudeAndLongitude(locationId,latitude,longitude);
                return $http.put(appConfig.applicationBaseUrl + '/pulseExecutionFramework/locations/' + locationId, locationPayload);
            });
    };

    function defaultSensorURI() {
        return settingsService.getSettingDetails().sensorURI;
    }

    return that;
});
