/**
 * Created by suhas on 3/4/17.
 */
NEC.factory('analyticsService', function($http, appConfig, $q, appConstantService, alertService, dateUtilService) {
    let that = this;
    // 5m, 10m, 1h, 6h, 24h, 2d, 7d
    const SECOND = 1000;
    const MINUTE = 60 * SECOND;
    const HOUR = 60 * MINUTE;
    const DAY = 24 * HOUR;
    const VALID_MAX_DATE_RANGE = 364 * DAY;
    const GRANUALRITY_SLOTS = [
        [{'slot': 'm'}, {'millisecEquivalent': 1 * MINUTE}],
        [{'slot': 'h'}, {'millisecEquivalent': 1 * HOUR}],
        [{'slot': 'd'}, {'millisecEquivalent': 1 * DAY}],
        [{'slot': '999d'}, {'millisecEquivalent': 999 * DAY}],
    ];
    const MAX_USERRECORDS_FOR_EXPORT = 200;
    that.MAX_USERRECORDS_FOR_EXPORT = MAX_USERRECORDS_FOR_EXPORT;
    that.GRANUALRITY_SLOTS = GRANUALRITY_SLOTS;
    that.genderAge = {
        data: {
            ageGroup: {
                x_axis: ['15-25', '26-35', '36-45', '46-55', '56-70'], // 15-25    25-35    35-45    45-55    55-70
                x: ['15 - 25', '26 - 35', '36 - 45', '46 - 55', 'Over 56'],
                y: {
                    gender_male: [],
                    gender_female: [],
                },
                male_mapping: 'gender-age-male',
                female_mapping: 'gender-age-female',
                splitter: '-',
                mapper: [],
            },
            consolidated: {
                x: [],
                y: [],
            },
        },
    };

    that.calculateGranularity = function(startTime, endTime) {
        return new $q(function(resolve, reject) {
            let timeSlotDiffOf10 = (moment(endTime).utc() - moment(startTime).utc()) /
                appConstantService.AGGREGATOR_ALERTTYPE.DEFAULT.GRANULARITY.NO_OF_X_AXISCOORDINATES;
            let granularity = null;
            angular.forEach(GRANUALRITY_SLOTS, function(key, value) {
                if (!granularity && value < (GRANUALRITY_SLOTS.length )) {
                    let nextNode = GRANUALRITY_SLOTS[value + 1];
                    if ((timeSlotDiffOf10 > key[1].millisecEquivalent) && (timeSlotDiffOf10 < nextNode[1].millisecEquivalent)) {
                        granularity = key[0].slot;
                        let num_Granularity = Math.round(timeSlotDiffOf10 / key[1].millisecEquivalent);
                        resolve(num_Granularity + granularity);
                    }
                }
            });
        });
    };

    that.getTotalAlertCountByDateRange = function(startDate, endDate, sensorId, alertType) {
        let query = '';
        if (sensorId == 'All Cameras') {
            if (alertType) {
                query = '/alert/alerts/summary?startTime=' + startDate + '&endTime=' + endDate + '&alertType=' + alertType;
            } else {
                query = '/alert/alerts/summary?startTime=' + startDate + '&endTime=' + endDate;
            }
        } else {
            sensorId = sensorId.id;
            query = '/alert/alerts/summary?startTime=' + startDate + '&endTime=' + endDate + '&alertType=' + alertType + '&sensorId=' + sensorId;
        }
        return $http.get(appConfig.applicationBaseUrl + query);
    };
    that.getAllAlertCountByDateRangeForgivenGranularity = function(startDate, endDate, granularity, sensorId, alertType) {
        let query = '';
        if (sensorId == 'All Cameras') {
            query = '/alert/alerts/summary?startTime=' + startDate + '&endTime=' + endDate + '&granularity=' + granularity + '&alertType=' + alertType;
        } else {
            sensorId = sensorId.id;
            query = '/alert/alerts/summary?startTime=' + startDate + '&endTime=' + endDate + '&granularity=' + granularity + '&alertType=' + alertType + '&sensorId=' + sensorId;
        }
        return $http.get(appConfig.applicationBaseUrl + query);
    };
    that.getTotalAlertDetailsByDateRangeAndOffsetAndLimit = function(startDate, endDate, offset, limit, sensorId, alertType) {
        let query = '';
        if (sensorId == 'All Cameras') {
            query = '/alert/alerts?offset=' + offset + '&limit=' + limit + '&startTime=' + startDate + '&alertType=' + alertType + '&endTime=' + endDate;
        } else {
            sensorId = sensorId.id;
            query = '/alert/alerts?offset=' + offset + '&limit=' + limit + '&startTime=' + startDate + '&endTime=' + endDate + '&alertType=' + alertType + '&sensorId=' + sensorId;
        }
        return $http.get(appConfig.applicationBaseUrl + query);
    };
    that.getAllAlertDetailsByDateRange = function(startDate, endDate, limit, sensorId, alertType) {
        let query = '';
        if (sensorId == 'All Cameras') {
            query = '/alert/alerts?startTime=' + startDate + '&endTime=' + endDate + '&limit=' + limit + '&alertType=' + alertType;
        } else {
            sensorId = sensorId.id;
            query = '/alert/alerts?startTime=' + startDate + '&endTime=' + endDate + '&limit=' + limit + '&alertType=' + alertType + '&sensorId=' + sensorId;
        }
        return $http.get(appConfig.applicationBaseUrl + query);
    };
    that.formRowDataToExport = function(data) {
        return new $q(function(resolve, reject) {
            let index = 1;
            let rows = [];
            _.each(data, function(rowDetails) {
                let rowData = [];
                rowData.push(index);
                rowData.push(rowDetails.appSpecificAlert.enrolledImage);
                rowData.push(rowDetails.appSpecificAlert.detectedImage);
                rowData.push(rowDetails.appSpecificAlert.person);
                rowData.push(((rowDetails.appSpecificAlert.score) * 100).toFixed(1) + '%');
                rowData.push(rowDetails.timestamp);
                rowData.push(rowDetails.appSpecificAlert.watchlistId);
                rows.push(rowData);
                index++;
                if (rows.length == data.length) {
                    resolve(rows);
                }
            });
        });
    };
    that.getAnalyticsOfAlertCountForSingleSeries = function(startDateVal, endDateVal, granularity, sensorId, alertAggregatorType, isDashboardRequest) {
        return new $q(function(resolve, reject) {
            let data = {
                y: [],
                x: [],
            };
            let dates = dateUtilService.modifyDataFormatAsRequired(startDateVal, endDateVal, dateUtilService.DATE_FORMAT.ALERT_MANAGER);
            alertService.getCountforAlertType(alertAggregatorType, dates.start, dates.end, sensorId, null, granularity)
                .then(function(count) {
                    if (count.status == 200) {
                        data.y = count.data[0].y;
                        data.x = count.data[0].x;
                        // data.y = count.data;
                        // data.y = _.map(data.y,function(obj){return obj.summary});
                        let startDate = angular.copy(startDateVal);
                        data.x = that.getDateRangeForXAxisUTCOffset(data.x, isDashboardRequest);
                        resolve(data);
                    } else {
                        reject(count.status);
                    }
                })
                .catch(function(err) {
                    reject(err);
                });
        });
    };

    that.getAnalyticsOfAlertCountForMultipleSeries = function(startDateVal, endDateVal, granularity, sensorId, aggregatorAlertType, isDashboardRequest) {
        return new $q(function(resolve, reject) {
            let isTypeGender = aggregatorAlertType === appConstantService.AGGREGATOR_ALERTTYPE.AGE_GENDER;
            let isTypePeople = aggregatorAlertType === appConstantService.AGGREGATOR_ALERTTYPE.PEOPLE_IN || aggregatorAlertType === appConstantService.AGGREGATOR_ALERTTYPE.PEOPLE_OUT;
            if (isTypeGender) {
                that.getAnalyticsAggregatorAlertDataForAlertTypeGender(startDateVal, endDateVal, granularity, sensorId, aggregatorAlertType)
                // that.getAnalyticsAlertDataForAlertTypeGender(startDateVal,endDateVal,granularity,sensorId,alertType)
                    .then(function(data) {
                        if (data.length > 0 && data[0].y !== undefined) {
                            let arrGraphDataSet = [];
                            arrGraphDataSet.y = data[0].y;
                            arrGraphDataSet.x = that.genderAge.data.ageGroup.x;
                            resolve(arrGraphDataSet);
                        }
                    })
                    .catch(function(e) {
                        reject(e);
                    });
            } else if (isTypePeople) {
                that.getAnalyticsAggregatorAlertDataForAlertPeopleIn_Out(startDateVal, endDateVal, granularity, sensorId, aggregatorAlertType)

                    .then(function(data) {
                        if (data.length > 0 && data[0].yAxisData != undefined) {
                            let arrGraphDataSet = [];
                            arrGraphDataSet.y = data[0].yAxisData;
                            // arrGraphDataSet.x = data[1].xAxisData[0];
                            let startDate = angular.copy(startDateVal);
                            // arrGraphDataSet.x = that.getDateRangeForXAxis(arrGraphDataSet.y[0].data, startDate, isDashboardRequest);
                            arrGraphDataSet.x = that.getDateRangeForXAxisUTCOffset(data[1].xAxisData[0], isDashboardRequest);
                            resolve(arrGraphDataSet);
                        }
                    })
                    .catch(function(e) {
                        reject(e);
                    });
            }
        });
    };

    that.getAggregatorAgeGenderURL = function(startDateVal, endDateVal, granularity, sensorId, aggregatorAlertType) {
        let dates = dateUtilService.modifyDataFormatAsRequired(startDateVal, endDateVal, dateUtilService.DATE_FORMAT.ALERT_MANAGER);
        let alertURI = appConfig.applicationBaseUrl + '/alertStatAggregator/timeseries/' + aggregatorAlertType;
        if (dates.start) alertURI += alertURI.indexOf('?') == -1 ? '?start=' + dates.start + '&' : 'start=' + dates.end + '&';
        if (dates.end) alertURI += alertURI.indexOf('?') == -1 ? '?end=' + dates.end + '&' : 'end=' + dates.end + '&';
        // defaulting granularity
        granularity = (granularity != undefined && granularity != '' && granularity != null) ? granularity : appConstantService.AGGREGATOR_ALERTTYPE.DEFAULT_GRANULARITY;
        if (granularity) alertURI += alertURI.indexOf('?') == -1 ? '?granularity=' + granularity + '&' : 'granularity=' + granularity + '&';
        if (sensorId) alertURI += alertURI.indexOf('?') == -1 ? '?sensors=' + sensorId + '&' : 'sensors=' + sensorId + '&';
        alertURI = alertURI.indexOf('&') == -1 ? alertURI : alertURI.substring(0, alertURI.length - 1);
        return encodeURI(alertURI);
    };

    that.getAnalyticsAggregatorAlertDataForAlertTypeGender = function(startDateVal, endDateVal, granularity, sensorId, aggregatorAlertType) {
        return new $q(function(resolve, reject) {
            let arrGraphDataSet = [];
            let arr = {
                male_female: [],
                male_AgeGenderAggregatorDataSet: [],
                female: [],
                female_AgeGenderAggregatorDataSet: [],
                AgeGenderAggregatorDataSet: [],
                yAxisData: {
                    male: {
                        data: [],
                    },
                    female: {
                        data: [],
                    },
                },
            }; // will store all the members information
            angular.forEach(that.genderAge.data.ageGroup.x_axis, function(dataSet) {
                let male_mapperUrl = that.genderAge.data.ageGroup.male_mapping + that.genderAge.data.ageGroup.splitter + dataSet;
                let url_male = that.getAggregatorAgeGenderURL(startDateVal, endDateVal, granularity, sensorId, male_mapperUrl);
                arr.male_female.push($http.get(url_male));
                let female_mapperUrl = that.genderAge.data.ageGroup.female_mapping + that.genderAge.data.ageGroup.splitter + dataSet;
                let url_female = that.getAggregatorAgeGenderURL(startDateVal, endDateVal, granularity, sensorId, female_mapperUrl);
                arr.male_female.push($http.get(url_female));
            });

            $q.all(arr.male_female).then(function(ret) {
                // the loop has to happen twice for male and female data URL sets for same x-axis parameters
                for (let iCtr = 0; iCtr < that.genderAge.data.ageGroup.x_axis.length * 2 && ret[iCtr].status == 200; iCtr++) {
                    let keyConfigStr = ret[iCtr].config.url;
                    keyConfigStr = keyConfigStr.substring(keyConfigStr.indexOf('timeseries') + 11, keyConfigStr.indexOf('?'));
                    if (keyConfigStr.indexOf(that.genderAge.data.ageGroup.male_mapping) > -1) {
                        arr.yAxisData.male.data.push((ret[iCtr].data[0].y[0]) ? ret[iCtr].data[0].y[0] : 0);
                    } else {
                        arr.yAxisData.female.data.push((ret[iCtr].data[0].y[0]) ? ret[iCtr].data[0].y[0] : 0);
                    }
                }
                arrGraphDataSet.push({'name': 'Male', 'data': arr.yAxisData.male.data});
                arrGraphDataSet.push({'name': 'Female', 'data': arr.yAxisData.female.data});
                arr.AgeGenderAggregatorDataSet.push({'y': arrGraphDataSet});
                resolve(arr.AgeGenderAggregatorDataSet);
            });
        });
    };


    that.getAnalyticsAggregatorAlertDataForAlertPeopleIn_Out = function(startDateVal, endDateVal, granularity, sensorId, aggregatorAlertType) {
        return new $q(function(resolve, reject) {
            let arrGraphDataSet = [];
            let arr = {
                people_url_Set: [],
                people: {
                    people_in: [],
                    people_out: [],
                },
                yAxisData: {
                    people_in: {
                        name: 'People-In',
                        data: [],
                    },
                    people_out: {
                        name: 'People-Out',
                        data: [],
                    },
                },
                xAxisData: {
                    people: {
                        data: [],
                    },
                },
                iCtr: 0,
            };
            // will store all the members information
            let url_people_in = that.getAggregatorAgeGenderURL(startDateVal, endDateVal, granularity, sensorId, appConstantService.AGGREGATOR_ALERTTYPE.PEOPLE.PEOPLE_IN);
            arr.people_url_Set.push($http.get(url_people_in));
            let url_people_out = that.getAggregatorAgeGenderURL(startDateVal, endDateVal, granularity, sensorId, appConstantService.AGGREGATOR_ALERTTYPE.PEOPLE.PEOPLE_OUT);
            arr.people_url_Set.push($http.get(url_people_out));

            $q.all(arr.people_url_Set).then(function(ret) {
                for(;arr.iCtr < 2; arr.iCtr++) {
                    let keyConfigStr = ret[arr.iCtr].config.url;
                    keyConfigStr = keyConfigStr.substring(keyConfigStr.indexOf('timeseries') + 11, keyConfigStr.indexOf('?'));
                    if (keyConfigStr.indexOf(appConstantService.AGGREGATOR_ALERTTYPE.PEOPLE.PEOPLE_IN) > -1) {
                       // arr.yAxisData.people_in.data.push(ret[arr.iCtr].data[0].y);
                        if( ret[arr.iCtr]&& ret[arr.iCtr].data&& ret[arr.iCtr].data[0]) {
                            arr.yAxisData.people_in.data = ret[arr.iCtr].data[0].y;
                        }
                    } else {
                        if (ret[arr.iCtr] && ret[arr.iCtr].data && ret[arr.iCtr].data[0]) {
                            // arr.yAxisData.people_out.data.push(ret[arr.iCtr].data[0].y);
                            arr.yAxisData.people_out.data = ret[arr.iCtr].data[0].y;
                        }
                    }
                    if (arr.xAxisData.people.data.length == 0)
                        arr.xAxisData.people.data.push(ret[arr.iCtr].data[0].x);
                }
                let arryAxis = [];
                arryAxis.push(arr.yAxisData.people_in);
                arryAxis.push(arr.yAxisData.people_out);
                arrGraphDataSet.push({'yAxisData': arryAxis});
                arrGraphDataSet.push({'xAxisData': arr.xAxisData.people.data});
                resolve(arrGraphDataSet);
            });
        });
    };

    that.getAnalyticsGraphDataByAlertType = function(startDateVal, endDateVal, granularity, sensorId, aggregatorAlertType, isDashboardRequest) {
        return new $q(function(resolve, reject) {
            let isSingle = aggregatorAlertType === appConstantService.AGGREGATOR_ALERTTYPE.FACE_MATCH || aggregatorAlertType === appConstantService.AGGREGATOR_ALERTTYPE.CROWD;
            let isMultiple = aggregatorAlertType === appConstantService.AGGREGATOR_ALERTTYPE.AGE_GENDER || aggregatorAlertType === appConstantService.AGGREGATOR_ALERTTYPE.PEOPLE_IN || aggregatorAlertType === appConstantService.AGGREGATOR_ALERTTYPE.PEOPLE_OUT;
            if (aggregatorAlertType === appConstantService.AGGREGATOR_ALERTTYPE.AGE_GENDER && granularity === '') {
                    if(isDashboardRequest)
                        granularity = appConstantService.AGGREGATOR_ALERTTYPE.DEFAULT.GRANULARITY.DASHBOARD.AGE_GENDER;
                    else
                        granularity = appConstantService.AGGREGATOR_ALERTTYPE.DEFAULT.GRANULARITY.APP.AGE_GENDER;
            }
            if (granularity === '') {
                that.calculateGranularity(startDateVal, endDateVal)
                    .then(function(granularity) {
                        if (isSingle) {
                            that.getAnalyticsOfAlertCountForSingleSeries(startDateVal, endDateVal, granularity, sensorId, aggregatorAlertType, isDashboardRequest)
                                .then(function(data) {
                                    data.type = appConstantService.ANALYTICS_GRAPH.SERIES_TYPE.SINGLE;
                                    resolve(data);
                                }).catch(function(e) {
                                reject(e);
                            });
                        } else if (isMultiple) {
                            that.getAnalyticsOfAlertCountForMultipleSeries(startDateVal, endDateVal, granularity, sensorId, aggregatorAlertType, isDashboardRequest)
                                .then(function(data) {
                                    data.type = appConstantService.ANALYTICS_GRAPH.SERIES_TYPE.MULTIPLE;
                                    resolve(data);
                                }).catch(function(e) {
                                reject(e);
                            });
                        }
                    });
                // granularity = that.calculateGranularity(startDateVal,endDateVal)
            } else {
                if (isSingle) {
                    that.getAnalyticsOfAlertCountForSingleSeries(startDateVal, endDateVal, granularity, sensorId, aggregatorAlertType)
                        .then(function(data) {
                            data.type = appConstantService.ANALYTICS_GRAPH.SERIES_TYPE.SINGLE;
                            resolve(data);
                        }).catch(function(e) {
                        reject(e);
                    });
                } else if (isMultiple) {
                    that.getAnalyticsOfAlertCountForMultipleSeries(startDateVal, endDateVal, granularity, sensorId, aggregatorAlertType)
                        .then(function(data) {
                            data.type = appConstantService.ANALYTICS_GRAPH.SERIES_TYPE.MULTIPLE;
                            resolve(data);
                        }).catch(function(e) {
                        reject(e);
                    });
                }
            }
        });
    };
    that.getAnalyticsAlertDataForAlertTypeGender = function(startDateVal, endDateVal, granularity, sensorId) {
        return new $q(function(resolve, reject) {
            let data = {y: [], x: []};
            let dates = dateUtilService.modifyDataFormatAsRequired(startDateVal, endDateVal, dateUtilService.DATE_FORMAT.ALERT_MANAGER);
            let filterMale = [{'key': 'gender', 'value': 'MALE', 'matchType': 'exact'}];
            let filterFeMale = [{'key': 'gender', 'value': 'FEMALE', 'matchType': 'exact'}];
            alertService.getCountforAlertType(appConstantService.AGGREGATOR_ALERTTYPE.AGE_GENDER, dates.start, dates.end, sensorId, null, granularity, JSON.stringify(filterMale))
                .then(function(result1) {
                    if (result1.status === 200) {
                        data.y[0] = result1.data;
                        data.y[0] = _.map(data.y[0], function(obj) {
                            return obj.summary;
                        });
                        alertService.getCountforAlertType(appConstantService.AGGREGATOR_ALERTTYPE.AGE_GENDER, dates.start, dates.end, sensorId, null, granularity, JSON.stringify(filterFeMale))
                            .then(function(result2) {
                                if (result2.status === 200) {
                                    data.y[1] = result2.data;
                                    data.y[1] = _.map(data.y[1], function(obj) {
                                        return obj.summary;
                                    });
                                    let startDate = angular.copy(startDateVal);
                                    data.x = _.map(angular.copy(data.y[1]), function(data) {
                                        startDate = moment(startDate).add(granularity, 'minutes');
                                        return moment(startDate).format('HH:mm:ss');
                                    });
                                    resolve(data);
                                    /* multipleSeriesGraphUpdate(xAxis,$scope.analyticsVal.AGE_GENDER.y)*/
                                } else {
                                    reject(data);
                                }
                            });
                    }
                });
        });
    };
    that.getAnalyticsAlertDataForAlertTypePeopleInAndOut = function(startDateVal, endDateVal, granularity, sensorId) {
        return new $q(function(resolve, reject) {
            let dates = dateUtilService.modifyDataFormatAsRequired(startDateVal, endDateVal, dateUtilService.DATE_FORMAT.ALERT_MANAGER);
            let data = {y: [], x: []};
            alertService.getCountforAlertType(appConstantService.AGGREGATOR_ALERTTYPE.PEOPLE_IN, dates.start, dates.end, sensorId, null, granularity)
                .then(function(count) {
                    if (count.status == 200) {
                        data.y[0] = count.data;
                        data.y[0] = _.map(data.y[0], function(obj) {
                            return obj.summary;
                        });
                        alertService.getCountforAlertType(appConstantService.AGGREGATOR_ALERTTYPE.PEOPLE_OUT, dates.start, dates.end, sensorId, null, granularity)
                            .then(function(count) {
                                if (count.status == 200) {
                                    data.y[1] = count.data;
                                    data.y[1] = _.map(data.y[1], function(obj) {
                                        return obj.summary;
                                    });
                                    let startDate = angular.copy(startDateVal);
                                    data.x = that.getDateRangeForXAxis(data.y[1], startDate, false);
                                    resolve(data);
                                } else {
                                    reject(count.status);
                                }
                            });
                    }
                });
        });
    };

    that.getDateRangeForXAxis = function(length, startDate, isDashboardRequest) {
        let x = _.map(angular.copy(length), function(data) {
            startDate = moment(startDate).add(30, 'minutes');
            if (isDashboardRequest) {
                var date = moment(startDate);
                return moment(moment(date)).format('HH:mm:ss');
            } else {
                var date = moment(startDate);
                // return moment(moment(date)).format('YYYY-MM-DD HH:mm:ss');
                return moment(moment(date)).format(dateUtilService.DATE_FORMAT.ALERT_FACEMATCH_FORMAT);
            }
        });
        return x;
    };

    that.parseFormattedUserAlertReport = function(alertType, start, end, sensorId, instanceId, offset, limit) {
        return new $q(function(resolve, reject) {
            let userDataArr = [];
            let parseResponseObject={};
            alertService.getDataForAlertType(alertType, start, end, sensorId, instanceId, offset, limit).then(function(response) {
                _.each(response.data, function(userData) {
                    let localTime = moment.utc(userData.timestamp).toDate();
                    userData.timestamp=dateUtilService.alertManagerToMMMDYYYYHHmmA(localTime);
                    userDataArr.push(userData);
                });
                parseResponseObject.status= response.status;
                parseResponseObject.data = userDataArr;
                resolve(parseResponseObject);
            });
        });
    };


    that.getDateRangeForXAxisUTCOffset = function(dataArrXAxis, isDashboardRequest) {
        let x = _.map(angular.copy(dataArrXAxis), function(startDate) {
            let localTime = moment.utc(startDate).toDate();
            if (isDashboardRequest) {
                return dateUtilService.alertManagerToHHmm(localTime);
            } else {
                return dateUtilService.alertManagerToMMMDYYYYHHmmA(localTime);
            }
        });
        return x;
    };

    return that;
});
