/**
 * Created by suhas on 14/2/17.
 */
NEC.factory('watchListService', function($http, $filter, appConfig, $q, dateUtilService) {
    let that = this;
    let watchlistData = {
        watchlistId: '',
        name: '',
        members: 0,
    };
    const PAGINATION = {
        DEFAULT: {
            OFFSET: 0,
            LIMIT: 10,
        },
    };
    that.PAGINATION = PAGINATION;
    let personlistMapper = [];// will build the consolidated structure
    let watchlistMemberDataSet = [];
    let parentScreenNameNavigation;
    let defaultImageBase64 = 'iVBORw0KGgoAAAANSUhEUgAAAgAAAAIAAgMAAACJFjxpAAAADFBMVEXFxcX////p6enW1tbAmiBwAAAFiElEQVR4AezAgQAAAACAoP2pF6kAAAAAAAAAAAAAAIDbu2MkvY0jiuMWWQoUmI50BB+BgRTpCAz4G6C8CJDrC3AEXGKPoMTlYA/gAJfwETawI8cuBs5Nk2KtvfiLW+gLfK9m+r3X82G653+JP/zjF8afP1S//y+An4/i51//AsB4aH+/QPD6EQAY/zwZwN8BAP50bh786KP4+VT+3fs4/noigEc+jnHeJrzxX+NWMDDh4g8+EXcnLcC9T8U5S/CdT8bcUeBEIrwBOiI8ki7Ba5+NrePgWUy89/nYyxQ8Iw3f+pWY4h1gb3eAW7sDTPEOsLc7wK1TIeDuDB+I/OA1QOUHv/dFsZQkhKkh4QlEfOULYz2nGj2/Nn1LmwR/86VxlCoAW6kCsHRGANx1RgCMo5Qh2EsZgrXNQZZShp5Liv7Il8eIc5C91EHY2hxk6bwYmNscZIReDBwtCdhbErC1JGBpScBcOgFMLQsZMQs5Whayd+UQsLYsZGlZyNyykKllISNmIUfAwifw8NXvTojAjGFrdYi11SGWVoeYWx1i6lmQCiEjFkKOVgjZ+xxIhZCtFULWHkCqxCw9gNQKmP9vNHzipdEPrRcxtVbAeDkAvve0iM2QozVD9hfjhp4YP/UrkJYDbD2AtBxgfSkAvvHEeNcDSAsilgtAWxIy91J8AXgZAJ5e33+4tuACcAG4AFwALgBXRXQB6AFcB5MXAuA6nl9/0Vx/011/1V5/1/dfTPJvRtdnu/zL6beeFO/7r+fXBYbrEkt/j+i6ytXfpuvvE/ZXOnsA/a3a/l5xf7O6v1t+Xe/vOyz6HpO8yyboM8o7rfJes77bru83THk48p7TvOs27zvOO6/73vO++z7l4cgnMPQzKPopHC0N9noSSz6LJp/Gk88jyicy5TOp6qlc+VyyfDJbPpuuns6XzyfMJzTmMyrrKZ35nNJ8Ums+q7af1tvPK+4nNodEnPKp3fnc8npyez67/qVP7+/fL8hfcMjfsOhf8cjfMclfcnn9+BkOnLECP8Q58OYeyJ40eoyF6Ee/En/JHlP6mIlRVXprF4BxtAvArV0AxtEuALd2ARhHuwDc2gVgHPX/hFv9fMBddjIGeKg/WCxlCsI46u+Ga5mCcJd+sIG9UkGAW32ZbApFAHhod4Bb3eo04h3god0BbiUHYApVCNjbHeBW+QDAXT4a7qg7r7e214057vg0QhkEHkoSwq0kIdydXw4/Q3H8hjYJ3vL0WConBJhCHQaOToeBrU0BljYFmEoVgHGUKgAPnREAt84IgLuqFgAYSUEOAHszDwuAtSkHAZhLGYIpdCLgKGUIHtocZG1zkLmUIRhxDnJU1RDA1uYga5uDzKUOwhTnIEfnxcDe5iBrcyQAYGlzkKkUYhhxDrKXQgxbSwLWUohhbknA1JKAEZOAvSUBW0sC1pYEzC0JmFoSMMJyCDhaFrK3JGDtyiFgaVnI3LKQqWUhI2YhR8tC9paFrC0LWVoWMrcsZGpZyIhZyNGykL2rSIGtlQHWVgZYWhlgbmWAqZUBRiwDHK0MsLcywNbKAGsOoNUhllaHmFsdYmp1iBHrEEerQ+w5gFYI2VodYm11iKXVIeYcQCuETK0QMmIh5MgBtELI3gohWyuErDmAVolZWiFkzgG0SszUKjGjfj6gVmKOVonZcwCtFbB9HQC+ozWDbz1bvGu9iKW1AuYcQOtFTLEX1GbIaFegN0OOHEBrhuw5gNYM2XIArRuz5gDacoB3bTnAEktxXQ4wfw0AvveM8b4tiJjSJOwLIsbXsAKeNeKCiOO3D+AVbUl0AfjGs8ZPbUnIdgFoa1LWC0BblfMuB9AeC1j6gqQE0J9LmC8AOYD2ZMb7i4bt2ZTpWoHfPoB7Tj2fXzT8N1X41vkq/QHOAAAAAElFTkSuQmCC';
    let dashboardClickedPersonDetails;
    let personListCache = {};


    that.getAllPersons = function() {
        return $http.get(appConfig.applicationBaseUrl + '/biometrics/persons');
    };

    that.getDefaultImage = function() {
        return defaultImageBase64;
    };
    that.getwatchlistMemberData = function() {
        return watchlistMemberDataSet;
    };
    that.setWatchlistMemberData = function(dataSet) {
        watchlistMemberDataSet = dataSet;
    };
    that.getParentScreenNavigationService = function() {
        return parentScreenNameNavigation;
    };
    that.setParentScreenNavigationService = function(screenName) {
        parentScreenNameNavigation = screenName;
    };
    that.setDashboardPersonData = function(dashboardPerson) {
        dashboardClickedPersonDetails = dashboardPerson;
    };
    that.getDashboardPersonData = function() {
        return dashboardClickedPersonDetails;
    };
    that.setWatchlistIDforFullDetails = function(data) {
        watchlistData = data;
    };
    that.getPersonsforWatchlistID = function(watchlistId) {
        return $http.get(appConfig.applicationBaseUrl + '/biometrics/watchlists/' + watchlistId);
    };
    that.deleteWatchlist = function(watchlistId) {
        return $http.delete(appConfig.applicationBaseUrl + '/biometrics/watchlists/' + watchlistId);
    };
    function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        let byteString ='';
        let mimeString ='image/jpeg';
        if (dataURI.split(',')[0].indexOf('base64') >= 0) {
            let dataStr = dataURI.split(',')[1];
            byteString = atob(dataStr);
            mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
           } else {
            try {
                byteString = atob(dataURI);
            }catch(e) {
            console.log('error while image parsing!!');
        }
        }
        let ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], {type: mimeString});
    }
    function updatePersonImg(person, personImg) {
        let personID = person[0].personId;
        let file = new File([dataURItoBlob(personImg)], personID+'.jpg', {type: 'image/jpeg'});
        let fd = new FormData();
        fd.append('picture', file);
        let uploadUrl = appConfig.applicationBaseUrl + '/multipart/biometrics/persons/' + personID+'/pictures?retainPicture=true';
        return $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined},
        });
    }
    function deletePictureOfPersonID(personId, pictureId) {
        return $http.delete(appConfig.applicationBaseUrl + '/biometrics/persons/' + personId+'/pictures/'+pictureId);
    }

    function uploadPeopleImageAsFiles(peopleData, ImageSet) {
        let deferred = $q.defer();
        let personPicturePOSTURL = [];
            _.each(ImageSet, function(individualImage) {
                if(individualImage) {
                    personPicturePOSTURL.push(updatePersonImg(peopleData, individualImage));
                  }
                });
            let iPostImageRequestCounter =0, imageUpdatedStatus = '';
            // let imageUpdatedStatus = '';
            $q.all(personPicturePOSTURL).then(function(ret) {
                for(; iPostImageRequestCounter < personPicturePOSTURL.length; iPostImageRequestCounter++) {
                    if(ret[iPostImageRequestCounter].data.Status === 200) {
                        let pictureId = (peopleData[0].pictures.data[iPostImageRequestCounter])?peopleData[0].pictures.data[iPostImageRequestCounter].pictureId:undefined;
                         if(pictureId) {
                            deletePictureOfPersonID(peopleData[0].personId, pictureId);
                        }
                        imageUpdatedStatus += ('Image '+(iPostImageRequestCounter + 1) +' updated successfully.');
                    }else if(ret[iPostImageRequestCounter].data.Status === 400) {
                        imageUpdatedStatus += ('Image '+(iPostImageRequestCounter + 1) +' rejected by Server.');
                    }else {
                        imageUpdatedStatus += ('Image '+(iPostImageRequestCounter + 1) +' update failed.');
                    }
                   }
                if (iPostImageRequestCounter >= personPicturePOSTURL.length) {
                   deferred.resolve(imageUpdatedStatus);
                }
            });
     return deferred.promise;
    }
    function updatePersonDetails(updatedPerson) {
       let personPayload = {
           'firstName': updatedPerson.firstName,
           'lastName': updatedPerson.lastName,
           'middleName': updatedPerson.middleName,
           'notes': updatedPerson.notes,
           'gender': updatedPerson.gender,
           'age': updatedPerson.age,
           'address': updatedPerson.address,
           'companyName': updatedPerson.companyName,
           'type': updatedPerson.type,
           'phone': updatedPerson.phone,
           'email': updatedPerson.email,
           'documentType': updatedPerson.documentType,
           'documentNumber': updatedPerson.documentNumber,
           'documentExpiryDate': updatedPerson.documentExpiryDate,
       };
        return ($http.put(appConfig.applicationBaseUrl + '/biometrics/persons/'+updatedPerson.personId, personPayload));
    }
    that.savePerson = function(data, imgData, editOperation, newImgData) {
        return new $q(function(resolve, reject) {
            if (editOperation) {
                updatePersonDetails(data[0]);
                uploadPeopleImageAsFiles(data, newImgData)
                    .then(function(resp) {
                        resolve(resp);
                    });
            } else {
                resolve($http.post(appConfig.applicationBaseUrl + '/biometrics/persons', data));
            }
        });
    };
    that.saveWatchList = function(data) {
        return $http.post(appConfig.applicationBaseUrl + '/biometrics/watchlists', data);
    };
    that.updateWatchList = function(watchlistId, data) {
        return $http.put(appConfig.applicationBaseUrl + '/biometrics/watchlists/' + watchlistId, data);
    };
    that.getAllWatchLists = function() {
        // create deferred object using $q
        let deferred = $q.defer();
        let watchlistMapper = [];// will build the consolidated structure to be used as cache
        let arr = []; // will store all the members information
        getAllWatchLists().then(function(watchlistResp) {
            if (watchlistResp.status == 200) {
                let watchlistDataSet = _.orderBy(watchlistResp.data, ['watchlistId'], ['desc']);// ;
                angular.forEach(watchlistDataSet, function(dataSet) {
                    arr.push($http.get(appConfig.applicationBaseUrl + '/biometrics/watchlists/' + dataSet.watchlistId + '/count'));
                });

                $q.all(arr).then(function(ret) {
                    watchlistMapper = [];
                    for (let iCtr = 0; iCtr < watchlistDataSet.length; iCtr++) {
                        watchlistMapper.push({
                            'name': watchlistDataSet[iCtr].name,
                            'watchlistId': watchlistDataSet[iCtr].watchlistId,
                            'members': angular.isNumber(ret[iCtr].data[0].count) ? ret[iCtr].data[0].count : '-',
                            'creationDate': moment(watchlistDataSet[iCtr].creationTime).format('MM-DD-YYYY'),
                            'description': watchlistDataSet[iCtr].description,
                            'icon': watchlistDataSet[iCtr].icon,
                            'alertDeliveryMethod': watchlistDataSet[iCtr].alertDeliveryMethod,
                            'matchingThreshold': watchlistDataSet[iCtr].matchingThreshold,
                            'securityLevel': watchlistDataSet[iCtr].securityLevel,
                            'colorCode': watchlistDataSet[iCtr].colorCode,
                        });
                    }
                    // resolve the deferred
                    deferred.resolve(watchlistMapper);
                    that.setWatchlistMemberData(watchlistMapper);
                });
            } else {
                deferred.reject(watchlistResp);
            }
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };
    that.getWatchlistforEdit = function(watchlistId) {
        let deferred = $q.defer();
        let watchlistmembers = getMembersofWatchlistId(watchlistId);
        getWatchlistSetforWatchlistId(watchlistId).then(function(respMemberWatchlistDataforEdit) {
            watchlistmembers.then(function(memberData) {
                if (respMemberWatchlistDataforEdit !== undefined) {
                    respMemberWatchlistDataforEdit.members = (memberData.data[0].count) ? memberData.data[0].count : 0;
                }
                deferred.resolve(respMemberWatchlistDataforEdit);
            });
        });
        return deferred.promise;
    };

    that.getPeopleDetailsForWatchlistId=function(watchlistId, offset, limit) {
        return $http.get(appConfig.applicationBaseUrl + '/biometrics/watchlists/' + watchlistId+'?offset='+offset+'&limit='+limit);
    };


    that.getPeopleList = function(offset, limit) { // 0  means all people Listing
        let deferred = $q.defer();
        let personlistMapper = [];// will build the consolidated structure to be used as cache
        let arr = []; // will store all the members information
      /*  if (watchlistId) {

        } else {*/
      offset = (offset)?offset: that.PAGINATION.DEFAULT.OFFSET;
      limit = (limit)?limit: that.PAGINATION.DEFAULT.LIMIT;
            that.getPersonsBasedOnLimitOffset(offset, limit)
                .then(function(response) {
                    if (response.data[0].persons !== null && response.status === 200) {
                        let PersonDataSet =response.data[0].persons;
                        personlistMapper.length = 0;
                        let arr = []; // will store all the members information
                        angular.forEach(PersonDataSet, function(dataSet) {
                            let url = appConfig.applicationBaseUrl + '/biometrics/persons/' + dataSet.personId + '/pictures?height=240&width=240&limit=1';
                            arr.push($http.get(url));
                        });

                        $q.all(arr).then(function(ret) {
                            personlistMapper = [];
                            for (let iCtr = 0; iCtr < PersonDataSet.length; iCtr++) {
                                let watchlistId = [];
                                personlistMapper.push({
                                    'firstName': PersonDataSet[iCtr].firstName,
                                    'lastName': PersonDataSet[iCtr].lastName,
                                    'middleName': PersonDataSet[iCtr].middleName,
                                    'notes': PersonDataSet[iCtr].notes,
                                    'gender': PersonDataSet[iCtr].gender,
                                    'age': PersonDataSet[iCtr].age,
                                    'address': PersonDataSet[iCtr].address,
                                    'companyName': PersonDataSet[iCtr].companyName,
                                    'type': PersonDataSet[iCtr].type,
                                    'phone': PersonDataSet[iCtr].phone,
                                    'email': PersonDataSet[iCtr].email,
                                    'documentType': PersonDataSet[iCtr].documentType,
                                    'documentNumber': PersonDataSet[iCtr].documentNumber,
                                    'documentExpiryDate': PersonDataSet[iCtr].documentExpiryDate,
                                    'createdOn': (PersonDataSet[iCtr].additionTime) ? PersonDataSet[iCtr].additionTime : '-',
                                    'pictures': ret[iCtr],
                                    'info': PersonDataSet[iCtr].info,
                                    'allPersonWatchListHoverData': PersonDataSet[iCtr].allPersonWatchListHoverData,
                                    'personId': PersonDataSet[iCtr].personId,
                                    'watchListIdNames': getWatchlistNamesAssociatedwithWatchlistID(PersonDataSet[iCtr].watchlistId),
                                    'watchListIds': fetchPersonWatchlistAssociation(PersonDataSet[iCtr]),
                                });
                            }
                            deferred.resolve(personlistMapper);
                        });
                    } else {
                        deferred.reject(response);
                    }
                }).catch(function(err) {
                deferred.reject(err);
                console.log('Error getting response:::' + err);
            });
      /*  }*/
        return deferred.promise;
    };
    that.validateWatchlist = function(watchlistDataset, newWatchlistName) {
        let isNewWatchlist = true;
        angular.forEach(watchlistDataset, function(index) {
            if (index.name == newWatchlistName)
                isNewWatchlist = false;
        });
        return isNewWatchlist;
    };
    that.deletePerson = function(personId) {
        return $http.delete(appConfig.applicationBaseUrl + '/biometrics/persons/' + personId);
    };
    that.getPersonDetailsforpersonid = function(personId) {
        return $http.get(appConfig.applicationBaseUrl + '/biometrics/persons/' + personId);
    };
    that.updatePersonWatchlistRelationship = function(oldRelationship, newRelationship, personID) {
        return new $q(function(resolve, reject) {
            /* Logic To Delete */
            let totalWatchlistForUpdate = _.difference(oldRelationship, newRelationship).length +_.difference(newRelationship, oldRelationship).length;
            let WatchlistUpdatedCount = 0;
            let payLoad = [{
                'personId': personID,
            }];
            payLoad = JSON.stringify(payLoad);
            _.each(oldRelationship, function(oldWatchListId) {
                let index = _.findIndex(newRelationship, function(val) {
                    return val === oldWatchListId;
                });
                if (index == -1) {
                    $http.delete(appConfig.applicationBaseUrl + '/biometrics/watchlists/' + oldWatchListId + '/members/' + personID)
                      .then(function (response) {
                        WatchlistUpdatedCount = WatchlistUpdatedCount+1;
                        if(totalWatchlistForUpdate == WatchlistUpdatedCount){
                          resolve();
                        }
                      });
                }
            });
            /* Logic To Add */
            _.each(newRelationship, function(newWatchListId) {
                let index = _.findIndex(oldRelationship, function(val) {
                    return val === newWatchListId;
                });
                if (index == -1) {
                    $http.post(appConfig.applicationBaseUrl + '/biometrics/watchlists/' + newWatchListId + '/members', payLoad)
                      .then(function (response) {
                        WatchlistUpdatedCount = WatchlistUpdatedCount+1;
                        if(totalWatchlistForUpdate == WatchlistUpdatedCount){
                          resolve();
                        }
                      });
                }
            });
            // resolve();
        });
    };
    that.getPersonImageforPersonID = function(personID) {
        return $http.get(appConfig.applicationBaseUrl + '/biometrics/persons/' + personID + '/pictures');
    };

    that.getWatchListPopForMarker=function(latestDetection) {
        return '<div class="camMarker">' +
            '<div class="camName">' +
            '<div class="detailsMain">' +
            '<div class="detailsBox">' +
            '<div class="personIcon midBlock">' +
            '<img src="data:image/jpeg;base64,' + latestDetection.appSpecificAlert.detectedImage + '" alt="">' +
            '</div>' +
            '<div class="details midBlock">' +
            '<div class="personName">' + latestDetection.appSpecificAlert.person + '</div>' +
            '<ul class="subDetails">' +
            '<li>' + moment(latestDetection.timestamp).format(dateUtilService.DATE_FORMAT.OVERVIEW_CARD_TIME_FORMAT) + '</li>' +
            '<li>' + moment( latestDetection.timestamp).format(dateUtilService.DATE_FORMAT.OVERVIEW_CARD_DATE_FORMAT) + '</li>' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '<div class="numberMain">' +
            '<div class="number">' + ((latestDetection.appSpecificAlert.score) * 100).toFixed(1) + '%</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
    };

    that.removePersonWatchlistAssociation = function(watchlistId, personId) {
        $http.delete(appConfig.applicationBaseUrl + '/biometrics/watchlists/' + watchlistId + '/members/' + personId);
       // delete personListCache[personId];
    };

    that.personDataInCache = function() {
        return $q(function(resolve) {
         /*   if(Object.keys(personListCache).length == 0) {*/
                that.getAllPersons().then(function(persons) {
                    if(persons.data[0].persons != null && persons.status == 200) {
                        angular.forEach(persons.data[0].persons, function(value, key) {
                            personListCache[value.personId] = value;
                        });
                        resolve(personListCache);
                    }
                });
        /*    } else{
                resolve(personListCache);
            }*/
        });
    };

    that.getAllPersonsForCount = function() {
        return $http.get(appConfig.applicationBaseUrl + '/biometrics/persons/count');
    };

    that.getPersonsBasedOnLimitOffset = function(offset, limit) {
        return $http.get(appConfig.applicationBaseUrl + '/biometrics' +'/persons?offset='+offset+'&limit='+limit);
    };


    that.getMemeberCountForWatchlist = function(watchlistId) {
        return $http.get(appConfig.applicationBaseUrl + '/biometrics/watchlists/' + watchlistId + '/count');
    };


    function getParentScreenNavigationService() {
        return parentScreenNameNavigation;
    }

    function parseImages(imgData) {
        for (let iCtr = 0; imgData.length > iCtr; iCtr++) {
            if (typeof imgData[iCtr] === 'object' || imgData[iCtr] instanceof Object) {
                imgData[iCtr] = imgData[iCtr].image;
            }
        }
        return imgData;
    }

    function getMembersofWatchlistId(watchlistId) {
        return $http.get(appConfig.applicationBaseUrl + '/biometrics/watchlists/' + watchlistId + '/count');
    }

    function deleteOldPersonPictueRecords(oldPersonId) {
        // delete pictures
        $http.get(appConfig.applicationBaseUrl + '/biometrics/persons/' + oldPersonId + '/pictures')
            .then(function(dataset) {
                angular.forEach(dataset.data, function(index) {
                    $http.delete(appConfig.applicationBaseUrl + '/biometrics/persons/' + oldPersonId + '/pictures/' + index.pictureId);
                });
                // delete person
                $http.delete(appConfig.applicationBaseUrl + '/biometrics/persons/' + oldPersonId);
            });
    }

    function getWatchlistSetforWatchlistId(watchlistid) {
        return getAllWatchLists()
            .then(function(response) {
                let allWatchlist = response.data;
                for (let iCtr = 0; iCtr < allWatchlist.length; iCtr++) {
                    if (allWatchlist[iCtr].watchlistId == watchlistid) {
                        return allWatchlist[iCtr];
                    }
                }
            });
    }

    function getWatchlistNamesAssociatedwithWatchlistID(watchlistId) { // returns all Watchlist associated with a Person
        let strAssociatedWatchlists = '';
        _.map(watchlistId, function(n) {
            strAssociatedWatchlists += (_.find(getwatchlistMemberData(), {'watchlistId': n.watchlistId})).name + ',';
        });
        return strAssociatedWatchlists.substring(0, strAssociatedWatchlists.length - 1);
    }

    function fetchPersonWatchlistAssociation(personData) {
        // return $q(function (resolve, reject) {
        let watchlistIds = [];
        if (personData.personWatchlistInfo.length == 0) {
            return watchlistIds;
        } else {
            for (let watchInfo in personData.personWatchlistInfo) {
                watchlistIds.push(personData.personWatchlistInfo[watchInfo].watchlistId);
                if (watchInfo == personData.personWatchlistInfo.length - 1) {
                    return watchlistIds;
                }
            }
        }
    }

    function getAllWatchLists() {
        return $http.get(appConfig.applicationBaseUrl + '/biometrics/watchlists');
    }


    return that;
})
;
