/**
 * Created by alok on 08/04/17.
 */
NEC.factory('ageGenderService', function($http, $q) {
    let that = this;
    let ageGender = [
        {
            img: 'images/detect1.jpg',
            age: '54',
            gender: 'MALE',
        },
        {
            img: 'images/detect2.jpg',
            age: '16',
            gender: 'MALE',
        },
        {
            img: 'images/detect3.jpg',
            age: '47',
            gender: 'FEMALE',
        },
        {
            img: 'images/detect4.jpg',
            age: '21',
            gender: 'FEMALE',
        },
    ];
    let ageGenderNotificationDataSet = [];

    that.getDataFromjson = function() {
        return new $q(function(resolve, reject) {
            $http({
                method: 'GET',
                url: 'data/gender-age.json',
            }).then(function successCallback(response) {
                let ageGenderResponse = response.data;
                resolve(ageGenderResponse);
                setAgeGenderNotificationRecords(ageGenderResponse);
            });
        });
    };
    that.setAgeGenderNotificationRecords = function(dataRecord) {
        if ((ageGenderNotificationDataSet.length + 1) > 5)
            ageGenderNotificationDataSet.pop();

        ageGenderNotificationDataSet.unshift(dataRecord);
    };
    that.getAgeGenderNotificationRecords = function() {
        return ageGenderNotificationDataSet;
    };
    that.getAgeGenderDataDetails = function() {
        return ageGender;
    };

    return that;
});
