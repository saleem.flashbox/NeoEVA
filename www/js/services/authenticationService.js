/**
 * Created by dhanalakshmi on 7/3/17.
 */
NEC.factory('authInterceptor', function( $q, $window, $injector, appConfig) {
    let that = this;

    that.request = function(config) {
        if (appConfig.platform === 'desktop' && $window.sessionStorage.token && config.url.indexOf(appConfig.applicationBaseUrl) === 0
            && config.url.indexOf('/maps/api/geocode/json?address') < 0) {
            config.headers.authorizationToken = $window.sessionStorage.token;
        }else if(appConfig.platform === 'mobile' && localStorage.getItem('token') && config.url.indexOf(appConfig.applicationBaseUrl) === 0
            && config.url.indexOf('/maps/api/geocode/json?address') < 0) {
            config.headers.authorizationToken = localStorage.getItem('token');
        }
        return config;
    },
        that.response = function(response) {
            if (response.status === 401) {
                // handle the case where the user is not authenticated
                if (appConfig.platform === 'mobile') {
                    $injector.get('$state').transitionTo('login');
                }else{
                    $injector.get('$state').transitionTo('admin.signIn');
                }
                return $q.reject(response);
            } else
                return response || $q.when(response);
        },
        that.responseError = function(rejection) {
            if (rejection.status === 401) {
                // handle the case where the user is not authenticated
                if (appConfig.platform === 'mobile') {
                    $injector.get('$state').transitionTo('login');
                }else {
                    $injector.get('$state').transitionTo('admin.signIn');
                }
            }
            return rejection || $q.reject(rejection);
        };
    return that;
    // };
});


