/**
 * Created by Flashbox on 2/15/2016.
 */
NEC.factory('camera', function ($q, $cordovaCamera) {
    return {
        uploadPhoto: function (source) {
            var deferred = $q.defer();
            if (source === 'useCamera') {
                var options = {
                    quality: 50,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: 0,
                    targetWidth: 600,
                    targetHeight: 450,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false
                }
            }
            if (source === 'useGallery') {
                var options = {
                    quality: 75,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 600,
                    targetHeight: 400,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false
                }
            }


            $cordovaCamera.getPicture(options).then(function (imageData) {
                var imgURI = imageData;
                deferred.resolve(imgURI);
            }, function (err) {
                deferred.reject(err);
            })
            return deferred.promise;

        }
    }
})
