// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var NEC = angular.module('NEC', ['ionic', 'ngCordova', 'ngCookies', 'circularLoaderUI', 'ngAnimate']);

NEC.run(function($ionicPlatform, $rootScope) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
      StatusBar.overlaysWebView(true);
    }
    if(localStorage.autoLogin == 'true'){
      $rootScope.$broadcast('autoLogin');
    }

  });

})


  .constant('appConfig', {
    sensorType: 'camera',
    applicationBaseUrl: 'http://163.172.172.74:3002/api',
    baseUrl: '163.172.172.74:3002',
    loginApiBaseUrl: 'http://163.172.172.74:3002/authorizationApi',
    platform: 'mobile',
  })
  .run(function (settingsService, $location, liveVideoFeedService) {
    let protocol = $location.protocol();
    if (protocol === 'https') {
      settingsService.setIsEnabledHttp(true);
    }
    settingsService.getAppSettingDetails().then(function (resultDetails) {
      settingsService.setSettingDetails(resultDetails.data);
    }, function error(errResponse) {
      console.log('cannot get settings config');
    });
    liveVideoFeedService.setAllVideoFeedDetailsForAllApps();
  })


  .config(function ($provide) {
    $provide.decorator('$exceptionHandler', function ($delegate) {
      return function (exception, cause) {
        $delegate(exception, cause);
      };
    });
  })

  .config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  })

NEC.config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'loginCtrl',
      cache: false
    })
    .state('menu', {
      url: '/menu',
      templateUrl: 'templates/menu.html',
      controller: 'mainController'
    })
    .state('menu.home', {
      url: '/home',
      templateUrl: 'templates/home.html'
    })
    .state('menu.app', {
      url: '/app',
      templateUrl: 'templates/app.html'
    })
    .state('menu.app.alerts', {
      url: '/alerts',
      templateUrl: 'templates/alerts.html'
    })
    .state('menu.app.people', {
      url: '/people',
      templateUrl: 'templates/people.html',
      controller: 'watchlistController'
    })
    .state('menu.app.enroll', {
      url: '/enroll',
      templateUrl: 'templates/enroll.html',
      controller: 'enrollCtrl',
      cache: false
    })
    .state('menu.app.watchlist', {
      url: '/watchlist',
      templateUrl: 'templates/watchlist.html',
      controller: 'watchlistController'
    })
    .state('menu.app.settingsTitle', {
      url: '/settingsTitle',
      templateUrl: 'templates/settingsTitle.html',
      controller: 'generalSettingsCtrl'
    })
    .state('menu.app.settings', {
      url: '/settings',
      templateUrl: 'templates/settings.html',
      controller: 'generalSettingsCtrl'
    })
    .state('settings', {
      url: '/settings',
      templateUrl: 'templates/settings.html',
      controller: 'generalSettingsCtrl'
    });
  $urlRouterProvider.otherwise('/login');
});
