/**
 * Created by MohammedSaleem on 12/11/15.
 */

$(document).ready(function () {
  let parentEle = $("body");
  fun = {
    dropDown: function () {
      parentEle.on("click", ".dropDown .head", function (e) {
        e.stopPropagation();
        let init = $(this).data("init");
        if (init == "0") {
          $(this).parent().addClass("open");
          $(this).parent().find(".list").slideDown(200);
          $(this).data({
            "init": "1"
          })
        }
        else {
          $(this).parent().find(".list").slideUp(200, function () {
            $(this).parent().removeClass("open");
          });
          $(this).data({
            "init": "0"
          })
        }
      });
      // parentEle.on("click", ".dropDown .list li", function (e) {
      //   let value = $(this).text();
      //   $(this).parents().eq(1).find(".headTitle").text(value);
      // })
    },
    alertView: function () {
      parentEle.on("click", '.alertsList .item', function (e) {
        $(this).addClass('viewed');
      });
    },
    popup: function (btn, popup) {
      parentEle.on("click", btn, function (e) {
        e.stopPropagation();
        $(".popupBox").fadeOut(0).removeClass('open');
        $(popup).fadeIn(0).addClass('open');
        //$(".analyticsMain .notificationMain").fadeIn(0).removeClass("fadeOutLeftTime").addClass("fadeInLeftTime") ""
      });
      parentEle.on("click", ".popupBox .x, .popupBox .close", function (e) {
        setTimeout(function () {
          $(".popupBox").fadeOut(0).removeClass('open');
        },200);
        //$(".analyticsMain .notificationMain").removeClass("fadeInLeftTime").addClass("fadeOutLeftTime");
        //setTimeout(function () {
        //    $(".analyticsMain .notificationMain").fadeOut(0);
        //},450);
      });

    },
    notificationClick: function () {
      parentEle.on('click', '.notificationList li', function (e) {
        e.stopPropagation();
        console.log('test')
        let init = $(this).data('init');

        if (init == '0') {
          $(this).find('.actionBtn').slideDown(200);
          $(this).data({
            'init': '1',
          });
        } else {
          $(this).find('.actionBtn').slideUp(200);
          $(this).data({
            'init': '0',
          });
        }
      });

      parentEle.on('click', '.notificationList li .actionBtn .btn', function (e) {
        e.stopPropagation();
        $('.notificationList li .actionBtn').slideUp(200);
        $('.notificationList li').data({
          'init': '0',
        });
        $(this).parents().eq(2).addClass('notified');
      });
    },
    defaultClick: function () {
      $(document).click(function () {
        $(".dropDown .list").slideUp(200, function () {
          $(".dropDown").removeClass("open");
        });
        $(".dropDown .head").data({
          "init": "0"
        });
      })
    },
    preventDefaultClicks: function () {
      let preventEle = ".cameraOpt, .dropDown.multi .list, .x";
      parentEle.on("click", preventEle, function (e) {
        e.stopPropagation();
      });
      parentEle.on("click", '.x', function (e) {
        e.preventDefault();
      });
    }
  };
  fun.dropDown();
  fun.alertView();
  //fun.menu();
  fun.notificationClick();
  fun.popup('.alertsList .item', '.alertDetailsPopup');
  fun.popup('.peopleList .item .expandIcon', '.addToWatchlistPopup');
  fun.defaultClick();
  fun.preventDefaultClicks();
});
