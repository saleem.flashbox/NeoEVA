/**
 * Created by MohammedSaleem on 12/08/16.
 */

NEC.directive('datePicker', function () {
    return {
        restrict: 'A',
        scope: {
            date: "=",
            currentDate: "@",
            dateChangeTrigger:"&"
        },
        link: function(scope, element, attrs) {
            $(element).datepicker({
              dateFormat: "mm-dd-yy",
                dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
                onSelect: function (date) {
                    scope.date = date;
                    scope.$apply();


                    if(scope.dateChangeTrigger()){
                        scope.dateChangeTrigger()();
                    }
                }
            });

            if (scope.currentDate){
                $(element).datepicker("setDate", new Date())
                scope.date=moment.utc(new Date()).format('MM-DD-YYYY');
            }
        }
    };
});
/*
link: function (scope, element, attrs, ngModelCtrl) {
    element.datepicker({
        /!* dateFormat: "dd-mm-yy",*!/
        dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
        minDate: 0,
        onSelect: function (date) {
            scope.date = date;
            console.log(date)
            scope.$apply();
        }
    });
}*/
