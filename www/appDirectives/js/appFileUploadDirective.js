/**
 * Created by zendynamix on 13-06-2016.
 */

NEC.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;




            element.bind('change', function (event) {

                var imageDetails=[];
                // scope.fileName = element.val().trim();
                // $(this).parent().find(".mask").text(fileName);

                var fileDetails = this.files[0];

                var img = new Image();
                var canvas = document.createElement('canvas'),
                    ctx = canvas.getContext("2d");

                var imgInfo;
                img.onload = function() {

                    canvas.height = img.height;
                    canvas.width = img.width;
                    ctx.drawImage(img, 0, 0);
                    var dataURL = canvas.toDataURL("image/jpeg");

                    imgInfo = {
                        width:this.width,
                        height: this.height,
                        'base64': dataURL
                    };

                    URL.revokeObjectURL(this.src);

                    imageDetails.push(fileDetails);
                    imageDetails.push(imgInfo);

                    if (scope.details()) {
                        scope.details()(imageDetails);
                    }
                    scope.fileName=fileDetails.name;
                };

                scope.file = URL.createObjectURL(fileDetails);
                img.src = scope.file;

                // scope.file=fileName;
                // var loadFile = function() {
                scope.file = URL.createObjectURL(event.target.files[0]);
                scope.fileDetails=imageDetails;


                console.log(scope.file)

                // var fileDetails = element[0].files[0];

                // scope.details()(fileDetails);
                scope.$apply();
            });


        },
        scope: {
            file: "=",
            fileName: "=",
            details: '&',
            fileDetails:'=',
            fileWidth:'=',
            fileHeight:'='
        }
    };
}]);

