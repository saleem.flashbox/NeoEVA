/**
 * Created by MohammedSaleem on 26/12/16.
 */


(function () {
    var jsSlideUI=angular.module('jsSlideUI',[]);

    var link= function(scope, element, attrs) {
        if (!scope.min){
            scope.min=0;
        }
        if (!scope.max){
            scope.max=10;
        }
        if (!scope.value){
            scope.value=0;
        }



        $(element).find(".jsSlide").slider({
            min: scope.min,
            max: scope.max,
            value: scope.value,
            animate: "fast",
            slide: function( event, ui ) {
                $(element).find(".selectionBar")
                .css({
                    width: ((($(this).slider( "value" )+1) * 100)/scope.max)+'%'
                });

                console.log("value");
            },
            stop: function( event, ui ) {
                if (scope.callbackOnSelection()) {
                    scope.callbackOnSelection()($(this).slider( "value" ),scope.min, scope.max)
                }
                $(element).find(".selectionBar")
                    .css({
                        width: ((($(this).slider( "value" )) * 100)/scope.max)+'%'
                    });
                console.log($(this).slider( "value" ));
            }
        });

        $('<div class="selectionBarMain"><div class="selectionBar"></div></div>').appendTo($(element).find(".jsSlide"))
            .find(".selectionBar").css({
                width: ((scope.value * 100)/scope.max)+'%'
            });

    };

    jsSlideUI.directive('jsSlide',function () {
        return {
            templateUrl: 'appDirectives/templates/jsSlide.html',
            link: link,
            scope: {
                min: "@",
                max: "@",
                value: "@",
                callbackOnSelection: '&'
            }
        }
    })
})();