/**
 * Created by MohammedSaleem on 06/03/17.
 */
(function () {
    'use strict';
    angular.module('pagination', [])
    // .controller('paginationCont', paginationCont)
    .directive('pagination', paginationDir);


    function  link($scope, paginationSer) {
        if(!$scope.type){
            $scope.type="client"
        }
        let currentPageNum=1;
        $scope.pager = {};
        $scope.setPage = setPage;
        $scope.jumpPage = jumpPage;
        initController();
        $scope.$watch('itemSource.length', function () {
                initController();
        })
        $scope.$watch('itemSource', function () {
            initController();
        })
        $scope.$watch('totalNoOfItems', function () {
            initController();
        })

        $scope.$watch('filterText', function () {
            filterByField($scope.itemSource,$scope.filterByField,$scope.filterText,setPage);
        })

        function setPage(currentPage,filteredData) {
            currentPageNum=currentPage;
            $scope.pageSize=$scope.pageSize || 10;
            if(filteredData){
                $scope.filteredItemList=filteredData || $scope.filteredItemList;
            }else{
                $scope.filteredItemList=$scope.itemSource;
            }
            if($scope.type=="client"){
                // get pager object from service
                $scope.pager = paginationSer.GetPager($scope.filteredItemList.length, currentPage, $scope.pageSize);
                // get current page of items
              //  let filteredArray = angular.copy($scope.filteredItemList);
                let filteredArray = $scope.filteredItemList;
                let dataToBeDisplayed = filteredArray.slice($scope.pager.startIndex, $scope.pager.endIndex + 1)
                $scope.itemList = dataToBeDisplayed;
            }else{
                // get pager object from service
                $scope.pager = paginationSer.GetPager($scope.totalNoOfItems, currentPage, $scope.pageSize);
                // get current page of items from server
                if($scope.getDataByPageNo()){
                    $scope.getDataByPageNo()($scope.pager)
                }
            }
        }

        function jumpPage(direction, val) {
            console.log($scope.pager.currentPage);
            let currentNum=$scope.pager.currentPage;
            if (direction=='left' && currentNum > val){
                $scope.setPage(currentNum-val);
            }
            else if (direction=='right' && currentNum < $scope.pager.totalPages-val){
                $scope.setPage(currentNum+val);
            }
        }

        function initController() {
            // initialize to page 1
            if ($scope.itemSource || $scope.totalNoOfItems){
                $scope.filteredItemList = angular.copy($scope.itemSource);
                $scope.setPage(currentPageNum);
            }
        }
    }

    let extractValueByKeys = function (filterOnField, item) {
        let fieldToBeMatched = "";
        _.each(filterOnField, function (fieldName) {
            let data = null;
            data = _.get(item, fieldName);
            if (data) {
                fieldToBeMatched += " "+data;
            }
        })
        return fieldToBeMatched;
    };

    function filterByField(mainArray,filterOnField,filterText,cb){
        if(filterText || filterText==""){
            if(filterText!=""){
                let filteredList = _.filter(angular.copy(mainArray), function (item) {
                    let fieldToBeMatched = extractValueByKeys(filterOnField, item);
                    if(fieldToBeMatched && fieldToBeMatched!=""){
                        let pattern = new RegExp(filterText.toString().toLowerCase());
                        let isMatched = pattern.test(fieldToBeMatched.toLowerCase());
                        if(isMatched){
                            return true;
                        }
                    }
                })
                cb(0,filteredList)
            }else{
                cb(0,null)
            }
        }
    }
    function paginationDir() {
        return{
            templateUrl: "appDirectives/templates/paginationTemp.html",
            controller: link,
            scope: {
                itemSource: "=",
                listName: "=",
                pageSize: "=",
                itemList: "=",
                type:"=",
                getDataByPageNo:"&",
                totalNoOfItems:"=",
                isFilterEnabled:"=",
                filterByField:"=",
                filterText:"="
            }
        }
    }

})();
