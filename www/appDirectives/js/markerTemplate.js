/**
 * Created by MohammedSaleem on 18/02/17.
 */


(function () {
    var markerTemp = angular.module('markerTemp', []);

    var link = function (scope, element, attrs) {


    };

    markerTemp.directive('markerBox', function () {
        return {
            templateUrl: 'appDirectives/templates/markerTemplate.html',
            link: link,
            scope: {

            }
        }
    })
})();