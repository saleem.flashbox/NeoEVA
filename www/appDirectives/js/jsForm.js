/**
 * Created by MohammedSaleem on 20/03/17.
 */

(function() {
    angular.module('jsForm', [])
        .directive('jsonForm', jsonFormFunc);

    function link(scope, obj) {
        scope.test= function(app) {
            scope.formFilter=app;
        };

        scope.myForm = {
            filter: '',
            formLevel: 'one',
        };
        function renderForm() {
            scope.formData = [];
            scope.output={};
            scope.disable=false;


            if (scope.description.hasOwnProperty('appDependencies') && scope.description.appDependencies.length > 0) {
                scope.myForm.formLevel = 'two';

                let allApps = scope.description.configuration;
                scope.apps = [];
                scope.appsInstance = [];
                scope.formData = {};

                if (allApps) {
                    let appsArray = Object.keys(allApps);
                    for (let i = 0; i < appsArray.length; i++) {
                        // ..... getting apps name and app's instance names from config ....


                        if (scope.description.instances[appsArray[i]].spawnType !== 'uniqueSiteWide') {
                            scope.apps.push({
                                appName: (allApps[appsArray[i]].type).split('-').join(' '),
                                appInstance: appsArray[i],
                                appNameInstance: scope.description.definitions[[allApps[appsArray[i]].type]].configuration.type,
                                visited: false,
                            });
                        }
                    }

                    // scope.formFilter = scope.apps[0].appNameInstance;
                    scope.apps[0].visited=true;
                    scope.formFilter = scope.apps[0];
                    // ...... get the form data .......
                    let formDef = scope.description.definitions;
                    for (let j = 0; j < scope.apps.length; j++) {
                        let formInstanceName = scope.apps[j].appNameInstance;

                        let formInstance = formDef[formInstanceName];
                        let instanceArray = Object.keys(formInstance);
                        let forEleEachApp = [];
                        let appProperties={};

                        for (let k = 0; k < instanceArray.length; k++) {
                            let formElement = formInstance[instanceArray[k]];
                         //   if (formElement.settingLevel == "basic") {
                                if(formInstance[instanceArray[k]].type.includes('array')) {
                                    scope.disable=true;
                                    // break;
                                }

                                formElement.name = instanceArray[k];
                                formElement.nameFormatted = instanceArray[k].split(/(?=[A-Z])/).join(' ');
                                if (formInstance[instanceArray[k]].default=='undefined') {
                                    formInstance[instanceArray[k]].default = '';
                                }
                                if(formElement.type=='sensor') {
                                    if(scope.sensorSelected) {
                                        formElement.default = scope.sensorSelected;
                                    }
                                }else{
                                    formElement.default = formInstance[instanceArray[k]].default;
                                }

                                if(formElement.name=='watchlistIds') {
                                    if(scope.watchListSelected) {
                                        formElement.default = scope.watchListSelected;
                                    }else{
                                        formElement.default = '';
                                    }
                                }
                                formElement.isDropDown=false;
                                (forEleEachApp).push(formElement);
                                appProperties[formElement.name]=formElement.default;
                          //  }

                            formElement={};

                            if(k===instanceArray.length-1) {
                                formElement.name = 'node';
                                formElement.nameFormatted = 'Node';
                                formElement.default = scope.nodeDetails[0];
                                formElement.isDropDown=true;
                                formElement.type='text';
                                formElement.description='Node on which this application instance should run';
                                (forEleEachApp).push(formElement);
                                appProperties[formElement.name]=formElement.default;
                            }
                        }


                        scope.output[scope.apps[j].appInstance]=appProperties;
                        scope.formData[scope.apps[j].appNameInstance]={
                            appName: formInstanceName,
                            appInstance: scope.apps[j].appInstance,
                            config: forEleEachApp,
                        };
                    }
                    scope.appDesc=scope.formData;
                }
            } else {
                scope.myForm.formLevel = 'one';
                let appData = scope.description.configuration;
                if (appData) {
                    let dataArray = Object.keys(appData);
                    let appEachEle = [];
                    for (let m = 0; m < dataArray.length; m++) {
                        let appElement = appData[dataArray[m]];
                       // if (appElement.settingLevel == "basic") {
                            if(appData[dataArray[m]].type.includes('array')) {
                                scope.disable=true;
                                // break;
                            }

                            appElement.name = dataArray[m];
                            appElement.nameFormatted = dataArray[m].split(/(?=[A-Z])/).join(' ');

                            if (appData[dataArray[m]].default==='undefined') {
                                appData[dataArray[m]].default = '';
                            }
                            appElement.default = appData[dataArray[m]].default;
                            (appEachEle).push(appElement);
                            scope.output[appElement.name]= appElement.default;
                       // }
                    }
                    (scope.formData).push({
                        config: appEachEle,
                    });
                    scope.appDesc=scope.formData;
                }
            }
        }

        function assignWatchListData() {
            let index = _.findIndex(scope.formData, function(val) {
                if(val.appName=='sim-face-matching-config') {
                    return true;
                }
            });
            if(index!=-1) {
                let index2= _.findIndex(scope.formData[index]['config'], function(val) {
                        if(val.name == 'watchlistIds') {
                            return true;
                        }
                });
                if(index2!=-1) {
                    scope.formData[index]['config'][index2].default = scope.watchListSelected;
                }
            }
        }
        renderForm();
        scope.$watch('description', function() {
            renderForm();
        });
        scope.$watch('watchListSelected', function() {
            renderForm();
        });
    }

    function jsonFormFunc() {
        return {
            // templateUrl: 'appDirectives/templates/jsForm.html',
            templateUrl: 'templates/jsForm.html',
            link: link,
            scope: {
                description: '=',
                output: '=',
                appDesc: '=',
                apps: '=',
                formFilter: '=',
                nodeDetails: '=',
                disable: '=',
                sensorSelected: '=',
                watchListSelected: '=',
            },
        };
    }
})();


