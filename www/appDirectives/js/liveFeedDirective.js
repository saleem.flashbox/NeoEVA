/**
 * Created by MohammedSaleem on 24/05/17.
 */
(function() {
    angular.module('camLiveFeed', [])
        .directive('liveFeed', function() {
            let feed={
                templateUrl: 'appDirectives/templates/liveFeedDirective.html',
                controller: link,
                scope: {
                    status: '=',
                    url: '=',
                    title: '@',
                    resolution: '@',
                    feedURL: '=',
                },
            };
            return feed;
        });

    function link($scope) {
        $scope.status=$scope.status || false;
        $scope.title=$scope.title || 'Live Feed';
        $scope.feedURL=$scope.url;

        $scope.callVideoFeed = function() {
            $scope.status= !$scope.status;
            if($scope.status) {
                $scope.feedURL=$scope.url;
            } else {
                $scope.feedURL = null;
            }
        };
        $scope.$watch('url', function(newValue, oldValue) {
            $scope.feedURL=$scope.url;
        });
    }
})();
